﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Models;
using PruebaEmpresa.Servicios.Interfaces;
using PruebaEmpresa.Contexto.Interfaces;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;

namespace PruebaEmpresa.Servicios
{
    public class Correo_Repo : ICorreo_Repo
    {

        IParametroCorreo_Repo ParametroCorreo_Repo;
        ParametrosCorreoView parametrosCorreoView;
        IPreguntas_Repo Preguntas_Repo;
        IUsuarios_Repo Usuarios_Repo;

        public Correo_Repo(IParametroCorreo_Repo parametroCorreo_Repo, IPreguntas_Repo preguntas_Repo, IUsuarios_Repo usuarios_Repo)
        {
            ParametroCorreo_Repo = parametroCorreo_Repo;
            parametrosCorreoView = ParametroCorreo_Repo.Obtener_Un_Parametro();
            Preguntas_Repo = preguntas_Repo;
            Usuarios_Repo = usuarios_Repo;
        }



        private AlternateView Correo_Respuesta(UsuariosView usuariosView)
        {
            StringBuilder str_html = new StringBuilder();

            string nombreUsuario = usuariosView.Nombre + " " + usuariosView.Apellido_Paterno + " " + (usuariosView.Apellido_Materno ?? "");

            string path = System.Web.HttpContext.Current.Server.MapPath(@"../Estructura/imagenes/icons/logo_completo.png");
            LinkedResource Img = new LinkedResource(path);
            Img.ContentId = "logo";



            str_html.AppendLine("<html><head>");
            str_html.AppendLine("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            str_html.AppendLine("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>");
            str_html.AppendLine("    <title>Centro Capacitación</title>");
            str_html.AppendLine("    <style type=\"text/css\">");

            str_html.AppendLine(" body {   background-color: white;   }");
            str_html.AppendLine(" #global { ");
            str_html.AppendLine(" width:750px; height:800px; ");
            str_html.AppendLine(" margin: 0 auto 0 auto; ");
            str_html.AppendLine("  background-color: #white; }");

            str_html.AppendLine(" #cabecera { ");
            str_html.AppendLine("  width:750px; ");
            str_html.AppendLine(" margin: 0 auto 0 auto; ");
            str_html.AppendLine(" background-color: black;}");

            str_html.AppendLine("#principal { ");
            str_html.AppendLine(" width:748px; ");
            str_html.AppendLine("  margin: 0 auto 0 auto; ");
            str_html.AppendLine(" background-color: white; ");
            str_html.AppendLine("  border: 1px solid; }");

            str_html.AppendLine("#pie { ");
            str_html.AppendLine(" width:750px; ");
            str_html.AppendLine(" margin: 0 auto 0 auto;");
            str_html.AppendLine(" background-color: black; ");
            str_html.AppendLine(" float: left; }");


            str_html.AppendLine(" .x_featuredTitle { ");
            str_html.AppendLine(" color: #ffffff;");
            str_html.AppendLine("   font-size: 26px;");
            str_html.AppendLine(" padding: 0px 0px 10px 0px;");
            str_html.AppendLine(" font-weight: bold;}");

            str_html.AppendLine(" .x_featuredContent {");
            str_html.AppendLine(" color: #ffffff; }");

            str_html.AppendLine("    </style>");
            str_html.AppendLine("  </head>");


            str_html.AppendLine("  <body>");

            str_html.AppendLine("  <div id=\"global\"> ");
            str_html.AppendLine("      <div id=\"cabecera\">");
            str_html.AppendLine("  <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100 % \">");
            str_html.AppendLine(" <tr> ");
            str_html.AppendLine("   <td class=\"x_featuredTitle\" > ");
            str_html.AppendLine("   <div style=\"padding: 20px;width: 500px\">");
            str_html.AppendLine("    Guía Emprende");
            str_html.AppendLine("   </div>");
            str_html.AppendLine(" </td>");
            str_html.AppendLine("<td>");
            str_html.AppendLine(" <img src='cid:logo' alt=\"sin imagen\" width=\"140px\" height=\"50px\" />");
            str_html.AppendLine("</td>");
            str_html.AppendLine(" </tr>");
            str_html.AppendLine("</table>");
            str_html.AppendLine("</div>");

            str_html.AppendLine(" <div id=\"principal\"> ");
            str_html.AppendLine("     <div style=\"padding: 20px\">");
            str_html.AppendLine("      <p> <h2> Estimado Usuario:</h2> </p>");
            str_html.AppendLine("      <hr>");
            str_html.AppendLine("       <b>  <span style='color: red; '> " + nombreUsuario + " </span> </b> ");
            str_html.AppendLine("       <br>");
            str_html.AppendLine("          <p> Se ha recuperado su constraseña : <span style='color: black; '>  " + usuariosView.Password + " </span> </p>");
            str_html.AppendLine("     </div>");
            str_html.AppendLine("</div>");
            str_html.AppendLine("  <div id=\"pie\"> ");
            str_html.AppendLine("     <div style=\"padding: 10px\" class=\"x_featuredContent\"> ");
            str_html.AppendLine("        Este es un correo electrónico no monitoreado, favor de no responder.");
            str_html.AppendLine("  </div>");

            str_html.AppendLine("  </div> </body></html>");

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str_html.ToString(), null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            return AV;
        }

        private AlternateView Correo_Respuesta(UsuariosView usuariosView, PreguntasView preguntasView, string Respuesta)
        {
            StringBuilder str_html = new StringBuilder();

            string nombreUsuario = usuariosView.Nombre + " " + usuariosView.Apellido_Paterno + " " + (usuariosView.Apellido_Materno ?? "");

            string path =  System.Web.HttpContext.Current.Server.MapPath(@"../Estructura/imagenes/icons/logo_completo.png");
            LinkedResource Img = new LinkedResource(path);
            Img.ContentId = "logo";
            


            str_html.AppendLine("<html><head>");
            str_html.AppendLine("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            str_html.AppendLine("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>");
            str_html.AppendLine("    <title>Centro Capacitación</title>");
            str_html.AppendLine("    <style type=\"text/css\">");

            str_html.AppendLine(" body {   background-color: white;   }");
            str_html.AppendLine(" #global { ");
            str_html.AppendLine(" width:750px; height:800px; ");
            str_html.AppendLine(" margin: 0 auto 0 auto; ");
            str_html.AppendLine("  background-color: #white; }");

            str_html.AppendLine(" #cabecera { ");
            str_html.AppendLine("  width:750px; ");
            str_html.AppendLine(" margin: 0 auto 0 auto; ");
            str_html.AppendLine(" background-color: black;}");

            str_html.AppendLine("#principal { ");
            str_html.AppendLine(" width:748px; ");
            str_html.AppendLine("  margin: 0 auto 0 auto; ");
            str_html.AppendLine(" background-color: white; ");
            str_html.AppendLine("  border: 1px solid; }");

            str_html.AppendLine("#pie { ");
            str_html.AppendLine(" width:750px; ");
            str_html.AppendLine(" margin: 0 auto 0 auto;");
            str_html.AppendLine(" background-color: black; ");
            str_html.AppendLine(" float: left; }");


            str_html.AppendLine(" .x_featuredTitle { ");
            str_html.AppendLine(" color: #ffffff;");
            str_html.AppendLine("   font-size: 26px;");
            str_html.AppendLine(" padding: 0px 0px 10px 0px;");
            str_html.AppendLine(" font-weight: bold;}");

            str_html.AppendLine(" .x_featuredContent {");
            str_html.AppendLine(" color: #ffffff; }");
  
            str_html.AppendLine("    </style>");
            str_html.AppendLine("  </head>");


            str_html.AppendLine("  <body>");

            str_html.AppendLine("  <div id=\"global\"> ");
            str_html.AppendLine("      <div id=\"cabecera\">");
            str_html.AppendLine("  <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100 % \">");
            str_html.AppendLine(" <tr> ");
            str_html.AppendLine("   <td class=\"x_featuredTitle\" > ");
            str_html.AppendLine("   <div style=\"padding: 20px;width: 500px\">");
            str_html.AppendLine("     Guía Emprende");
            str_html.AppendLine("   </div>");
            str_html.AppendLine(" </td>");
            str_html.AppendLine("<td>");
            str_html.AppendLine(" <img src='cid:logo' alt=\"sin imagen\" width=\"140px\" height=\"50px\" />");
            str_html.AppendLine("</td>");
            str_html.AppendLine(" </tr>");
            str_html.AppendLine("</table>");
            str_html.AppendLine("</div>");

            str_html.AppendLine(" <div id=\"principal\"> ");
            str_html.AppendLine("     <div style=\"padding: 20px\">");
            str_html.AppendLine("      <p> <h2> Estimado Usuario:</h2> </p>");
            str_html.AppendLine("      <hr>");
            str_html.AppendLine("       <b>  <span style='color: red; '> " + nombreUsuario + " </span> </b> ");
            str_html.AppendLine("       <br>");
            str_html.AppendLine("      <u> <h2>  " + preguntasView.Nombre_Mentor + " </h2> </u>");
            str_html.AppendLine("      <p> <b> ha respondido: </b>  </p> ");
            str_html.AppendLine("      <p style='text-align:justify'> " + Respuesta + " </p>");
            str_html.AppendLine("      <h3> A la pregunta:  "  + preguntasView.Descripcion_Pregunta +" </h3>");
            str_html.AppendLine("     </div>");
            str_html.AppendLine("</div>");
            str_html.AppendLine("  <div id=\"pie\"> ");
            str_html.AppendLine("     <div style=\"padding: 10px\" class=\"x_featuredContent\"> ");
            str_html.AppendLine("        Este es un correo electrónico no monitoreado, favor de no responder.");
            str_html.AppendLine("  </div>");

            str_html.AppendLine("  </div> </body></html>");

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str_html.ToString(), null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            return AV;
        }

        public string Enviar_Correo_Respuesta(int Pregunta_ID, string Respuesta)
        {
            string res = "bien";
            //string mensaje1 = "";
            try
            {
                PreguntasView preguntasView = Preguntas_Repo.Obtener_Una_Pregunta(Pregunta_ID);
                UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(preguntasView.Usuario_ID);

                if (parametrosCorreoView == null)
                {
                    res = "No hay configuración de parametros de correo";
                    return res;
                }

                //mensaje1 = "<p> <h2> Estimado Usuario:</h2> </p>";
                //mensaje1 += " <hr>";
                //mensaje1 += "<b>  <span style='color: red; '> " + usuariosView.Nombre + " " + usuariosView.Apellido_Paterno + " </span> </b>";
                //mensaje1 += "<br>";
                //mensaje1 += "<u> <h2>  El mentor: " + preguntasView.Nombre_Mentor + " </h2> </u>";
                //mensaje1 += "<br>";
                //mensaje1 += "<p> <b> ha respondido: </b>  </p>";
                //mensaje1 += "<br>";
                //mensaje1 += "<p style='text-align:justify'> " + Respuesta + " </p>";
                //mensaje1 += "<br>";
                //mensaje1 += "<h3> A la pregunta: " + preguntasView.Descripcion_Pregunta  + " </h3>";
                AlternateView alternateView = Correo_Respuesta(usuariosView, preguntasView, Respuesta);

                Procesar_Correo(usuariosView.Correo_Electronico, "Respuesta Mentor",alternateView);

            }
            catch (Exception ex) {
                res = ex.Message;
            }

            return res;
        }

        public string Enviar_Correo(UsuariosView usuariosView)
        {
            string respuesta = "bien";
            //string mensaje = "";

            try
            {

                if(parametrosCorreoView == null)
                {
                    respuesta = "No hay configuración de parametros de correo";
                    return respuesta;
                }

                //mensaje = "<p> <h2> Estimado Usuario:</h2> </p>";
                //mensaje += " <hr>";
                //mensaje += "<b> " + usuariosView.Correo_Electronico  + " </b>";
                //mensaje += "<p> Se ha recuperado su constraseña : <span style='color: red; '>  " + usuariosView.Password + " </span> </p>";
                AlternateView alternateView = Correo_Respuesta(usuariosView);


                Procesar_Correo(usuariosView.Correo_Electronico, "Recuperar Contraseña", alternateView);

            }
            catch (Exception ex) {
                respuesta = ex.Message;
            }

            return respuesta;
        }


        public void Procesar_Correo(string pCorreoDestinatario, string pAsunto, AlternateView alternateView)
        {
            int puerto = 0;
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(pCorreoDestinatario);
            mailMessage.From = new MailAddress(parametrosCorreoView.Email, "Centro de Capacitación", System.Text.Encoding.UTF8);
            mailMessage.Subject = pAsunto;

            mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.High;
            mailMessage.AlternateViews.Add(alternateView);
            Int32.TryParse(parametrosCorreoView.Puerto, out puerto);

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Credentials = new System.Net.NetworkCredential(parametrosCorreoView.Email, parametrosCorreoView.Password);
            smtpClient.Port = puerto;
            smtpClient.Host = parametrosCorreoView.Host_Email;
            smtpClient.EnableSsl = true;
            smtpClient.Send(mailMessage);

            mailMessage.Dispose();
        }

        public void Procesar_Correo(string pCorreoDestinatario, string pAsunto, string pMensaje)
        {
            int puerto = 0;
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(pCorreoDestinatario);
            mailMessage.From = new MailAddress(parametrosCorreoView.Email, "Centro de Capacitación", System.Text.Encoding.UTF8);
            mailMessage.Subject = pAsunto;

            mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            mailMessage.Body = pMensaje;
            mailMessage.IsBodyHtml = true;

            Int32.TryParse(parametrosCorreoView.Puerto, out puerto);

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Credentials = new System.Net.NetworkCredential(parametrosCorreoView.Email, parametrosCorreoView.Password);
            smtpClient.Port = puerto;
            smtpClient.Host = parametrosCorreoView.Host_Email;
            smtpClient.EnableSsl = true;
            smtpClient.Send(mailMessage);

            mailMessage.Dispose();
        }
    }
}