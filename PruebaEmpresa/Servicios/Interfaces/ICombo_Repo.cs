﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Servicios.Interfaces
{
   public interface ICombo_Repo
    {
        List<SimpleComboView> Obtener_Estatus();
    }
}
