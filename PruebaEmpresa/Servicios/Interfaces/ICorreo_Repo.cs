﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Servicios.Interfaces
{
   public interface ICorreo_Repo
    {
        string Enviar_Correo(UsuariosView usuariosView);
        string Enviar_Correo_Respuesta(int Pregunta_ID, string Respuesta);
    }
}
