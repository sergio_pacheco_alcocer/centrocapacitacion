﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Models;
using PruebaEmpresa.Servicios.Interfaces;

namespace PruebaEmpresa.Servicios
{
    public class Combo_Repo : ICombo_Repo
    {
        public List<SimpleComboView> Obtener_Estatus()
        {
            List<SimpleComboView> Lista_Combo = new List<SimpleComboView>();

            Lista_Combo.Add(new SimpleComboView { name = "ACTIVO" });
            Lista_Combo.Add(new SimpleComboView { name = "INACTIVO" });

            return Lista_Combo;

        }
    }
}