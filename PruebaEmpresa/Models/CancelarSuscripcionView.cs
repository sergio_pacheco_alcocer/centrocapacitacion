﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class CancelarSuscripcionView
    {

        public int Cancelar_Suscripcion_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public int Cargo_ID { get; set; }
        public string Identificador { get; set; }


        public CancelarSuscripcionView()
        {
            Cancelar_Suscripcion_ID = 0;
            Usuarios_ID = 0;
            Cargo_ID = 0;
            Identificador = "";
        }
    }
}