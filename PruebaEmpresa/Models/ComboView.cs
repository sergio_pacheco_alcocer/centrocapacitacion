﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class ComboView
    {
        public int id { get; set; }
        public string name { get; set; }
        public string clave { get; set; }

        public ComboView()
        {
            id = 0;
            name = "";
            clave = "";
        }
    }
}