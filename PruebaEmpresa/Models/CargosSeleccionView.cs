﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class CargosSeleccionView
    {
      public  List<CargosView> Lista_Cargo { get; set; }
      public  bool Tiene_Proveedor { get; set; }
      public  bool Tiene_Pago { get; set; }
      public  int Cargo_ID { get; set; }

       public CargosSeleccionView()
        {
            Lista_Cargo = new List<CargosView>();
            Tiene_Pago = false;
            Tiene_Proveedor = false;
            Cargo_ID = 0;
        }

    }
}