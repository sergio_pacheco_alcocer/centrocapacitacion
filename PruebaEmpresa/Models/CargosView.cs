﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class CargosView
    {
        public int Cargo_ID { get; set; }

        [Display(Name = "Nombre Cargo")]
        [Required(ErrorMessage = "Se requiere un nombre del cargo")]
        public string Nombre_Cargo { get; set; }
        [Required(ErrorMessage = "Se requiere un precio")]
        [Range(1, 10000, ErrorMessage = "Precio Incorrecto")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "Número valido con máximo dos decimales")]
        public decimal Precio { get; set; }
        [Required(ErrorMessage = "Se requiere un día")]
        [Range(1,1000, ErrorMessage ="Selección del día incorrecto")]
        public int Dias { get; set; }
        [Required(ErrorMessage = "Se requiere un estatus")]
        public string Estatus { get; set; }
        [Display(Name = "Producto Stripe")]
        public string ProductoFinancieroID { get; set; }
        public string Usuario_Creo { get; set; }
    }
}