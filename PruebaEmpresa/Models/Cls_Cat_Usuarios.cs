﻿using PruebaEmpresa.Contexto.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    [NotMapped]
    public class Cls_Cat_Usuarios : Cat_Usuarios
    {
        public string Nombre_Especialidad { get; set; }
    }
}