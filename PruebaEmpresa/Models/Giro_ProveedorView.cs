﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Giro_ProveedorView
    {
        public int Giro_Proveedor_ID { get; set; }
        public string Nombre_Giro_Proveedor { get; set; }
        public string Estatus { get; set; }
    }
}