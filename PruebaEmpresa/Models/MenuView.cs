﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class MenuView
    {
      public  List<Cls_Modelo_Menu> objNavegacionCombo { get; set; }
      public  List<Cls_Modelo_Menu> objNavegacionSimple { get; set; }

        public MenuView() {
            objNavegacionCombo = new List<Cls_Modelo_Menu>();
            objNavegacionSimple = new List<Cls_Modelo_Menu>();
        }
    }
}