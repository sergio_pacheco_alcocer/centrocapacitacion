﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class PreguntasView
    {
        public int Pregunta_ID { get; set; }
        public int Usuario_ID { get; set; }
        public int Usuario_Mentor_ID { get; set; }
        public string Descripcion_Pregunta { get; set; }
        public bool Visto { get; set; }
        public int Total_Respuestas { get; set; }

        [Display(Name = "Fecha Creo")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Creo { get; set; }
        public string Nombre_Mentor { get; set; }
        public string Nombre_Usuario { get; set; }
        public string Visto_Cadena { get; set; }

        public PreguntasView() {
            Pregunta_ID = 0;
            Usuario_ID = 0;
            Usuario_Mentor_ID = 0;
            Descripcion_Pregunta = "";
            Visto = false;
            Fecha_Creo = DateTime.Now;
            Usuario_Creo = "";
            Nombre_Mentor = "";
            Nombre_Usuario = "";
            Visto_Cadena = "";
        }
    }
}