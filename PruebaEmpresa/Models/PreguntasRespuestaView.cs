﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class PreguntasRespuestaView
    {
        public int Pregunta_Respuesta_ID { get; set; }
        public int Pregunta_ID { get; set; }
        public int Usuario_ID { get; set; }
        public string Respuesta { get; set; }
        public string Usuario_Creo { get; set; }
        public string Tipo { get; set; }
        public bool Visto_Respuesta { get; set; }
        public DateTime Fecha_Creo { get; set; }


        public PreguntasRespuestaView()
        {
            Pregunta_Respuesta_ID = 0;
            Pregunta_ID = 0;
            Usuario_ID = 0;
            Respuesta = "";
            Usuario_Creo = "";
            Fecha_Creo = DateTime.Now;
            Tipo = "";
        }
    }
}