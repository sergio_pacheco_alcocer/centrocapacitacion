﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Cls_Cargo_Pago
    {

        [Required(ErrorMessage = "Se requiere nombre de tarjeta")]
        public string CardName { get; set; }
        [Required(ErrorMessage = "Se requiere un correo electronico")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Se requiere un número de teléfono")]
        public string Phone { get; set; }
        public string stripeToken { get; set; }
        public int Cargo_ID { get; set; }
    }
}