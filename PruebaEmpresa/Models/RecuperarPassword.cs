﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class RecuperarPassword
    {
        [Required(ErrorMessage = "Se requiere un correo")]
        [RegularExpression(".+\\@.+\\..+",
   ErrorMessage = "Se requiere un correo electrónico valido")]
        public string Corre_Electronico { get; set; }

        public string Mensajes { get; set; }
        public string  returnUrl { get; set; }

        public RecuperarPassword()
        {
            Corre_Electronico = "";
            Mensajes = "";
        }
    }
}