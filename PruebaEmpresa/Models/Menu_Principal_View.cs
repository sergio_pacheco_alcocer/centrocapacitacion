﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Menu_Principal_View
    {
        public int Menu_Principal_ID { get; set; }
        public string Nombre { get; set; }
        public string Controlador { get; set; }
        public string Accion { get; set; }

        public List<FaseUnoView> Lista_Fase_Uno { get; set; }

        public Menu_Principal_View()
        {
            Lista_Fase_Uno = new List<FaseUnoView>();
            Nombre = "";
            Controlador = "";
            Accion = "";
        }
    }
}