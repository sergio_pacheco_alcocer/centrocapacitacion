﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class SimpleComboView
    {
        public string name { get; set; }

        public SimpleComboView()
        {
            this.name = "";
        }
    }
}