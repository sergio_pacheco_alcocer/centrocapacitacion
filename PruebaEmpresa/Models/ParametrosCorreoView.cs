﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class ParametrosCorreoView
    {
        public int Parametro_Correo_ID { get; set; }
        [Required(ErrorMessage = "Se requiere Correo")]
        [RegularExpression(".+\\@.+\\..+",
      ErrorMessage = "Se requiere un correo electrónico valido")]
        [Display(Name = "Correo Electrónico")]
        public string Email { get; set; }
        [Display(Name = "Host Email")]
        [Required(ErrorMessage = "Se requiere Host")]
        public string Host_Email { get; set; }
        [Required(ErrorMessage = "Se requiere un puerto")]
        [RegularExpression("^[0-9]+$", ErrorMessage ="Solo acepta números")]
        public string Puerto { get; set; }
        [Required(ErrorMessage = "Se requiere un password")]
        public string Password { get; set; }
        public string Usuario_Creo { get; set; }
    }
}