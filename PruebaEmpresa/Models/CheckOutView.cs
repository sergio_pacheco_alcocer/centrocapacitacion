﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class CheckOutView
    {
     
       public string Mensaje { get; set; }
       public  CargosView cargosView { get; set; }
       public Cls_Cargo_Pago cls_Cargo_Pago { get; set; }


        public CheckOutView()
        {
            cargosView = new CargosView();
            cls_Cargo_Pago = new Cls_Cargo_Pago();
            Mensaje = "";
        }

    }
}