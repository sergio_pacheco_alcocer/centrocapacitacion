﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class RespuestasView
    {
      public  PreguntasView preguntasView { get; set; }
      public  List<PreguntasRespuestaView> preguntasRespuestaViews { get; set; }

       public  RespuestasView() {
            preguntasView = new PreguntasView();
            preguntasRespuestaViews = new List<PreguntasRespuestaView>();
        }
    }
}