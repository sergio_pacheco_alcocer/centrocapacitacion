﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Cls_Mensaje_Servidor
    {

        public string Mensaje { get; set; }
        public string Dato1 { get; set; }
        public string Dato2 { get; set; }
        public string Dato3 { get; set; }
        public string Dato4 { get; set; }
        public string Dato5 { get; set; }

        public Cls_Mensaje_Servidor()
        {
            this.Mensaje = "";
            this.Dato1 = "";
            this.Dato2 = "";
            this.Dato3 = "";
            this.Dato4 = "";
            this.Dato5 = "";
        }
    }
}