﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Cls_Inicio_Sesion
    {
        [Required(ErrorMessage = "Se requiere un correo")]
        [RegularExpression(".+\\@.+\\..+",
      ErrorMessage = "Se requiere un correo electrónico valido")]
        public string Correo_Electronico { get; set; }

        [Required(ErrorMessage = "Se requiere una Contraseña")]
        public string Contraseña { get; set; }

        public Cls_Inicio_Sesion()
        {
            this.Correo_Electronico = "";
            this.Contraseña = "";
        }

    }
}