﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class LoginView
    {
        public Cls_Inicio_Sesion Inicio_Sesion { get; set; }
        public Cls_Registro_Usuario Registro_Usuario { get; set; }
        public string Mensajes { get; set; }
        public LoginView()
        {
            Inicio_Sesion = new Cls_Inicio_Sesion();
            Registro_Usuario = new Cls_Registro_Usuario();
            Mensajes = "";
        }
    }
}