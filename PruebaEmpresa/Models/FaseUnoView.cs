﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class FaseUnoView
    {

        public string Nombre_Fase_Uno { get; set; } 
        public List<FaseDosView> Lista_Fase_Dos { get; set; }

        public FaseUnoView()
        {
            Nombre_Fase_Uno = "";
            Lista_Fase_Dos = new List<FaseDosView>();
        }
    }
}