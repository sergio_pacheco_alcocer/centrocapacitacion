﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Cls_Preguntas_Pendientes
    {
        public int Pregunta_ID { get; set; }
        public string Descripcion_Pregunta { get; set; }
        public string Nombre_Mentor_Completo { get; set; }
        public string Nombre_Especialidad { get; set; }
        public DateTime Fecha_Creo { get; set; }

    }
}