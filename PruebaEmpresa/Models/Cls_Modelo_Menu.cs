﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Cls_Modelo_Menu
    {
        public string Nombre { get; set; }
        public string Accion { get; set; }
        public string Controlador { get; set; }
        public string dato { get; set; }
        public List<Cls_Modelo_Menu> Elementos { get; set; }

        public Cls_Modelo_Menu()
        {
            this.Nombre = "";
            this.Accion = "";
            this.Controlador = "";
            this.dato = "";
            this.Elementos = new List<Cls_Modelo_Menu>();
        }
    }
}