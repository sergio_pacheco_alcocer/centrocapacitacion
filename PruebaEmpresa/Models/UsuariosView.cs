﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class UsuariosView
    {
        public int Usuarios_ID { get; set; }
        public int Rol_ID { get; set; }
        public int? Especialidad_ID { get; set; }


        [Required(ErrorMessage = "Se requiere una nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "Se requiere Apellido")]
        public string Apellido_Paterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string Apellido_Materno { get; set; }

        [Display(Name = "Correo Electrónico")]
        [RegularExpression(".+\\@.+\\..+",
        ErrorMessage = "Se requiere un correo electrónico valido")]
        [Required(ErrorMessage = "Se requiere un correo")]
        public string Correo_Electronico { get; set; }

        [Required(ErrorMessage = "Se requiere un teléfono")]
        [Display(Name = "Teléfono")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Solo acepta números")]
        [StringLength(10, ErrorMessage = "La longitud del teléfono debe ser de 10 carácteres", MinimumLength = 10)]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Se requiere una contraseña")]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Se requiere un estatus")]
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }

        [Display(Name = "ROL")]
        public string Nombre_Rol { get; set; }
        public string CustomerID { get; set; }
        public string SubscriptionID { get; set; }
        public string Nombre_Especialidad { get; set; }
        public bool Es_Empresa { get; set; }
        public string Nombre_Imagen { get; set; }
        public string Tipo_Imagen { get; set; }

        [Display(Name = "Empresa")]
        public string Nombre_Empresa { get; set; }


        public UsuariosView()
        {
            Es_Empresa = false;
            Usuarios_ID = 0;
        }

    }
}