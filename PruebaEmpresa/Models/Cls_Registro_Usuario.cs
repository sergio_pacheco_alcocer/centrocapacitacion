﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class Cls_Registro_Usuario
    {
        [Required(ErrorMessage = "Se requiere un correo")]
        [RegularExpression(".+\\@.+\\..+",
      ErrorMessage = "Se requiere un correo electrónico valido")]
        public string Correo_Electronico { get; set; }
        [Required(ErrorMessage = "Se requiere una Contraseña")]
        [StringLength(8, ErrorMessage = "La longitud de la constraseña es incorrecta", MinimumLength = 6)] 
        public string Contraseña { get; set; }
        [Compare("Contraseña", ErrorMessage = "La contraseña y la confirmación de la contraseña no coinciden")]
        public string Confirmar_Contraseña { get; set; }

        [Required(ErrorMessage = "Se requiere un teléfono")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Solo acepta números")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Se requiere un nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Se requiere un apellido paterno")]
        public string Apellido_Paterno { get; set; }

        public string Apellido_Materno { get; set; }


        public Cls_Registro_Usuario()
        {
            this.Correo_Electronico = "";
            this.Contraseña = "";
            this.Confirmar_Contraseña = "";
            this.Telefono = "";
            this.Nombre = "";
            this.Apellido_Paterno = "";
            this.Apellido_Materno = "";
        }
    }
}