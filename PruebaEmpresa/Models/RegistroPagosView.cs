﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class RegistroPagosView
    {
        public int Registro_Pago_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public int Cargo_ID { get; set; }
        public string Identificador { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Inicio_Vigencia { get; set; }
        public DateTime Fecha_Fin_Vigencia { get; set; }
    }
}