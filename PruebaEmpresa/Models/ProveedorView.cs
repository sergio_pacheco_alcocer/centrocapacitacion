﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class ProveedorView
    {
        public int Proveedor_ID { get; set; }
        public int Giro_Proveedor_ID { get; set; }
        public string Nombre_Proveedor { get; set; }
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string Numero_Exterior { get; set; }
        public string Numero_Interior { get; set; }
        public string Sitio_WEB { get; set; }
        public string Telefono { get; set; }
        public string Nombre_Giro_Proveedor { get; set; }
        public string Estatus { get; set; }
        public string Correo_Electronico { get; set; }
        public string Que_Hace_la_Empresa { get; set; }
        public int Usuarios_ID { get; set; }
        public Decimal Latitud { get; set; }
        public Decimal Longitud { get; set; }

        public ProveedorView()
        {
            Proveedor_ID = 0;
            Giro_Proveedor_ID = 0;
            Nombre_Proveedor = "";
            Colonia = "";
            Calle = "";
            Numero_Exterior = "";
            Numero_Interior = "";
            Sitio_WEB = "";
            Telefono = " ";
            Nombre_Giro_Proveedor = "";
            Estatus = "";
            Correo_Electronico = "";
            Usuarios_ID = 0;
        }
    }
}