﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class MentorView
    {
        public int Usuarios_ID { get; set; }
        public int Rol_ID { get; set; }

        [Required(ErrorMessage = "Se requiere una especialidad")]
        public int? Especialidad_ID { get; set; }

        [Required(ErrorMessage = "Se requiere un nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        [RequiredIfFalse("Es_Empresa", ErrorMessage = "Se requiere un apellido paterno")]      
        public string Apellido_Paterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string Apellido_Materno { get; set; }

        [Display(Name = "Correo Electrónico")]
        [RegularExpression(".+\\@.+\\..+",
      ErrorMessage = "Se requiere un correo electrónico valido")]
        [Required(ErrorMessage = "Se requiere un correo")]
        public string Correo_Electronico { get; set; }

        [Required(ErrorMessage = "Se requiere un teléfono")]
        [Display(Name = "Teléfono")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Solo acepta números")]
        [StringLength(10, ErrorMessage = "La longitud del teléfono debe ser de 10 carácteres", MinimumLength = 10)]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Se requiere una contraseña")]
        [StringLength(8, ErrorMessage = "La longitud de la constraseña es incorrecta", MinimumLength = 6)]
        public string Contraseña { get; set; }


        [Compare("Contraseña", ErrorMessage = "La contraseña y la confirmación de la contraseña no coinciden")]
        [Display(Name = "Confirmar Contraseña")]
        public string Confirmar_Contraseña { get; set; }

        [Required(ErrorMessage = "Se requiere un estatus")]
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
        public string Nombre_Rol { get; set; }
        [Display(Name = "Especialidad")]
        public string Nombre_Especialidad { get; set; }

        public string Nombre_Imagen { get; set; }
        public string Tipo_Imagen { get; set; }


        [Display(Name = "Da clic si  el mentor es una empresa")]
        public bool Es_Empresa { get; set; }

        public MentorView()
        {
            Usuarios_ID = 0;
        }
    }
}