﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class EspecialidadViewModel
    {
        public int Especialidad_ID { get; set; }
        [Display(Name = "Nombre Especialidad")]
        [Required(ErrorMessage = "Se requiere un nombre de especialidad")]
        public string Nombre_Especialidad { get; set; }
        [Required(ErrorMessage = "Se requiere un estatus")]
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }


        public EspecialidadViewModel()
        {
            Especialidad_ID = 0;
            Nombre_Especialidad = "";
            Estatus = "";
            Usuario_Creo = "";
        }
    }
}