﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    public class FaseDosView
    {
        public int Fase_Dos_ID { get; set; }
        public int Fase_Uno_ID { get; set; }
        public string Nombre_Fase_Dos { get; set; }
    }
}