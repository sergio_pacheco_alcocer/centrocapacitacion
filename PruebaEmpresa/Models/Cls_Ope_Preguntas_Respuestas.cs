﻿using PruebaEmpresa.Contexto.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Models
{
    [NotMapped]
    public class Cls_Ope_Preguntas_Respuestas : Ope_Preguntas_Respuestas
    {
        public int Usuario_Visitante_ID { get; set; }
        public string Nombre_Mentor_Completo { get; set; }
        public string Nombre_Especialidad { get; set; }
        public string Descripcion_Pregunta { get; set; }

    }
}