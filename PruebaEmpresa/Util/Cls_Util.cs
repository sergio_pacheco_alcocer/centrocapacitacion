﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace PruebaEmpresa.Util
{
    public class Cls_Util
    {
        public static DateTime ConvertirCadenaFecha(string strFecha)
        {
            DateTime date;

            date = DateTime.ParseExact(strFecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            return date;
        }
    }
}