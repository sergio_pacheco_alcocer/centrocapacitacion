﻿using PruebaEmpresa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Util
{
    public class Ayudante_Menu
    {

        public static Cls_Modelo_Menu Crear_Menu_Nivel1(string NombreMenu)
        {
            Cls_Modelo_Menu cls_Modelo_Menu = new Cls_Modelo_Menu();


            cls_Modelo_Menu = new Cls_Modelo_Menu();
            cls_Modelo_Menu.Nombre = NombreMenu;
            cls_Modelo_Menu.Accion = "";
            cls_Modelo_Menu.Controlador = "";


            return cls_Modelo_Menu;

        }

        public static Cls_Modelo_Menu Crear_Menu_Nivel2(string NombreMenu, string MenuAccion, string MenuControlador)
        {
            Cls_Modelo_Menu cls_Modelo_Menu = new Cls_Modelo_Menu();


            cls_Modelo_Menu = new Cls_Modelo_Menu();
            cls_Modelo_Menu.Nombre = NombreMenu;
            cls_Modelo_Menu.Accion = MenuAccion;
            cls_Modelo_Menu.Controlador = MenuControlador;


            return cls_Modelo_Menu;
        }


        public static Cls_Modelo_Menu Crear_Menu_Proveedor()
        {
            Cls_Modelo_Menu objElementoPrincipal;

            objElementoPrincipal = Ayudante_Menu.Crear_Menu_Nivel1("Proveedores");
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Mi Proveedor", "Listado", "Proveedor"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Suscripción", "Listado_Cargos", "Pago"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Buscar", "Buscar", "Proveedor"));

            return objElementoPrincipal;
        }

        public static Cls_Modelo_Menu Crear_Menu_Catalogos()
        {
            Cls_Modelo_Menu objElementoPrincipal;

            objElementoPrincipal = Ayudante_Menu.Crear_Menu_Nivel1("Catálogos");
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Giro Proveedor", "Catalogo_Giro_Proveedores", "GiroProveedor"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Cargos", "Listado", "Cargos"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Parámetros Correo", "Listado", "ParametrosCorreo"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Especialidades", "Listado", "Especialidades"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Mentores", "Listado", "Mentores"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Usuarios", "Listado", "Usuarios"));

            return objElementoPrincipal;

        }

        public static Cls_Modelo_Menu Crear_Menu_Mentor() {
            Cls_Modelo_Menu objElementoPrincipal;
            objElementoPrincipal = Ayudante_Menu.Crear_Menu_Nivel1("Mentor");
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Mi Cuenta", "MiCuenta","Responder"));
            objElementoPrincipal.Elementos.Add(Ayudante_Menu.Crear_Menu_Nivel2("Mis Preguntas", "Listado_Preguntas", "Responder"));
            return objElementoPrincipal;
        }


        public static Cls_Modelo_Menu CrearMenuPreguntasSinLeer(int total_preguntas_sin_leer)
        {
            Cls_Modelo_Menu cls_Modelo_Menu;

            cls_Modelo_Menu = new Cls_Modelo_Menu();
            cls_Modelo_Menu.Nombre = "Preguntas sin leer";
            cls_Modelo_Menu.Accion = "Listado_Preguntas";
            cls_Modelo_Menu.Controlador = "Responder";
            cls_Modelo_Menu.dato = total_preguntas_sin_leer.ToString();
            return cls_Modelo_Menu;
        }
    }
}