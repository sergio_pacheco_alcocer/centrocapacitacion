﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Util
{
    public class Cls_Sesiones
    {
        public static string FECHA_ACTUALIZACION = "17/09/2021";


        public static string NOMBRE_ROL
        {
            set { HttpContext.Current.Session["Nombre_Rol"] = value; }
            get
            {
                if (HttpContext.Current.Session["Nombre_Rol"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["Nombre_Rol"].ToString();
            }
        }

        public static string EMAIL
        {
            set { HttpContext.Current.Session["Email"] = value; }
            get
            {
                if (HttpContext.Current.Session["Email"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["Email"].ToString();
            }
        }

        public static int USUARIO_ID
        {
            set
            {
                HttpContext.Current.Session["UsuarioID"] = value;
            }
            get
            {
                if (HttpContext.Current.Session["UsuarioID"] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session["UsuarioID"]);
            }

        }






        public static int USUARIO_VISITANTE_ID
        {
            set
            {
                HttpContext.Current.Session["Visitante_ID"] = value;
            }
            get
            {
                if (HttpContext.Current.Session["Visitante_ID"] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session["Visitante_ID"]);
            }

        }

        public static string USUARIO_VISITANTE_NOMBRE
        {
            set { HttpContext.Current.Session["Visitante_Nombre"] = value; }
            get
            {
                if (HttpContext.Current.Session["Visitante_Nombre"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["Visitante_Nombre"].ToString();
            }
        }
    }
}