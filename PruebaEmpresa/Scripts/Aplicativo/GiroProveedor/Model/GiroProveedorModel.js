﻿var Modelo_Giro_Proveedor = function () {

    var _Giro_Proveedor_ID = ko.observable(''),
        _Nombre_Giro_Proveedor = ko.observable('').extend({ required: true })
        .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' })
            .extend({ maxLength: 199 }),
        _Estatus = ko.observable('').extend({ required: true });

    return {
        Giro_Proveedor_ID: _Giro_Proveedor_ID,
        Nombre_Giro_Proveedor: _Nombre_Giro_Proveedor,
        Estatus: _Estatus
    }
}