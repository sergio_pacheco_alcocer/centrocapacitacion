﻿
var TablaGiroProveedor = $("#Tabla_Giro_Proveedor");


$(function () {

    Inicializar_Tabla();
});


function Inicializar_Tabla() {
    TablaGiroProveedor.bootstrapTable('destroy');
    TablaGiroProveedor.bootstrapTable({ url: 'Obtener_Giros_Proveedores' });

}


function Botones_Operacion_Formato_Eliminar(value, row, index) {
    return [
        '<a class="eliminar" href="javascript:void(0)" title="Eliminar">',
        '<i class="glyphicon glyphicon-trash"></i>',
        '</a>  '
    ].join('');
}

function Botones_Operacion_Formato_Editar(value, row, index) {
    return [
        '<a class="editar" href="javascript:void(0)" title="Editar">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>'
    ].join('');
}


window.Botones_Operacion_Evento = {

    'click .eliminar': function (e, value, row, index) {

        var ventanaEliminar = $('#VentanaEliminar');
        $('#SpanDato').html('<b> ' + row.Nombre_Giro_Proveedor + ' </b>');

        ventanaEliminar.modal();
        RenglonSeleccionado = row;


    },
    'click .editar': function (e, value, row, index) {
        ObjGiroProveedorViewModel.Editar_Elemento(row);
    }


};


function Eliminar_Registro() {
    var ventanaEliminar = $('#VentanaEliminar');
    ventanaEliminar.modal('hide');
    ObjGiroProveedorViewModel.Borrar_Elemento(RenglonSeleccionado);
}