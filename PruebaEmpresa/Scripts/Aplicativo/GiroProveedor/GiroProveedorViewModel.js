﻿var ObjGiroProveedorViewModel;

$(function () {

    ObjGiroProveedorViewModel = new giroProveedorViewModel();
    ObjGiroProveedorViewModel.ModeloGiroProveedor.errors = ko.validation.group(ObjGiroProveedorViewModel.ModeloGiroProveedor);
    ko.applyBindings(ObjGiroProveedorViewModel, $("#Interfaz_Giro_Proveedor")[0]);
});

function giroProveedorViewModel() {


    var self = this;
    ko.validation.locale('es-ES');
    "use strict";


    self.ModeloGiroProveedor = Modelo_Giro_Proveedor();
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.TipoOperacion = ko.observable('');
    self.ValidarDatos = ko.observable(true);


    //Ventanas *******************************************

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };

    self.Abrir_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal();
    };

    self.Cerrar_Ventana_Interfaz = function () {
        var ventanaInterfaz = $('#Ventana_Interfaz')
        ventanaInterfaz.modal('hide');
    };

    // Busquedas *************************************************
    self.Buscar_Datos = function () {
        TablaGiroProveedor.bootstrapTable('refresh');
    };

    self.Limpiar_Cajas = function () {

        self.ModeloGiroProveedor.Giro_Proveedor_ID('0');
        self.ModeloGiroProveedor.Nombre_Giro_Proveedor('');
        self.ModeloGiroProveedor.Estatus('ACTIVO');
        self.ModeloGiroProveedor.errors.showAllMessages(false);

    };

    self.Asignar_Valores = function (p_Row) {
        self.ModeloGiroProveedor.Giro_Proveedor_ID(p_Row.Giro_Proveedor_ID);
        self.ModeloGiroProveedor.Nombre_Giro_Proveedor(p_Row.Nombre_Giro_Proveedor);
        self.ModeloGiroProveedor.Estatus(p_Row.Estatus);
        self.ModeloGiroProveedor.errors.showAllMessages(false);
    }

    // Operaciones ***********************************************

    self.Nuevo_Elemento = function () {
        self.Abrir_Ventana_Interfaz();
        self.Limpiar_Cajas();
    };

    self.Editar_Elemento = function (p_Row) {
        self.Limpiar_Cajas();
        self.Asignar_Valores(p_Row);
        self.Abrir_Ventana_Interfaz();
    };

    self.Borrar_Elemento = function (p_Row) {
        self.Asignar_Valores(p_Row);
        self.TipoOperacion("Eliminar");
        self.ValidarDatos(false);
        self.Procesar_Operacion();
    };

    self.Aceptar_Operacion = function () {
        self.TipoOperacion('Guardar');
        self.ValidarDatos(true);
        self.Procesar_Operacion();
    };


    self.Procesar_Operacion = function () {

        var strDatos;

        if (self.ValidarDatos()) {

            if (self.ModeloGiroProveedor.errors().length > 0) {
                self.ModeloGiroProveedor.errors.showAllMessages();
                $.toast({
                    heading: 'Catálogo de Giros Proveedor',
                    text: 'Se requiere un valores',
                    position: 'top-right',
                    stack: false
                });
                return;
            }
        }



        strDatos = ko.toJSON(self.ModeloGiroProveedor);
        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    self.Buscar_Datos();
                    self.Cerrar_Ventana_Interfaz();
                    $.toast({
                        heading: 'Catálogo de Giro Proveedor',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });
                }
                else {
                    $.toast({
                        heading: 'Catálogo de Giro Proveedor',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.toast({
                    heading: '' + result.status + ' ' + result.statusText,
                    text: response.Mensaje,
                    position: 'top-right',
                    stack: false,
                    icon: 'error'
                });

            }

        });

    };


}