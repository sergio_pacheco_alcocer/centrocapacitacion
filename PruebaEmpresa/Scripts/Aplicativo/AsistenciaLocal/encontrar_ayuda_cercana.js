﻿
var ID_Mentor_Seleccionado = 0;

$(document).ready(function () {
    
    eventos();

    consultar_mentores();
});



function eventos() {

    $('#btn_enviar_pregunta_mentor').on('click', function (e) {
        e.preventDefault();

        if ($('#txt_pregunta_mentor').val() != "") {

            try {

                var obj_filtro = new Object();
                obj_filtro.Descripcion_Pregunta = $('#txt_pregunta_mentor').val();
                obj_filtro.Usuario_Mentor_ID = ID_Mentor_Seleccionado;

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj_filtro) });

                jQuery.ajax({
                    type: 'POST',
                    url: '/AsistenciaLocal/Registrar_Pregunta',
                    contentType: "application/json; charset=utf-8",
                    data: $data,
                    dataType: "text",
                    async: false,
                    cache: false,
                    success: function (datos) {                        

                        if (datos != "completado") {

                            bootbox.alert(datos);

                        } else {

                            bootbox.alert("<h3 style='width:100%; text-aling:center;'>El mensaje ha sido enviado con éxito<br><br>Te notificaremos por correo electrónico en cuanto el mentor te conteste, igual puedes revisarlo desde tu sección de panel de control en la plataforma</h3>");
                        }

                        $('#modal_pregunta_mentor').modal('hide');
                    },
                    error: function (datos) {
                        alert(datos.responseText);
                    }
                });


            } catch (e) {
                alert('Technical report [validar_login]', e);
            }  

        } else {

            bootbox.alert("Favor de llenar el campo del mensaje");
        }
    });

    
    $('#btn_consultar_mentores').on('click', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: ($("#encabezado_mentores").offset().top - 85)
        }, 500);
        
    });


    $('#btn_guia_pregunta').on('click', function (e) {
        e.preventDefault();

        $('#modal_guia_pregunta').modal('show');
    });

}



function consultar_mentores() {

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/AsistenciaLocal/Consultar_Especialidades',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                
                var lista_especialidades = [];

                $('#lista_especialidades').empty();
                $('#lista_mentores').empty();


                lista_especialidades.push(datos[0]);
                $('#lista_especialidades').append('<a class="list-group-item list-group-item-action active" id="list-' + datos[0].Especialidad_ID + '-list" data-toggle="list" href="#list-' + datos[0].Especialidad_ID + '" role="tab" aria-controls="' + datos[0].Especialidad_ID + '">' + datos[0].Nombre_Especialidad + '</a>');

                for (var i = 1; i < datos.length; i++) {

                    lista_especialidades.push(datos[i]);
                    $('#lista_especialidades').append('<a class="list-group-item list-group-item-action" id="list-' + datos[i].Especialidad_ID + '-list" data-toggle="list" href="#list-' + datos[i].Especialidad_ID + '" role="tab" aria-controls="' + datos[i].Especialidad_ID + '">' + datos[i].Nombre_Especialidad + '</a>');
                }                



                jQuery.ajax({
                    type: 'POST',
                    url: '/AsistenciaLocal/Consultar_Mentores',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (mentores) {                        

                        for (var i = 0; i < lista_especialidades.length; i++) {

                            var contenido = '';

                            if (i == 0) {
                                contenido += '<div class="tab-pane fade show active" id="list-' + lista_especialidades[i].Especialidad_ID + '" role="tabpanel" aria-labelledby="list-' + lista_especialidades[i].Especialidad_ID + '-list">' +
                                    '<div class="list-group">';
                            } else {
                                contenido += '<div class="tab-pane fade" id="list-' + lista_especialidades[i].Especialidad_ID + '" role="tabpanel" aria-labelledby="list-' + lista_especialidades[i].Especialidad_ID + '-list">' +
                                    '<div class="list-group">';
                            }
                                                       
                            for (var j = 0; j < mentores.length; j++) {

                                if (mentores[j].Especialidad_ID == lista_especialidades[i].Especialidad_ID) {
                                
                                    var lineaImagen = "";

                                    if (mentores[j].Nombre_Imagen != null && mentores[j].Nombre_Imagen.length > 0) {
                                        lineaImagen = '<div class="col-lg-3"><img src="../../Imagenes/' + mentores[j].Nombre_Imagen + '" height="150" style="width:100%;"/></div>';
                                    } else {
                                        lineaImagen = '<div class="col-lg-3"><img src="../../Estructura/imagenes/icons/usuario.jpg" height="150" style="width:100%;"/></div>';
                                    }

                                    contenido += '<div class="list-group-item list-group-item-action flex-column align-items-start">' +
                                                    '<div class="row">' +
                                                         lineaImagen +
                                                        '<div class="col-lg-6"> <h3> ' + mentores[j].Nombre + ' ' + mentores[j].Apellido_Paterno + ' ' + mentores[j].Apellido_Materno + '</h3></div>' +
                                                        '<div class="col-lg-3">' +
                                                            '<br><button class="btn btn-primary" onClick="hacer_pregunta_click(' + mentores[j].Usuarios_ID + ')">Hacer Pregunta</button>' +
                                                        '</div>' +
                                                    '</div>'+
                                                '</div>';
                                }
                            }

                            contenido += '</div></div>';
                            $('#lista_mentores').append(contenido);
                                                        
                        }
                        
                    },
                    error: function (datos_m) {
                        alert('Consultar_Mentores' + datos_m.responseText);
                    }
                });

            },
            error: function (datos) {
                alert('Consultar_Especialidades: ' + datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [consultar_especialidades]', e);
    }
}




function hacer_pregunta_click(id_mentor) {

    try {

        ID_Mentor_Seleccionado = id_mentor;

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Verificar_Login',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos.mensaje != "correcto") {

                    bootbox.confirm({
                        title: "Inicio de sesión requerido",
                        message: "Para poder hacer preguntas a nuestros mentores es necesario estar registrado e iniciar sesión <br>¿Deseas iniciar sesión ahora?",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> Cancelar'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Confirmar'
                            }
                        },
                        callback: function (result) {

                            if (result) {

                                window.location.href = "/UsuariosVisitantes/Login";
                            }
                        }
                    });

                } else {
                    
                    $('#txt_pregunta_mentor').val("");
                    $('#txt_pregunta_mentor').focus();
                    $('#modal_pregunta_mentor').modal('show');
                }
            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [validar_login]', e);
    }    
}





function validar_login() {

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Verificar_Login',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos.mensaje != "correcto") {

                    bootbox.confirm({
                        title: "<br>Inicio de sesión requerido",
                        message: "Para poder hacer preguntas a nuestros mentores es necesario estar registrado e iniciar sesión <br>¿Deseas iniciar sesión ahora?",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> Cancelar'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Confirmar'
                            }
                        },
                        callback: function (result) {

                            if(result){

                                window.location.href = "/UsuariosVisitantes/Login";
                            }
                        }
                    });                    

                }
            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [validar_login]', e);
    }
}