﻿
var TablaProveedores = $("#Tabla_Proveedores");


$(function () {


});


function Inicializar_Tabla() {
    TablaProveedores.bootstrapTable('destroy');
    TablaProveedores.bootstrapTable({
        url: 'Obtener_Proveedores',
        queryParams: function (p) {
            return {
                limit: p.limit,
                offset: p.offset,
                giro_proveedor_id: ObjBuscarProveedorViewModel.BuscarProveedorModel.Giro_Proveedor_Selected() !== undefined ? ObjBuscarProveedorViewModel.BuscarProveedorModel.Giro_Proveedor_Selected().id : null,
                busqueda: ObjBuscarProveedorViewModel.BuscarProveedorModel.Busqueda()
            }
        }
    });
}


function botonMostrarDetalle(value, row, index) {

    return [
      '<a class="detalle" href="javascript:void(0)" title="Mostrar Detalle">',
      '<i class="glyphicon glyphicon-list-alt"></i>',
      '</a>  '
    ].join('');
}

window.botonesEvento = {

    'click .detalle': function (e, value, row, index) {
        ObjBuscarProveedorViewModel.mostrarDetalle(row);
    }
}