﻿
var ObjBuscarProveedorViewModel;

$(function () {
    ObjBuscarProveedorViewModel = new buscarProveedorViewModel();
    ObjBuscarProveedorViewModel.BuscarProveedorModel.errors = ko.validation.group(ObjBuscarProveedorViewModel.BuscarProveedorModel);
    ko.applyBindings(ObjBuscarProveedorViewModel, $("#Interfaz_Buscar_Proveedor")[0]);

    ObjBuscarProveedorViewModel.Cargar_Informacion();
});



function buscarProveedorViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.BuscarProveedorModel = Buscar_Proveedor_Model();
    self.ArregloGirosIndustriales = ko.observableArray([]);


    // Busquedas *************************************************
    self.Cargar_Informacion = function () {
        self.ArregloGirosIndustriales($.parseJSON(Datos_Giro_Proveedor));
        Inicializar_Tabla();
    }

    self.buscar = function () {

        if (self.BuscarProveedorModel.errors().length > 0) {
            self.BuscarProveedorModel.errors.showAllMessages();
            $.toast({
                heading: 'Búsqueda Proveedor',
                text: 'Algunos valores son incorrectos',
                position: 'top-right',
                stack: false
            });
            return;
        }

        TablaProveedores.bootstrapTable('refresh');
    }

    self.mostrarDetalle = function (row) {
        window.location.href = URL_Proveedor_Detalle + "?Proveedor_ID=" + row.Proveedor_ID;
    }

}