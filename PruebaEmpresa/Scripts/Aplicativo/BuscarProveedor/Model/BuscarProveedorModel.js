﻿
var Buscar_Proveedor_Model = function () {

    var _Giro_Proveedor_Selected = ko.observable(''),
        _Giro_Proveedor_ID = ko.observable(''),
        _Busqueda = ko.observable('').extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-()&.' });

    return {
        Giro_Proveedor_Selected: _Giro_Proveedor_Selected,
        Giro_Proveedor_ID: _Giro_Proveedor_ID,
        Busqueda: _Busqueda
    }

}