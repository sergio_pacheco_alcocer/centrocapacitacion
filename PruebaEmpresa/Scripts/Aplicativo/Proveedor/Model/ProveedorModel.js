﻿var Proveedor_Model = function () {

    var _Proveedor_ID = ko.observable(''),
        _Giro_Proveedor_ID = ko.observable(''),
        _Nombre_Proveedor = ko.observable('').extend({ required: true })
           .extend({ maxLength: 250 })
           .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-().' }),
        _Colonia = ko.observable('').extend({ required: true })
            .extend({ maxLength: 250 })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-().' }),
        _Calle = ko.observable('').extend({ required: true })
            .extend({ maxLength: 250 })
            .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-().' }),
        _Numero_Exterior = ko.observable('').extend({ required: true })
              .extend({ maxLength: 50 })
              .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-().' }),
        _Numero_Interior = ko.observable('')
          .extend({ maxLength: 50 })
          .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-().' }),
        _Sitio_WEB = ko.observable('').extend({
            pattern: {
                message: 'se requiere una url valida',
                params: '^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$'
            }
        }),
        _Telefono = ko.observable('').extend({ required: true })
                     .extend({ digit: true })
                     .extend({ minLength: 10 })
                     .extend({ maxLength: 10 }),
        _Giro_Proveedor_Selected = ko.observable('').extend({ required: true }),
        _Estatus = ko.observable('').extend({ required: true }),
        _Correo_Electronico = ko.observable('').extend({ email: true })
            .extend({ maxLength: 50 }),
        _Nombre_Giro_Proveedor = ko.observable(''),
        _Que_Hace_la_Empresa = ko.observable('').extend({ required: true })
           .extend({ maxLength: 3000 })
           .extend({ caracterPermitido: 'ÁÉÍÓÚáéíóúñÑ_/-().' })
       _Latitud = ko.observable('0'),
       _Longitud = ko.observable('0');


    return {
        Proveedor_ID: _Proveedor_ID,
        Giro_Proveedor_ID: _Giro_Proveedor_ID,
        Nombre_Proveedor: _Nombre_Proveedor,
        Colonia: _Colonia,
        Calle: _Calle,
        Numero_Exterior: _Numero_Exterior,
        Numero_Interior: _Numero_Interior,
        Sitio_WEB: _Sitio_WEB,
        Telefono: _Telefono,
        Giro_Proveedor_Selected: _Giro_Proveedor_Selected,
        Estatus: _Estatus,
        Correo_Electronico: _Correo_Electronico,
        Nombre_Giro_Proveedor: _Nombre_Giro_Proveedor,
        Que_Hace_la_Empresa: _Que_Hace_la_Empresa,
        Latitud: _Latitud,
        Longitud: _Longitud
    }
}

var BusquedaMapa = function () {

    var _Latitud = ko.observable(''),
        _Longitud = ko.observable('');
    
    return {
        Latitud: _Latitud,
        Longitud: _Longitud
    }
}