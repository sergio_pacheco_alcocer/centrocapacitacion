﻿var options = {
    map: ".map_canvas",
    country: 'MX',
    details: "form ul",
    detailsAttribute: "data-geo",
    markerOptions: {
        draggable: true
    }
}

$(function () {

    Iniciar_Mapa();
});


function Aceptar_Busqueda_Mapa() {
    ObjProveedorViewModel.aceptarBusquedaMapa();
}

function Iniciar_Mapa() {

    $("#geocomplete").geocomplete(options).bind("geocode:result", function (event, result) {
        console.log("Result: " + result.formatted_address);
        ObjProveedorViewModel.borrarCoordenadasBusquedas();
    }).bind("geocode:error", function (event, status) {
        console.log("ERROR: " + status);
    }).bind("geocode:multiple", function (event, results) {
        console.log("Multiple: " + results.length + " results found");
        ObjProveedorViewModel.borrarCoordenadasBusquedas();
        //$("#multiple>li").each(function () {
        //    $(this).remove();
        //});

        //$.each(results, function () {
        //    var result = this;
        //    $("<li>")
        //      .html(result.formatted_address)
        //      .appendTo($multiple)
        //      .click(function () {
        //          $("#geocomplete").geocomplete("update", result)
        //      });
        //});

    });

    $("#geocomplete").bind("geocode:dragged", function (event, latLng) {
        ObjProveedorViewModel.ModeloBuscarMapa.Latitud(latLng.lat());
        ObjProveedorViewModel.ModeloBuscarMapa.Longitud(latLng.lng());
    });

}