﻿var ObjProveedorViewModel;


$(function () {
    ObjProveedorViewModel = new proveedorViewModel();
    ObjProveedorViewModel.ModeloProveedor.errors = ko.validation.group(ObjProveedorViewModel.ModeloProveedor);
    ko.applyBindings(ObjProveedorViewModel, $("#Interfaz_Registrar_Proveedor")[0]);

    ObjProveedorViewModel.Cargar_Informacion();
});


function proveedorViewModel() {

    var self = this;
    ko.validation.locale('es-ES');
    "use strict";

    self.ModeloProveedor = Proveedor_Model();
    self.ArregloGirosIndustriales = ko.observableArray([]);
    self.ArregloEstatus = ko.observableArray(['ACTIVO', 'INACTIVO']);
    self.TipoOperacion = ko.observable('Guardar');
    self.ValidarDatos = ko.observable(true);
    self.habilitado = ko.observable(false);
    self.ModeloBuscarMapa = BusquedaMapa();

    //Ventanas *******************************************

    self.Abrir_Ventana_Espera = function () {

        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal();

    };

    self.Cerrar_Ventana_Espera = function () {
        var ventantaEspera = $('#Ventana_Espera');
        ventantaEspera.modal('hide');
    };


    self.Abri_Ventana_Mapa = function () {
        var ventanaMapa = $('#Ventana_Mapa');
        ventanaMapa.modal();
    }

    self.Cerrar_Venata_Mapa = function () {
        var ventanaMapa = $('#Ventana_Mapa');
        ventanaMapa.modal('hide');
    }


    // Busquedas *************************************************
    self.Cargar_Informacion = function () {
        self.ArregloGirosIndustriales($.parseJSON(Datos_Giro_Proveedor));

        self.ModeloProveedor.Proveedor_ID(Modelo_Proveedor_Servidor.Proveedor_ID);
        self.ModeloProveedor.Giro_Proveedor_ID(Modelo_Proveedor_Servidor.Giro_Proveedor_ID);
        self.ModeloProveedor.Nombre_Proveedor(Modelo_Proveedor_Servidor.Nombre_Proveedor);
        self.ModeloProveedor.Colonia(Modelo_Proveedor_Servidor.Colonia);
        self.ModeloProveedor.Calle(Modelo_Proveedor_Servidor.Calle);
        self.ModeloProveedor.Numero_Exterior(Modelo_Proveedor_Servidor.Numero_Exterior);
        self.ModeloProveedor.Numero_Interior(Modelo_Proveedor_Servidor.Numero_Interior);
        self.ModeloProveedor.Sitio_WEB(Modelo_Proveedor_Servidor.Sitio_WEB);
        self.ModeloProveedor.Telefono(Modelo_Proveedor_Servidor.Telefono);
        self.ModeloProveedor.Estatus(Modelo_Proveedor_Servidor.Estatus);
        self.ModeloProveedor.Que_Hace_la_Empresa(Modelo_Proveedor_Servidor.Que_Hace_la_Empresa);
        self.ModeloProveedor.Correo_Electronico(Modelo_Proveedor_Servidor.Correo_Electronico);
        self.ModeloProveedor.Latitud(Modelo_Proveedor_Servidor.Latitud);
        self.ModeloProveedor.Longitud(Modelo_Proveedor_Servidor.Longitud);

        self.ModeloProveedor.Giro_Proveedor_Selected(self.ArregloGirosIndustriales.find('id', { id: self.ModeloProveedor.Giro_Proveedor_ID() }));


        self.ModeloProveedor.errors.showAllMessages(false);
  
    }

    self.Limpiar_Cajas = function () {

        self.ModeloProveedor.Proveedor_ID(0);
        self.ModeloProveedor.Giro_Proveedor_ID('');
        self.ModeloProveedor.Nombre_Proveedor('');
        self.ModeloProveedor.Colonia('');
        self.ModeloProveedor.Calle('');
        self.ModeloProveedor.Numero_Exterior('');
        self.ModeloProveedor.Numero_Interior('');
        self.ModeloProveedor.Sitio_WEB('');
        self.ModeloProveedor.Telefono('');
        self.ModeloProveedor.Estatus('');
        self.ModeloProveedor.Correo_Electronico('');
        self.ModeloProveedor.Giro_Proveedor_Selected(self.ArregloGirosIndustriales.find('name', { name: 'Selecciona' }));

        


        self.ModeloProveedor.errors.showAllMessages(false);

    }

    self.borrarCoordenadasBusquedas = function () {
        self.ModeloBuscarMapa.Longitud('');
        self.ModeloBuscarMapa.Latitud('');
    }


    self.aceptarBusquedaMapa = function () {

        var latitud = self.ModeloBuscarMapa.Latitud();
        var longitud = self.ModeloBuscarMapa.Longitud();

        if (latitud.length == 0 || longitud.length == 0) {

            $.toast({
                heading: 'Catálogo Proveedor',
                text: "Seleccione una ubicación",
                position: 'top-right',
                stack: false,
                icon: 'error'
            });

            return;
        }

        self.ModeloProveedor.Latitud(latitud);
        self.ModeloProveedor.Longitud(longitud);

        self.Cerrar_Venata_Mapa();
    }

    self.abrirBusquedaMapa = function () {

        var latitud = self.ModeloProveedor.Latitud();
        var longitud = self.ModeloProveedor.Longitud();

        if (parseInt(latitud) != 0 && parseInt(longitud) != 0) {
            var lat_and_long = latitud + ', ' + longitud;
            $("#geocomplete").geocomplete("find", lat_and_long);
        } else {
            $("#geocomplete").geocomplete("find", "Avenida Juárez, Zona Centro, Irapuato, Gto., México");
        }

        self.Abri_Ventana_Mapa();
    }

    self.guardar = function () {

        var strDatos;

        if (self.ModeloProveedor.errors().length > 0) {
            self.ModeloProveedor.errors.showAllMessages();
            $.toast({
                heading: 'Catálogo Proveedor',
                text: 'Se requiere un valores',
                position: 'top-right',
                stack: false
            });
            return;
        } 
        self.ModeloProveedor.Giro_Proveedor_ID(self.ModeloProveedor.Giro_Proveedor_Selected().id);
        strDatos = ko.toJSON(self.ModeloProveedor);

        self.Abrir_Ventana_Espera();

        $.ajax({
            type: "POST",
            url: self.TipoOperacion(),
            data: strDatos,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                self.Cerrar_Ventana_Espera();
                if (response.Mensaje === "bien") {
                    $.toast({
                        heading: 'Catálogo Proveedor',
                        text: 'Proceso Correcto',
                        position: 'top-right',
                        stack: false,
                        icon: 'success'
                    });

                    window.location.href = Url_Regresar_Listado;
                }
                else {
                    $.toast({
                        heading: 'Catálogo Proveedor',
                        text: response.Mensaje,
                        position: 'top-right',
                        stack: false,
                        icon: 'error'
                    });
                }

            },
            error: function (result) {
                self.Cerrar_Ventana_Espera();
                $.toast({
                    heading: '' + result.status + ' ' + result.statusText,
                    text: response.Mensaje,
                    position: 'top-right',
                    stack: false,
                    icon: 'error'
                });

            }

        });

    }

}