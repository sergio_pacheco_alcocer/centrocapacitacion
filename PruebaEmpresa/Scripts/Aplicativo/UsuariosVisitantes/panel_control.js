﻿
$(document).ready(function () {

    validar_login();
    eventos();

    consultar_respuestas_nuevas();
    consultar_respuestas_contestadas();
    consultar_preguntas_pendientes();
    adecuar_clases_responsive();
});


$(window).resize(function () {
    adecuar_clases_responsive();
});


function adecuar_clases_responsive() {

    if (screen.width <= 840) {

        $("#columna_contenido").removeClass("col-8");
        $("#columna_clasificaciones").removeClass("col-4");

        $("#columna_contenido").addClass("col-12");
        $("#columna_clasificaciones").addClass("col-12");
        
    } else {

        $("#columna_contenido").removeClass("col-12");
        $("#columna_clasificaciones").removeClass("col-12");

        $("#columna_contenido").addClass("col-8");
        $("#columna_clasificaciones").addClass("col-4");
    }
}





function eventos() {

    $('#modal_conultar_mensaje').on('hidden.bs.modal', function () {

        if ($("#hid_respuesta_id").val() != "") {

            try {

                var obj_filtro = new Object();
                obj_filtro.Pregunta_Respuesta_ID = parseInt($('#hid_respuesta_id').val(), 10);

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj_filtro) });

                jQuery.ajax({
                    type: 'POST',
                    url: '/UsuariosVisitantes/Actualizar_Respuesta_Visto',
                    contentType: "application/json; charset=utf-8",
                    data: $data,
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos.mensaje != "correcto") {

                            alert(datos.mensaje);
                        }

                        consultar_respuestas_nuevas();
                        consultar_respuestas_contestadas();
                        consultar_preguntas_pendientes();
                    },
                    error: function (datos) {
                        alert(datos.responseText);
                    }
                });


            } catch (e) {
                alert('Technical report [validar_login]', e);
            }
        }

        $('#hid_respuesta_id').val("");

    })
}



function mostrar_mensaje(row,tipo) {  

    $('#txt_respuesta').val("");

    if(tipo=="nueva")
        $('#hid_respuesta_id').val(row.Pregunta_Respuesta_ID);

    if(tipo!="pendiente")
        $('#txt_respuesta').val(row.Respuesta);

    $('#txt_pregunta').val(row.Descripcion_Pregunta);
    $('#lbl_especialidad').text("Especialidad: " + row.Nombre_Especialidad);
    $('#lbl_nombre_mentor').text(row.Nombre_Mentor_Completo);
    $('#lbl_fecha').text(row.Fecha_Creo.substring(0,10));
    $('#modal_conultar_mensaje').modal('show');
}



function consultar_respuestas_nuevas() {

    $('#lista_respuestas_nuevas').empty();

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Consultar_Respuestas_Nuevas',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {

                if (datos.length == 0) {
                    $(".notificacion").hide();
                    $('#lista_respuestas_nuevas').append("<p>Aún no tienes respuestas nuevas, nuestros mentores contestarán tus dudas en cuanto les sea posible. Gracias por tu paciencia</p>");
                } else {
                    $(".notificacion").show();
                    $("#notificacion_recibidas").text(datos.length);
                }

                for (var i = 0; i < datos.length; i++) {

                    var row = new Object();
                    row.Pregunta_Respuesta_ID = datos[i].Pregunta_Respuesta_ID;
                    row.Usuario_Visitante_ID = datos[i].Usuario_Visitante_ID;
                    row.Respuesta = datos[i].Respuesta;
                    row.Nombre_Mentor_Completo = datos[i].Nombre_Mentor_Completo;
                    row.Nombre_Especialidad = datos[i].Nombre_Especialidad;
                    row.Fecha_Creo = datos[i].Fecha_Creo;
                    row.Descripcion_Pregunta = datos[i].Descripcion_Pregunta;

                    var label_respuesta = row.Respuesta;
                    if (row.Respuesta.length >= 81)
                        label_respuesta = row.Respuesta.substring(0, 80) + "...";  

                    var label_pregunta = row.Descripcion_Pregunta;
                    if (row.Descripcion_Pregunta.length >= 65)
                        label_pregunta = row.Descripcion_Pregunta.substring(0, 64) + "...";


                    var contenido = "<a onclick='mostrar_mensaje(" + JSON.stringify(row) + ",\"nueva\");' class='list-group-item list-group-item-action flex-column align-items-start'> " +
                                    "<div class='d-flex w-100 justify-content-between'> "+
                                        "<h4 style='font-weight:bold; font-size:20px'>" + row.Nombre_Mentor_Completo + "</h4> "+
                                        "<small>" + row.Fecha_Creo.substring(0,10) + "</small> "+
                                    "</div > "+
                                    "<small style='font-size:15px; font-weight:bold;'>Especialidad: " + row.Nombre_Especialidad + "</small> "+
                                    "<br /> <br/ >"+
                                    "<small style='font-size:17px;'>Tu pregunta: " + label_pregunta + "</small> " +
                                    "<br />" +
                                    "<small style='font-size:15px;'>" + label_respuesta + "</small> "+
                                "</a>";

                    $('#lista_respuestas_nuevas').append(contenido);
                }                

            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [consultar_respuestas_nuevas]', e);
    }
}



function consultar_respuestas_contestadas() {

    $('#lista_respuestas_contestadas').empty();

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Consultar_Respuestas_Leidas',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {

                if (datos.length == 0) {
                    $('#lista_respuestas_contestadas').append("<p>Aún no tienes respuestas leídas, nuestros mentores contestarán tus dudas en cuanto les sea posible. Gracias por tu paciencia</p>");
                }


                for (var i = 0; i < datos.length; i++) {

                    var row = new Object();
                    row.Pregunta_Respuesta_ID = datos[i].Pregunta_Respuesta_ID;
                    row.Usuario_Visitante_ID = datos[i].Usuario_Visitante_ID;
                    row.Respuesta = datos[i].Respuesta;
                    row.Nombre_Mentor_Completo = datos[i].Nombre_Mentor_Completo;
                    row.Nombre_Especialidad = datos[i].Nombre_Especialidad;
                    row.Fecha_Creo = datos[i].Fecha_Creo;
                    row.Descripcion_Pregunta = datos[i].Descripcion_Pregunta;

                    var label_respuesta = row.Respuesta;
                    if (row.Respuesta.length >= 81)
                        label_respuesta = row.Respuesta.substring(0, 80) + "...";

                    var label_pregunta = row.Descripcion_Pregunta;
                    if (row.Descripcion_Pregunta.length >= 65)
                        label_pregunta = row.Descripcion_Pregunta.substring(64, 80) + "...";


                    var contenido = "<a onclick='mostrar_mensaje(" + JSON.stringify(row) + ",\"leida\");' class='list-group-item list-group-item-action flex-column align-items-start'> " +
                        "<div class='d-flex w-100 justify-content-between'> " +
                        "<h4 style='font-weight:bold; font-size:20px'>" + row.Nombre_Mentor_Completo + "</h4> " +
                        "<small>" + row.Fecha_Creo.substring(0, 10) + "</small> " +
                        "</div > " +
                        "<small style='font-size:15px; font-weight:bold;'>Especialidad: " + row.Nombre_Especialidad + "</small> " +
                        "<br /> <br/ >" +
                        "<small style='font-size:17px;'>Tu pregunta: " + label_pregunta + "</small> " +
                        "<br />" +
                        "<small style='font-size:15px;'>" + label_respuesta + "</small> " +
                        "</a>";

                    $('#lista_respuestas_contestadas').append(contenido);
                }

            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [consultar_respuestas_contestadas]', e);
    }
}


function consultar_preguntas_pendientes() {

    $('#lista_preguntas_pendientes').empty();

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Consultar_Preguntas_Pendientes',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {

                if (datos.length == 0) {                    
                    $('#lista_preguntas_pendientes').append("<p>Aún no tienes preguntas pendientes, puedes hacer una nueva pregunta desde el boton \"Hacer una nueva pregunta\" en la parte superior</p>");
                } 

                for (var i = 0; i < datos.length; i++) {

                    var row = new Object();
                    row.Pregunta_ID = datos[i].Pregunta_Respuesta_ID;
                    row.Descripcion_Pregunta = datos[i].Descripcion_Pregunta;
                    row.Nombre_Mentor_Completo = datos[i].Nombre_Mentor_Completo;                    
                    row.Nombre_Especialidad = datos[i].Nombre_Especialidad;
                    row.Fecha_Creo = datos[i].Fecha_Creo;
                                        

                    var label_pregunta = row.Descripcion_Pregunta;
                    if (row.Descripcion_Pregunta.length >= 65)
                        label_pregunta = row.Descripcion_Pregunta.substring(0, 64) + "...";


                    var contenido = "<a onclick='mostrar_mensaje(" + JSON.stringify(row) + ",\"pendiente\");' class='list-group-item list-group-item-action flex-column align-items-start'> " +
                        "<div class='d-flex w-100 justify-content-between'> " +
                        "<h4 style='font-weight:bold; font-size:20px'>" + row.Nombre_Mentor_Completo + "</h4> " +
                        "<small>" + row.Fecha_Creo.substring(0, 10) + "</small> " +
                        "</div > " +
                        "<small style='font-size:15px; font-weight:bold;'>Especialidad: " + row.Nombre_Especialidad + "</small> " +
                        "<br /> <br/ >" +
                        "<small style='font-size:17px;'>Tu pregunta: " + label_pregunta + "</small> " +                        
                        "</a>";

                    $('#lista_preguntas_pendientes').append(contenido);
                }

            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [consultar_preguntas_pendientes]', e);
    }
}






function validar_login() {

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Verificar_Login',            
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos.mensaje != "correcto") {

                    window.location.href = "/UsuariosVisitantes/Login";

                }
            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [validar_login]', e);
    }
}