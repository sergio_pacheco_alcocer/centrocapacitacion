﻿
$(document).ready(function () {
    
    eventos();
});

function eventos() {

    $('#btn_login').on('click', function (e) {

        e.preventDefault();
        iniciar_sesion();
    });


    $('#btn_registrarse').on('click', function (e) {

        e.preventDefault();
        registrarse();
    });    


    $('#txt_telefono_rg').keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        
        return (
            key == 8 ||
            key == 9 ||
            key == 13 ||
            key == 46 ||
            key == 110 ||
            key == 190 ||
            (key >= 35 && key <= 40) ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));
    });
}



function validar_registro() {

    var resultado = "";

    if ($("#txt_mail_rg").val() == "") {
        resultado += "- El correo es un dato obligatorio<br>"
    } else {

        //validar mail
        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test($("#txt_mail_rg").val())==false) {
            resultado += "- Correo electronico no válido<br>";
        }        
    }

    if ($("#txt_clave_rg").val() == "")
        resultado += "- La clave es un dato obligatorio<br>";

    if($("#txt_nombre_rg").val()=="")
        resultado += "- El nombre es un dato obligatorio<br>";

    if($("#txt_apellido_p_rg").val()=="")
        resultado += "- El apellido paterno es un dato obligatorio<br>";

    if($("#txt_apellido_m_rg").val()=="")
        resultado += "- El apellido materno es un dato obligatorio<br>";

    if($("#txt_telefono_rg").val()=="")
        resultado += "- El télefono es un dato obligatorio<br>";

    return resultado;
}




function registrarse() {

    var validacion = validar_registro();
    if (validacion != "") {
        bootbox.alert("Favor de ingresar los datos necesarios: <br><br>" + validacion);
        return false;
    }



    var obj_filtro = new Object();
    
    obj_filtro.Correo_Electronico = $("#txt_mail_rg").val();
    obj_filtro.Password = $("#txt_clave_rg").val();
    obj_filtro.Nombre = $("#txt_nombre_rg").val();
    obj_filtro.Apellido_Paterno = $("#txt_apellido_p_rg").val();
    obj_filtro.Apellido_Materno = $("#txt_apellido_m_rg").val();
    obj_filtro.Telefono = $("#txt_telefono_rg").val();
    obj_filtro.Nombre_Empresa = $("#txt_empresa_rg").val();
    obj_filtro.Rol_ID = 2;

    var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj_filtro) });

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Registrar_Usuario',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: $data,
            async: true,
            cache: false,
            success: function (datos) {

                if (datos.mensaje == "correcto") {

                    window.location.href = Ruta_Panel_Control;

                } else {

                    alert(datos.mensaje);
                }
            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [validar_login]', e);
    }
}


function iniciar_sesion() {

    var obj_filtro = new Object();
    obj_filtro.Correo_Electronico = $("#txt_mail").val();
    obj_filtro.Password = $("#txt_clave").val();

    var $data = JSON.stringify({ 'jsonObject': JSON.stringify(obj_filtro) });

    try {

        jQuery.ajax({
            type: 'POST',
            url: '/UsuariosVisitantes/Consultar_Usuario_Visitante',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: $data,
            async: true,
            cache: false,
            success: function (datos) {                

                if (datos.mensaje != undefined) {

                    alert(datos.mensaje);

                } else {
                    window.location.href = "/UsuariosVisitantes/Panel_Control";
                }
            },
            error: function (datos) {
                alert(datos.responseText);
            }
        });


    } catch (e) {
        alert('Technical report [validar_login]', e);
    }
}


function mostrar_login() {

    $("#frm_login").slideDown();
    $("#frm_registro").slideUp();

    $(".contenedor_login").css("height", "460px");
    $(".fondo_gris").css("height", "500px");
}

function mostrar_registro() {
    $("#frm_registro").slideDown();
    $("#frm_login").slideUp();

    $(".contenedor_login").css("height", "850px");
    $(".fondo_gris").css("height", "850px");    
}