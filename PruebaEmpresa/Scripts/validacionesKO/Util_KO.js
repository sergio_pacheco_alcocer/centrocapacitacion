﻿
ko.observableArray.fn.find = function (prop, data) {
    var valueToMatch = data[prop];
    return ko.utils.arrayFirst(this(), function (item) {
        return item[prop] === valueToMatch;
    });
};


ko.bindingHandlers.disableClick = {
    init: function (element, valueAccessor) {
        $(element).click(function (evt) {
            if (valueAccessor())
                evt.preventDefault();
        });

    },

    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        ko.bindingHandlers.css.update(element, function () { return { disabled_anchor: value }; });
    }
};

ko.bindingHandlers.stopBubble = {
    init: function (element) {
        ko.utils.registerEventHandler(element, "change", function (event) {
            event.preventDefault();
            event.cancelBubble = true;
            if (event.stopPropagation) {
                event.stopPropagation();
            }
        });
    }
};

ko.extenders.upperCase = function (target, options) {
    //create a writable computed observable to intercept writes to our observable
    var result = ko.computed({
        read: function () {
            console.log("extenders.upperCase.read")
            return target(); //always return the original observables value
        },
        write: function (newValue) {
            console.log("extenders.upperCase.write");
            var current = target() || "";
            console.log("\tCurrent", current, "newValue", newValue);
            var capitalized = newValue.toUpperCase();
            if (capitalized !== current) {
                console.log("\tChanged", current, "=>", capitalized);
                target(capitalized); // Unfortunately, this will also move the cursor to the end of the field!
            } else {
                //if the capitalized value is the same, but a different value was written, 
                // force a notification for the current field
                if (newValue !== current) {
                    console.log("\tNotifying subscribers");
                    target.notifySubscribers(capitalized);
                }
            }
        }
    });

    //initialize with current value
    result(target());

    //return the new computed observable
    return result;
};