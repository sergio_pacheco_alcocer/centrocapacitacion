﻿
ko.validation.rules['es_fecha_menor'] = {

    validator: function (fecha, fecha2) {
        var xMes = fecha.substring(3, 5);
        var xDia = fecha.substring(0, 2);
        var xAnio = fecha.substring(6, 10);
        var yMes = fecha2.substring(3, 5);
        var yDia = fecha2.substring(0, 2);
        var yAnio = fecha2.substring(6, 10);

        if (xAnio > yAnio) {
            return (true);
        } else {
            if (xAnio == yAnio) {
                if (xMes > yMes) {
                    return (true);
                }
                if (xMes == yMes) {
                    if (xDia >= yDia) {
                        return (true);
                    } else {
                        return (false);
                    }
                } else {
                    return (false);
                }
            } else {
                return (false);
            }

        }

    },
    message: 'La fecha no puede ser menor'

}


ko.validation.rules['es_igual'] = {

    validator: function (password_confirmar, password) {
        return password_confirmar == password;
    },
    message:'La contraseña no coincide'


}



ko.validation.rules['numero_alumnos'] = {

    validator: function (cadena, objeto) {

        var respuesta = true;

        if (objeto !== undefined) {

            if (objeto == "SI") {

                if (isNaN(cadena) == false) {
                    // significa que es número o puede estar vacia
                    if (cadena == "")
                        respuesta = false;

                    var numero = parseInt(cadena);
                    if (numero <= 0)
                        respuesta = false;

                }
                else {
                    respuesta = false;
                }

            }
        }


        return respuesta;
    },
    message:'Se requiere número de alumnos'

}

ko.validation.rules['rango_numero_enteros'] = {
  
    validator: function (cadena, objeto) {

        var respuesta = true;
        var numero1;
        var numero2;

        if (objeto.length == 0) {
            return respuesta;
        }

        // significa que hay un número
        if (isNaN(objeto) == false) {
            numero1 = parseFloat(objeto);
            if (cadena.length == 0) {
                respuesta = false;
            } else {
                numero2 = parseInt(cadena);
                if (numero2 < numero1) {
                    respuesta = false;
                }
            }

        }
        return respuesta;
    } ,
    message: 'rango de números incorrecto'
}


ko.validation.rules['rango_numeros_decimales'] = {

    validator: function (cadena, objeto) {
        var respuesta = true;
        var numero1;
        var numero2;


        if (objeto.length == 0) {
            return respuesta;
        }

        // significa que hay un número
        if (isNaN(objeto) == false) {
            numero1 = parseFloat(objeto);
            if (cadena.length==0) {
                respuesta = false;
            } else {
                numero2 = parseFloat(cadena);
                if (numero2 < numero1) {
                    respuesta = false;
                }
            }

        }

        return respuesta;
    },
    message:'rango de números incorrecto'
}

ko.validation.rules['estado_seleccionado'] = {

    validator: function (objetoCiudad, objetoEstado) {
        var respuesta = true;

        if (objetoEstado !== undefined  && objetoEstado !== null) {

            if (objetoEstado.id.length > 0 && objetoCiudad === undefined) {
                respuesta = false;
            }
        }

        return respuesta;
    },
    message:'Selecciona una ciudad'


}




ko.validation.rules['tiene_seleccion_id'] = {
 
    validator: function (cadena, cadena_id) {
        var respuesta = true;

        if (cadena_id === undefined || cadena_id.length == 0) {
            respuesta = false;
        }

        return respuesta;
    },
    message:'Sel. elemento'

}

ko.validation.rules['lectura_anterior'] = {

    validator: function (cadena, objeto) {
        var respuesta = true;

        if (objeto == null || objeto === undefined) {
            respuesta = false;
        }

        if (cadena == null || objeto === undefined) {
            respuesta = false;
        }
            

        var lecturaActual = parseInt(cadena);
        var lecturaAnterior = parseInt(objeto);

        var consumo = lecturaActual - lecturaAnterior;
        if (consumo < 0) {
            respuesta = false;
        }
            

        return respuesta;
    }
     ,
    message: 'Letura Nueva debe ser Mayor o igual a la lectura Anterior'

}

ko.validation.rules['tiene_seleccion'] = {

    validator: function (cadena, objeto) {
        var respuesta = true;

        if (objeto == null || objeto === undefined)
        {
            respuesta = false;
        }
        return respuesta;
    }
     ,
    message: 'Selecciona un Elemento de la Lista'
}


ko.validation.rules['esFecha'] = {

    validator: function (cadena) {

        if (cadena.length == 0) {
            return true;
        }

        var fecha = new String(cadena);
        var ano = new String(fecha.substring(fecha.lastIndexOf("/") + 1, fecha.length));
        var mes = new String(fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/")));
        var dia = new String(fecha.substring(0, fecha.indexOf("/")));


        if (mes.length == 0 || dia.length == 0) {
            return false;
        }


        // valida si el año
        if (isNaN(ano) || ano.length < 4 || parseFloat(ano) < 1900) {
            return false;
        }


        if (isNaN(mes) || parseFloat(mes) < 1 || parseFloat(mes) > 12) {
            return false;
        }


        if (isNaN(dia) || parseInt(dia, 10) < 1 || parseInt(dia, 10) > 31) {
            return false;
        }


        if (parseInt(mes) == 4 || parseInt(mes) == 6 || parseInt(mes) == 9 || parseInt(mes) == 11) {
            if (parseInt(dia) > 30) {
                return false;
            }
        }

        // para el año biciesto
        if (parseInt(ano) % 4 == 0) {
            // si es biciesto
            if (parseInt(dia) > 29 && parseInt(mes) == 2) {
                return false;
            }
        }
        else {
            // nos biciesto
            if (parseInt(dia) > 28 && parseInt(mes) == 2) {
                return false;
            }
        }

        return true;

    }
    ,
    message: 'Formato fecha Invalida'
};

ko.validation.rules['solo_numeros'] = {
    validator: function (cadena, cadendaExtra) {

        var respuesta = true;
        var letra = "";
        var enter = "\n";
        var caracteres, i;
        caracteres = "1234567890" + enter + cadendaExtra;

        if (cadena == undefined) {
            return respuesta;
        }

        for (i = 0; i < cadena.length; i++) {
            letra = cadena.substring(i, i + 1);
            if (caracteres.indexOf(letra) == -1) {
                respuesta = false;
                return respuesta;
            }
        }

        return respuesta;

    },
    message: 'Carecter No Permitido'


};

ko.validation.rules['caracterPermitido'] = {
    validator: function (cadena, cadendaExtra) {

        var respuesta = true;
        var letra = "";
        var enter = "\n";
        var caracteres, i;
        caracteres = "abcdefghijklmnopqrstuvwxyz1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ." + enter + cadendaExtra;

        if (cadena == undefined) {
            return respuesta;
        }

        for (i = 0; i < cadena.length; i++) {
            letra = cadena.substring(i, i + 1);
            if (caracteres.indexOf(letra) == -1) {
                respuesta = false;
                return respuesta;
            }
        }

        return respuesta;

    },
    message:'Carecter No Permitido'

};
ko.validation.registerExtenders();
