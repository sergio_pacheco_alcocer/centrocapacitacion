﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;
using PagedList;
using PruebaEmpresa.Servicios.Interfaces;
using PruebaEmpresa.Infraestructura.Permisos;
using PruebaEmpresa.Infraestructura.Enums;

namespace PruebaEmpresa.Controllers
{


    [SessionExpireAttribute]
    [RoleAuthorize(Roles_Enum.Mentor)]
    public class ResponderController : Controller
    {

        IPreguntas_Repo Preguntas_Repo;
        IPreguntas_Repuesta_Repo Preguntas_Repuesta_Repo;
        IUsuarios_Repo Usuarios_Repo;
        ICorreo_Repo Correo_Repo;

        public ResponderController(IPreguntas_Repo preguntas_Repo,
            IPreguntas_Repuesta_Repo preguntas_Repuesta_Repo, ICorreo_Repo correo_Repo)
        {
            Preguntas_Repo = preguntas_Repo;
            Preguntas_Repuesta_Repo = preguntas_Repuesta_Repo;
            Correo_Repo = correo_Repo;
        }

        // GET: Responder
        public ActionResult MiCuenta()
        {

            return RedirectToAction("Registrar", "Mentores" , new { Usuarios_ID = Cls_Sesiones.USUARIO_ID });
        }


        [HttpPost]
        public ActionResult CrearRespuesta(int Pregunta_ID, string Respuesta , string Nombre_Mentor)
        {
            string status = "";
            string mensaje_correo = "";
            try
            {

                PreguntasRespuestaView preguntasRespuestaView = new PreguntasRespuestaView();
                preguntasRespuestaView.Pregunta_ID = Pregunta_ID;
                preguntasRespuestaView.Usuario_ID = Cls_Sesiones.USUARIO_ID;
                preguntasRespuestaView.Respuesta = Respuesta;
                preguntasRespuestaView.Tipo = "Mentor";
                preguntasRespuestaView.Usuario_Creo = Nombre_Mentor;
                preguntasRespuestaView.Visto_Respuesta = false;
            
                Preguntas_Repuesta_Repo.Guardar(preguntasRespuestaView);
                status = "Proceso Exitoso";
                Preguntas_Repo.Actualizar_Visto_Pregunta(Pregunta_ID);

                mensaje_correo = Correo_Repo.Enviar_Correo_Respuesta(Pregunta_ID, Respuesta);

            }
            catch (Exception ex) {
                status = ex.Message;
            }

            return RedirectToAction("Contestar", new {  Pregunta_ID, message = status });
        }

        public ActionResult Contestar(int? Pregunta_ID, string message)
        {
            PreguntasView preguntasView;
            RespuestasView respuestasView = new RespuestasView();
            try
            {

                ViewBag.Mensaje = message;

                if (Pregunta_ID.HasValue) {
                    preguntasView = Preguntas_Repo.Obtener_Una_Pregunta(Pregunta_ID.Value);
                    respuestasView.preguntasRespuestaViews = Preguntas_Repuesta_Repo.Obtener_Respuestas(Pregunta_ID.Value);
                }
                else
                {
                    preguntasView = new PreguntasView();
                }

                respuestasView.preguntasView = preguntasView;
              

            }
            catch (Exception ex) {

            }


            return View(respuestasView);
        }

        public ActionResult Listado_Preguntas(string sortOrder, string currentFilter, string searchString,  string currentFechaInicio, string FechaInicio, string currentFechaFin  ,string FechaFin, int? page) {

            IQueryable<PreguntasView> listaPreguntas;
            int pageSize = 10;
            int pageNumber=0;
            try
            {
            
                ViewBag.CurrentSort = sortOrder;
                ViewBag.FechaSortParam = String.IsNullOrEmpty(sortOrder) ? "fecha_asc" : "";
                ViewBag.UsuarioSortParam = sortOrder == "Usuario" ? "usuario_desc" : "Usuario";
                ViewBag.RespuestaSortParam = sortOrder == "Respuesta" ? "respuesta_desc" : "Respuesta";
                ViewBag.RespondidoSortParam = sortOrder == "Respondido" ? "respondido_desc" : "Respondido";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                if (FechaInicio != null)
                {
                    page = 1;
                }
                else
                {
                    FechaInicio = currentFechaInicio;
                }


                if (FechaFin != null)
                {
                    page = 1;
                }
                else
                {
                    FechaFin = currentFechaFin;
                }


                ViewBag.CurrentFechaInicio = FechaInicio;
                ViewBag.CurrentFechaFin = FechaFin;
                ViewBag.CurrentFilter = searchString;

                var dbPreguntas = Preguntas_Repo.Ope_Preguntas.Where(x => x.Usuario_Mentor_ID == Cls_Sesiones.USUARIO_ID);



                if (!String.IsNullOrEmpty(searchString))
                {

                    dbPreguntas = dbPreguntas.Where(x => x.Cat_Usuarios.Nombre.Contains(searchString) || x.Cat_Usuarios.Apellido_Paterno.Contains(searchString) || x.Cat_Usuarios.Apellido_Materno.Contains(searchString));
                }

                if (!String.IsNullOrEmpty(FechaInicio))
                {
                    // buscamos por fecha inicio
                    // tenemos que convertir a date time
                    DateTime dateTimeIncio = Cls_Util.ConvertirCadenaFecha(FechaInicio);
                    dbPreguntas = dbPreguntas.Where(x => x.Fecha_Creo >= dateTimeIncio);
                }


                if (!String.IsNullOrEmpty(FechaFin))
                {
                    // buscamos por fecha fin
                    DateTime dateTimeFin = Cls_Util.ConvertirCadenaFecha(FechaFin);
                    DateTime dateTimeFinMas = dateTimeFin.AddHours(23).AddMinutes(59).AddSeconds(59);
                    dbPreguntas = dbPreguntas.Where(x => x.Fecha_Creo <= dateTimeFinMas);
                }



                switch (sortOrder)
                {
                    case "fecha_asc":
                        dbPreguntas = dbPreguntas.OrderBy(x => x.Fecha_Creo);
                        break;
                    case "Respondido":
                        dbPreguntas = dbPreguntas.OrderBy(x => x.Visto);
                        break;
                    case "Respuesta":
                        dbPreguntas = dbPreguntas.OrderBy(x => x.Ope_Preguntas_Respuestas.Count);
                        break;
                    case "Usuario":
                        dbPreguntas = dbPreguntas.OrderBy(x => x.Cat_Usuarios.Nombre)
                      .ThenBy(x => x.Cat_Usuarios.Apellido_Paterno)
                      .ThenBy(x => x.Cat_Usuarios.Apellido_Materno);
                        break;
                    case "usuario_desc":
                        dbPreguntas = dbPreguntas.OrderByDescending(x => x.Cat_Usuarios.Nombre)
                            .ThenByDescending(x => x.Cat_Usuarios.Apellido_Paterno)
                            .ThenByDescending(x => x.Cat_Usuarios.Apellido_Materno);
                        break;
                    case "respuesta_desc":
                        dbPreguntas = dbPreguntas.OrderByDescending(x => x.Ope_Preguntas_Respuestas.Count);
                        break;
                    case "respondido_desc":
                        dbPreguntas = dbPreguntas.OrderByDescending(x => x.Visto);
                        break;
                    default:
                        dbPreguntas = dbPreguntas.OrderByDescending(x => x.Fecha_Creo);
                        break;
                }

                
                 pageNumber = (page ?? 1);

                listaPreguntas = dbPreguntas.Select(x => new PreguntasView
                { 
                    Pregunta_ID = x.Pregunta_ID,
                    Descripcion_Pregunta = x.Descripcion_Pregunta,
                    Fecha_Creo = x.Fecha_Creo,
                    Nombre_Mentor = (x.Cat_Usuarios_Mentor.Nombre + " " + x.Cat_Usuarios_Mentor.Apellido_Paterno) + " " + (x.Cat_Usuarios_Mentor.Apellido_Materno ?? ""),
                    Nombre_Usuario = (x.Cat_Usuarios.Nombre + " " + x.Cat_Usuarios.Apellido_Paterno) + " " + (x.Cat_Usuarios.Apellido_Materno ?? ""),
                    Visto = x.Visto,
                    Visto_Cadena = x.Visto ? "SI": "NO",
                    Usuario_ID = x.Usuario_ID,
                    Usuario_Mentor_ID = x.Usuario_Mentor_ID,
                    Total_Respuestas = x.Ope_Preguntas_Respuestas.Count
                });

            }
            catch(Exception ex)
            {
                listaPreguntas = Enumerable.Empty<PreguntasView>().AsQueryable();
            }
            return View(listaPreguntas.ToPagedList(pageNumber,pageSize));
        }
    }
}