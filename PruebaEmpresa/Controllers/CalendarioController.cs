﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using Stripe;
using Stripe.Checkout;
using PruebaEmpresa.Util;
using PruebaEmpresa.Contexto;
using System.Data.Entity;
using System.Configuration;
using System.IO;
using System.Net;

namespace PruebaEmpresa.Controllers
{
    public class CalendarioController : Controller
    {
        ICargos_Repo Cargos_Repo;
        IUsuarios_Repo Usuarios_Repo;
        IRegistroPagos_Repo RegistroPagos_Repo;
        IProveedores_Repo Proveedores_Repo;


        public CalendarioController(ICargos_Repo cargos_Repo, IUsuarios_Repo usuarios_Repo, IRegistroPagos_Repo registroPagos_Repo,
            IProveedores_Repo proveedores_Repo)
        {
            Cargos_Repo = cargos_Repo;
            Usuarios_Repo = usuarios_Repo;
            RegistroPagos_Repo = registroPagos_Repo;
            Proveedores_Repo = proveedores_Repo;
        }


        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ActualizarVigencia()
        {

            try
            {
                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["stripeSecretkey"];
                Stream req = Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string json = await new StreamReader(req).ReadToEndAsync();

                Event stripeEvent;

                //var stripeEvent = EventUtility.ParseEvent(json);   
               var webhookSecret = ConfigurationManager.AppSettings["webhooks"];
                        stripeEvent = EventUtility.ConstructEvent(
                         json,
                          Request.Headers["Stripe-Signature"],
                         webhookSecret
                         );

              Console.WriteLine($"Webhook notification with type: {stripeEvent.Type} found for {stripeEvent.Id}");

                switch (stripeEvent.Type)
                {
                    case Events.InvoicePaid:
                        Invoice invoice = stripeEvent.Data.Object as Invoice;
                        string customerID = invoice.CustomerId;
                     

                        foreach( InvoiceLineItem item in invoice.Lines)
                        {
                            string productoID = item.Plan.Id;
                            string subscriptionID = item.Subscription;

                            CargosView cargosView = Cargos_Repo.Obtener_Un_Cargo(productoID);
                            UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario_Customer(customerID);

                            DateTime datFechaInicio = DateTime.Now;
                            DateTime datFechaFin = DateTime.Now.AddDays(cargosView.Dias);

                            RegistroPagosView registroPagosView = new RegistroPagosView();
                            registroPagosView.Cargo_ID = cargosView.Cargo_ID;
                            registroPagosView.Identificador = subscriptionID;
                            registroPagosView.Usuarios_ID = usuariosView.Usuarios_ID;
                            registroPagosView.Usuario_Creo = "plataforma stripe";
                            registroPagosView.Fecha_Inicio_Vigencia = datFechaInicio;
                            registroPagosView.Fecha_Fin_Vigencia = datFechaFin;

                            using (EFDbContext context = new EFDbContext())
                            {
                                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                                {

                                    try
                                    {
                                       
                                        RegistroPagos_Repo.Guardar(context, registroPagosView);
                                        Proveedores_Repo.Actualizar_Vigencia_Proveedor(context, usuariosView.Usuarios_ID, datFechaInicio, datFechaFin);

                                        transaction.Commit();
                                        
                                    }
                                    catch (Exception ex)
                                    {
                                       
                                        transaction.Rollback();
                                    }

                                }
                            }

                        }


                        break;
                    case Events.CustomerSubscriptionCreated:
                        Subscription subscription = stripeEvent.Data.Object as Subscription;

                        foreach(SubscriptionItem lista in subscription.Items)
                        {
                           string plan_id =  lista.Plan.Id;
                        }
                  
                        break;
                    case Events.CustomerCreated:
                        Customer customer = stripeEvent.Data.Object as Customer;
                        break;
                    default:
                        break;
                }


                

            }
            catch(Exception ex)
            {

                return new BadRequest(ex.Message);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);  // OK = 200

        }

        // GET: Calendario
        public ActionResult Index()
        {
            return View();
        }
    }
}