﻿using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Infraestructura.Permisos;
using PruebaEmpresa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PruebaEmpresa.Servicios.Interfaces;
using PruebaEmpresa.Util;

namespace PruebaEmpresa.Controllers
{

    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class UsuariosController : Controller
    {
        IUsuarios_Repo Usuarios_Repo;
        private IRoles_Repo Roles_Repo;
        ICombo_Repo Combo_Repo;

        public UsuariosController(IUsuarios_Repo usuarios_Repo, IRoles_Repo roles_Repo, ICombo_Repo combo_Repo) {
            Usuarios_Repo = usuarios_Repo;
            Roles_Repo = roles_Repo;
            Combo_Repo = combo_Repo;
        }

        public ActionResult Listado(string sortOrder, string currentFilter, string searchString, int? currentRol, int?Rol_ID, int? page)
        {
            IQueryable<UsuariosView> Lista_Usuarios;
            int pageSize = 10;
            int pageNumber = 0;

            try
            {

                ViewBag.Rol_ID = new SelectList(Roles_Repo.Obtener_Roles_Combo(), "id", "name", currentRol);

                ViewBag.CurrentSort = sortOrder;
                ViewBag.NombreSortParam = String.IsNullOrEmpty(sortOrder) ? "nombre_desc" : "";
                ViewBag.ApellidoPaternoSortParam = sortOrder == "Apellido_Paterno" ? "apellido_paterno_desc" : "Apellido_Paterno";
                ViewBag.CorreSortParam = sortOrder == "Correo" ? "correo_desc" : "Correo";
                ViewBag.EstatusSortParam = sortOrder == "Estatus" ? "estatus_desc" : "Estatus";



                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }


                if (Rol_ID.HasValue)
                {
                    page = 1;
                }
                else
                {
                    Rol_ID = currentRol;
                }


                ViewBag.CurrentRol = Rol_ID;
                ViewBag.CurrentFilter = searchString;

                pageNumber = (page ?? 1);
                Lista_Usuarios = Usuarios_Repo.Obtener_Usuarios(sortOrder, searchString, Rol_ID);
            }
            catch(Exception ex)
            {
                Lista_Usuarios = Enumerable.Empty<UsuariosView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }

            return View(Lista_Usuarios.ToPagedList(pageNumber, pageSize));
        }


        public ActionResult Registrar(int? Usuarios_ID)
        {
            ViewBag.Mensaje = "";
            UsuariosView usuariosView = new UsuariosView();
            try
            {

                if (Usuarios_ID.HasValue)
                {
                    usuariosView = Usuarios_Repo.Obtener_Usuario(Usuarios_ID.Value);
                    ViewBag.Rol_ID = new SelectList(Roles_Repo.Obtener_Roles_Combo(), "id", "name",usuariosView.Rol_ID);
                    ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name", usuariosView.Estatus);
                }
                else
                {
                    ViewBag.Rol_ID = new SelectList(Roles_Repo.Obtener_Roles_Combo(), "id", "name");
                    ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name");
                }

            }catch(Exception ex)
            {

            }


            return View(usuariosView);
        }

        public ActionResult Guardar(UsuariosView usuariosView)
        {
            ViewBag.Rol_ID = new SelectList(Roles_Repo.Obtener_Roles_Combo(), "id", "name", usuariosView.Rol_ID);
            ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name", usuariosView.Estatus);
            ViewBag.Mensaje = "";

            if (ModelState.IsValid)
            {
                try
                {

                    if(Usuarios_Repo.Esta_Repetido_Usuario(usuariosView.Correo_Electronico,usuariosView.Usuarios_ID) == "SI")
                    {
                        ModelState.AddModelError("error", "El correo escrito ya esta registrado");
                        return View("Registrar", usuariosView);
                    }

                    usuariosView.Usuario_Creo = Cls_Sesiones.EMAIL;
                    Usuarios_Repo.Registrar_Usuario(usuariosView);

                    return RedirectToAction("Listado", "Usuarios");

                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("error", ex.Message);
                }
            }
            return View("Registrar", usuariosView);
        }

        public ActionResult Eliminar(int? Usuarios_ID)
        {
            UsuariosView usuariosView = new UsuariosView();

            try
            {

                if(Usuarios_ID.HasValue)
                    usuariosView = Usuarios_Repo.Obtener_Usuario(Usuarios_ID.Value);
            }
            catch(Exception ex)
            {

            }


            return View(usuariosView);
        }

        [HttpPost]
        public ActionResult Eliminar(int Usuarios_ID)
        {
            UsuariosView usuariosView = new UsuariosView();

            try
            {
                string mensaje = Usuarios_Repo.Eliminar_Usuario(Usuarios_ID);
                if (mensaje == "bien")
                {
                    return RedirectToAction("Listado", "Usuarios");
                }
                else
                {
                    usuariosView = Usuarios_Repo.Obtener_Usuario(Usuarios_ID);
                    ModelState.AddModelError("error", mensaje);
                }

            }
            catch(Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
            }

            return View(usuariosView);
        }

    }
}