﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;

namespace PruebaEmpresa.Controllers
{
    public class ProveedorController : Controller
    {

        IGiroProveedor_Repo GiroProveedor_Repo;
        IProveedores_Repo Proveedores_Repo;

        public ProveedorController(IGiroProveedor_Repo giroProveedor_Repo, IProveedores_Repo proveedores_Repo)
        {
            this.GiroProveedor_Repo = giroProveedor_Repo;
            this.Proveedores_Repo = proveedores_Repo;
        }

        // GET: Proveedor
        public ActionResult Index()
        {
            return View();
        }


        [SessionExpireAttribute]
        [Authorize]
        public ActionResult Listado()
        {
            int Usuario_ID = Cls_Sesiones.USUARIO_ID;
            List<ProveedorView> Lista_Proveedor = Proveedores_Repo.Obtener_Proveedores(Usuario_ID);
            return View(Lista_Proveedor);
        }

        /// <summary>
        /// Para registrar un proveedor este requiere autorización
        /// </summary>
        /// <param name="Proveedor_ID">proveedor id</param>
        /// <returns> una vista </returns>
        /// 
        [SessionExpireAttribute]
        [Authorize]
        public ActionResult Registrar(int? Proveedor_ID)
        {
            ProveedorView proveedorView = new ProveedorView();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                List<ComboView> listaGirosProovedores = GiroProveedor_Repo.Obtener_Giros_Industriales();
                ViewBag.Lista_Giros_Proveedores = serializer.Serialize(listaGirosProovedores);

                if (Proveedor_ID.HasValue)
                    proveedorView = Proveedores_Repo.Obtener_Un_Proveedor(Proveedor_ID.Value);
                else
                    proveedorView = new ProveedorView();

            }catch(Exception ex)
            {

            }

            return View(proveedorView);

        
        }

        /// <summary>
        /// Buscar Proveedor la teoría dice que es para todos los usuarios , no requiere autorización
        /// </summary>
        /// <returns> una vista </returns>
        public ActionResult Buscar()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                List<ComboView> listaGirosProovedores = GiroProveedor_Repo.Obtener_Giros_Industriales();
                ViewBag.Lista_Giros_Proveedores = serializer.Serialize(listaGirosProovedores);

            }
            catch (Exception ex) {

            }


            return View();
        }

        public ActionResult Detalle(int? Proveedor_ID) {
            ProveedorView proveedorView;


            if (Proveedor_ID.HasValue)
                proveedorView = Proveedores_Repo.Obtener_Un_Proveedor(Proveedor_ID.Value);
            else
                proveedorView = new ProveedorView();

            return View(proveedorView);
        }

        /// <summary>
        /// Obtiene los proeveedores es una petición ajax
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="giro_proveedor_id"></param>
        /// <param name="busqueda"></param>
        /// <returns></returns>
        public string Obtener_Proveedores(int offset, int limit, int? giro_proveedor_id, string busqueda = null)
        {
            Cls_Paginado<ProveedorView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try {

                Paginado = Proveedores_Repo.Obtener_Proveedores(offset, limit, giro_proveedor_id, busqueda);
                str_datos = serializer.Serialize(Paginado);


            }
            catch(Exception ex)
            {

            }

            return str_datos;

        }

        /// <summary>
        /// Guarda los proveedores si requiere autorización petición ajax
        /// </summary>
        /// <param name="proveedorView"></param>
        /// <returns> una cadena </returns>
        /// 
        [SessionExpireAttribute]
        [Authorize]
        [HttpPost]
        public string Guardar(ProveedorView proveedorView)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";


            try
            {
                proveedorView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                Proveedores_Repo.Guardar(proveedorView);
                objMensajeServidor.Mensaje = "bien";

            }
            catch(Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }
            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;
        }
    }
}