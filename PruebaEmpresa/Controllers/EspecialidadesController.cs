﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Web.Script.Serialization;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;
using PruebaEmpresa.Servicios.Interfaces;
using PruebaEmpresa.Infraestructura.Permisos;

namespace PruebaEmpresa.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]

    public class EspecialidadesController : Controller
    {
        IEspecialidad_Repo Especialidad_Repo;
        ICombo_Repo Combo_Repo;

        public EspecialidadesController(IEspecialidad_Repo especialidad_Repo, ICombo_Repo combo_Repo)
        {
            Especialidad_Repo = especialidad_Repo;
            Combo_Repo = combo_Repo;
        }

        // GET: Especialidades
        public ActionResult Listado(string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NombreEspecialidadSortParm = String.IsNullOrEmpty(sortOrder) ? "nombre_especialidad_desc" : "";
            ViewBag.EstatusSortParm = sortOrder == "Estatus" ? "estatus_desc" : "Estatus";


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var dbEspecialidad = Especialidad_Repo.Cat_Especialidades;


            if (!String.IsNullOrEmpty(searchString))
            {
                dbEspecialidad = dbEspecialidad.Where(s => s.Nombre_Especialidad.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "nombre_especialidad_desc":
                    dbEspecialidad = dbEspecialidad.OrderByDescending(x => x.Nombre_Especialidad);
                    break;
                case "Estatus":
                    dbEspecialidad = dbEspecialidad.OrderBy(x => x.Estatus);
                    break;
                case "estatus_desc":
                    dbEspecialidad = dbEspecialidad.OrderByDescending(x => x.Estatus);
                    break;
                default:
                    dbEspecialidad = dbEspecialidad.OrderBy(x => x.Nombre_Especialidad);
                    break;
            }


            int pageSize = 5;
            int pageNumber = (page ?? 1);


            var listaEspecialidad = dbEspecialidad.Select(x => new EspecialidadViewModel
            {
                Especialidad_ID = x.Especialidad_ID,
                Estatus = x.Estatus,
                Nombre_Especialidad = x.Nombre_Especialidad

            });


            return View(listaEspecialidad.ToPagedList(pageNumber,pageSize));
        }


        public ActionResult Registrar(int? Especialidad_ID)
        {
            EspecialidadViewModel especialidadViewModel = new EspecialidadViewModel();

            try
            {
        

                if (Especialidad_ID.HasValue)
                {
                    especialidadViewModel = Especialidad_Repo.Obtener_Una_Especialidad(Especialidad_ID.Value);
                    ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name",especialidadViewModel.Estatus);
                }
                else {
                    especialidadViewModel = new EspecialidadViewModel();
                    ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name");
                }

            }
            catch (Exception ex) {

            }


            return View(especialidadViewModel);
        }


        public ActionResult Guardar(EspecialidadViewModel especialidadViewModel)
        {
            ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name", especialidadViewModel.Estatus);

            if (ModelState.IsValid)
            {
                try
                {
                    especialidadViewModel.Usuario_Creo = Cls_Sesiones.EMAIL;

                    if(Especialidad_Repo.Esta_Repetido_Especialidad(especialidadViewModel.Nombre_Especialidad, especialidadViewModel.Especialidad_ID)== "SI")
                    {
                        ModelState.AddModelError("error", "Nombre de la Especialidad Repetida");
                        return View("Registrar", especialidadViewModel);
                    }

                    Especialidad_Repo.Guardar(especialidadViewModel);

                    return RedirectToAction("Listado", "Especialidades");

                }
                catch (Exception ex) {
                    ModelState.AddModelError("error", ex.Message);
                }
            }

            return View("Registrar", especialidadViewModel);
        }

        public ActionResult Eliminar(int? Especialidad_ID)
        {
            EspecialidadViewModel especialidadViewModel = new EspecialidadViewModel();

            try
            {

                if (Especialidad_ID.HasValue)
                    especialidadViewModel = Especialidad_Repo.Obtener_Una_Especialidad(Especialidad_ID.Value);
                else
                    especialidadViewModel = new EspecialidadViewModel();

            }catch(Exception ex)
            {

            }

            return View(especialidadViewModel);
        }


        [HttpPost]
        public ActionResult Eliminar(int Especialidad_ID)
        {
            EspecialidadViewModel especialidadViewModel = new EspecialidadViewModel();

            try {

                string mensaje = Especialidad_Repo.Eliminar(Especialidad_ID);

                if (mensaje == "bien")
                {
                    return RedirectToAction("Listado", "Especialidades");
                }
                else {
                    ModelState.AddModelError("error", mensaje);
                    especialidadViewModel = Especialidad_Repo.Obtener_Una_Especialidad(Especialidad_ID);
                }


            }catch(Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
            }

            return View(especialidadViewModel);
        }
    }
}