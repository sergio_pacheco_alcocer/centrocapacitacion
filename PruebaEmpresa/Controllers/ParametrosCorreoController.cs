﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Infraestructura.Permisos;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;


namespace PruebaEmpresa.Controllers
{

    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class ParametrosCorreoController : Controller
    {
        IParametroCorreo_Repo ParametroCorreo_Repo;

        public ParametrosCorreoController(IParametroCorreo_Repo parametroCorreo_Repo)
        {
            ParametroCorreo_Repo = parametroCorreo_Repo;
        }

        // GET: ParametrosCorreo
        public ActionResult Listado()
        {
            List<ParametrosCorreoView> Lista_Parametros_Correo = ParametroCorreo_Repo.Obtener_Parametros_Correo();
            return View(Lista_Parametros_Correo);
        }



        public ActionResult Registrar(int? Parametro_Correo_ID)
        {
            ParametrosCorreoView parametrosCorreoView = new ParametrosCorreoView();
            try
            {

                if (Parametro_Correo_ID.HasValue)
                    parametrosCorreoView = ParametroCorreo_Repo.Obtener_Un_Parametro(Parametro_Correo_ID.Value);
                else
                    parametrosCorreoView = new ParametrosCorreoView();

            }catch(Exception ex)
            {

            }

            return View(parametrosCorreoView);
        }


        public ActionResult Eliminar(int? Parametro_Correo_ID)
        {
            ParametrosCorreoView parametrosCorreoView = new ParametrosCorreoView();
            try
            {

                if (Parametro_Correo_ID.HasValue)
                    parametrosCorreoView = ParametroCorreo_Repo.Obtener_Un_Parametro(Parametro_Correo_ID.Value);
                else
                    parametrosCorreoView = new ParametrosCorreoView();

            }
            catch (Exception ex)
            {

            }

            return View(parametrosCorreoView);
        }

        [HttpPost]
        public ActionResult Eliminar(int Parametro_Correo_ID)
        {
            ParametrosCorreoView parametrosCorreoView = new ParametrosCorreoView();
            try
            {
                string mensaje = ParametroCorreo_Repo.Eliminar(Parametro_Correo_ID);

                if(mensaje == "bien")
                {
                    return RedirectToAction("Listado", "ParametrosCorreo");
                }
                else
                {
                    ModelState.AddModelError("error", mensaje);
                    parametrosCorreoView = ParametroCorreo_Repo.Obtener_Un_Parametro(Parametro_Correo_ID);
                }

                


            }catch(Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
            }

            return View(parametrosCorreoView);

        }

        [HttpPost]
        public ActionResult Guardar(ParametrosCorreoView parametrosCorreoView)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    parametrosCorreoView.Usuario_Creo = Cls_Sesiones.EMAIL;
                    ParametroCorreo_Repo.Guardar(parametrosCorreoView);

                    return RedirectToAction("Listado", "ParametrosCorreo");

                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("error", ex.Message);

                }

            }


            return View("Registrar", parametrosCorreoView);
        }
    }
}