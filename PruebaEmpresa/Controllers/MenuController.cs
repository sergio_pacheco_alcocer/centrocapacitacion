﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
namespace PruebaEmpresa.Controllers
{
    public class MenuController : Controller
    {
        IMenuPrincipal_Repo MenuPrincipal_Repo;


        public MenuController(IMenuPrincipal_Repo menuPrincipal_Repo)
        {
            MenuPrincipal_Repo = menuPrincipal_Repo;
        }


        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Construir_Menu()
        {
            List<Menu_Principal_View> Lista_Menu = new List<Menu_Principal_View>();

            Lista_Menu = MenuPrincipal_Repo.Obtener_Menu_Principal();

            return PartialView(Lista_Menu);
        }
    }
}