﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;
using PruebaEmpresa.Servicios.Interfaces;
using PruebaEmpresa.Infraestructura.Permisos;

namespace PruebaEmpresa.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]

    public class CargosController : Controller
    {

        ICargos_Repo Cargos_Repo;
        ICombo_Repo Combo_Repo;

        public CargosController(ICargos_Repo cargos_Repo, ICombo_Repo combo_Repo)
        {
            Cargos_Repo = cargos_Repo;
            Combo_Repo = combo_Repo;
        }

        // GET: Cargos
        public ActionResult Listado()
        {
            List<CargosView> Listado_Cargos = Cargos_Repo.Obtener_Cargos();

            return View(Listado_Cargos);
        }

        public ActionResult Registrar(int? Cargo_ID)
        {
            CargosView cargosView = new CargosView();

            try
            {
                ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name");

                if (Cargo_ID.HasValue)
                    cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID.Value);
                else
                    cargosView = new CargosView();

            }catch(Exception ex)
            {

            }

            return View(cargosView);
        }


        [HttpPost]
        public ActionResult Guardar(CargosView cargosView)
        {
            ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name",cargosView.Estatus);

            if (ModelState.IsValid)
            {
                try
                {

                    cargosView.Usuario_Creo = Cls_Sesiones.EMAIL;
                    Cargos_Repo.Guardar(cargosView);

                    return RedirectToAction("Listado", "Cargos");

                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("error", ex.Message);
                }

            }

            return View("Registrar", cargosView);
        }

        public ActionResult Eliminar(int? Cargo_ID)
        {
            CargosView cargosView = new CargosView();

            try
            {
               

                if (Cargo_ID.HasValue)
                    cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID.Value);
                else
                    cargosView = new CargosView();

            }
            catch (Exception ex)
            {

            }

            return View(cargosView);

        }
        [HttpPost]
        public ActionResult Eliminar(int Cargo_ID)
        {
            CargosView cargosView = new CargosView();

            try
            {
                string mensaje = Cargos_Repo.Eliminar(Cargo_ID);

                if (mensaje == "bien")
                {
                    return RedirectToAction("Listado", "Cargos");
                }
                else
                {
                    ModelState.AddModelError("error", mensaje);
                    cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID);
                }

            }
            catch(Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
            }


            return View(cargosView);
        }
    }
}