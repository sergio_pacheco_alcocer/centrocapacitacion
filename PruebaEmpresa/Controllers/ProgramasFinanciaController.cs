﻿using PruebaEmpresa.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaEmpresa.Controllers
{
    public class ProgramasFinanciaController : Controller
    {
        // GET: ProgramasFinancia
        public ActionResult Programas_Financiamiento()
        {
            ViewData["actualizacion"] = Cls_Sesiones.FECHA_ACTUALIZACION;
            return View();
        }


        public ActionResult prestamos()
        {
            ViewData["actualizacion"] = Cls_Sesiones.FECHA_ACTUALIZACION;
            return View();
        }

        
        public ActionResult capital_inversion()
        {
            ViewData["actualizacion"] = Cls_Sesiones.FECHA_ACTUALIZACION;
            return View();
        }
        
    }
}