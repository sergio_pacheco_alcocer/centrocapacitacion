﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;
using PruebaEmpresa.Servicios.Interfaces;
using PruebaEmpresa.Infraestructura.Permisos;
using PruebaEmpresa.Infraestructura.Enums;
using System.Web.Helpers;
using System.Drawing;
using System.IO;

namespace PruebaEmpresa.Controllers
{

   
    public class MentoresController : Controller
    {
        IEspecialidad_Repo Especialidad_Repo;
        IUsuarios_Repo Usuarios_Repo;
        ICombo_Repo Combo_Repo;
        private IRoles_Repo Roles_Repo;

        public MentoresController(IEspecialidad_Repo especialidad_Repo, IUsuarios_Repo usuarios_Repo, ICombo_Repo combo_Repo, IRoles_Repo roles_Repo)
        {
            Especialidad_Repo = especialidad_Repo;
            Usuarios_Repo = usuarios_Repo;
            Combo_Repo = combo_Repo;
            Roles_Repo = roles_Repo;
        }

        [SessionExpireAttribute]
        [RoleAuthorize()]
        // GET: Mentores
        public ActionResult Listado(string sortOrder, string currentFilter, string searchString, int? currentEspecialidad ,int? Especialidad_ID, int? page)
        {

            ViewBag.Especialidad_ID = new SelectList(Especialidad_Repo.Obtener_Especialidades_Combo(), "id", "name", currentEspecialidad);

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NombreSortParam = String.IsNullOrEmpty(sortOrder) ? "nombre_desc" : "";
            ViewBag.ApellidoPaternoSortParam = sortOrder == "Paterno" ? "paterno_desc" : "Paterno";
            ViewBag.EspecialidadSortParam = sortOrder == "Especialidad" ? "especialidad_desc" : "Especialidad";
            ViewBag.EstatusSortParam = sortOrder == "Estatus" ? "estatus_desc" : "Estatus";



            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            if (Especialidad_ID.HasValue)
            {
                page = 1;
            }
            else
            {
                Especialidad_ID = currentEspecialidad;
            }


            ViewBag.CurrentEspecialidad = Especialidad_ID;
            ViewBag.CurrentFilter = searchString;

            var dbMentores = Usuarios_Repo.Cat_Usuarios.Where(x => x.Cat_Roles.Nombre == "Mentor");


            if (!String.IsNullOrEmpty(searchString)) {

                dbMentores = dbMentores.Where(s => s.Nombre.Contains(searchString) || s.Apellido_Paterno.Contains(searchString) || s.Apellido_Materno.Contains(searchString) || s.Correo_Electronico.Contains(searchString));
            }


            if (Especialidad_ID.HasValue)
            {
                dbMentores = dbMentores.Where(s => s.Especialidad_ID == Especialidad_ID.Value);
            }

            switch (sortOrder)
            {
                case "nombre_desc":
                    dbMentores = dbMentores.OrderByDescending(x => x.Nombre);
                    break;
                case "Paterno":
                    dbMentores = dbMentores.OrderBy(x => x.Apellido_Paterno);
                    break;
                case "paterno_desc":
                    dbMentores = dbMentores.OrderByDescending(x => x.Apellido_Paterno);
                    break;
                case "Especialidad":
                    dbMentores = dbMentores.OrderBy(x => x.Cat_Especialidades.Nombre_Especialidad);
                    break;
                case "especialidad_desc":
                    dbMentores = dbMentores.OrderByDescending(x => x.Cat_Especialidades.Nombre_Especialidad);
                    break;
                case "Estatus":
                    dbMentores = dbMentores.OrderBy(x => x.Estatus);
                    break;
                case "estatus_desc":
                    dbMentores = dbMentores.OrderByDescending(x => x.Estatus);
                    break;
                default:
                    dbMentores = dbMentores.OrderBy(x => x.Nombre);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var listaMentores = dbMentores.Select(x => new MentorView
            {
                Apellido_Materno = x.Apellido_Materno,
                Apellido_Paterno = x.Apellido_Paterno,
                Correo_Electronico = x.Correo_Electronico,
                Estatus = x.Estatus,
                Nombre = x.Nombre,
                Contraseña = x.Password,
                Confirmar_Contraseña = x.Password,
                Telefono = x.Telefono,
                Usuarios_ID = x.Usuarios_ID,
                Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                Nombre_Especialidad = x.Cat_Especialidades == null ? "" : x.Cat_Especialidades.Nombre_Especialidad,
                Especialidad_ID = x.Especialidad_ID,
                Es_Empresa = x.Es_Empresa

            });


            return View(listaMentores.ToPagedList(pageNumber,pageSize));
        }


        [SessionExpireAttribute]
        [RoleAuthorize(Roles_Enum.Mentor)]
        public ActionResult Registrar(int? Usuarios_ID)
        {

            MentorView mentorView = new MentorView();
            ViewBag.Mensaje = "";
            try
            {
                

                if (Usuarios_ID.HasValue)
                {
                    mentorView = Usuarios_Repo.Obtener_Un_Mentor(Usuarios_ID.Value);
                    ViewBag.Especialidad_ID = new SelectList(Especialidad_Repo.Obtener_Especialidades_Combo(), "id", "name", mentorView.Especialidad_ID);
                    ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name", mentorView.Estatus);
                }
                else {
                    ViewBag.Especialidad_ID = new SelectList(Especialidad_Repo.Obtener_Especialidades_Combo(), "id", "name");
                    ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name");

                    mentorView = new MentorView();
                }

            }
            catch (Exception ex) {

            }

            return View(mentorView);

        }

        [SessionExpireAttribute]
        [RoleAuthorize(Roles_Enum.Mentor)]
        public ActionResult Guardar(MentorView mentorView, HttpPostedFileBase image = null)
        {
            ViewBag.Especialidad_ID = new SelectList(Especialidad_Repo.Obtener_Especialidades_Combo(), "id", "name", mentorView.Especialidad_ID);
            ViewBag.Estatus = new SelectList(Combo_Repo.Obtener_Estatus(), "name", "name", mentorView.Estatus);
            ViewBag.Mensaje = "";


            if (ModelState.IsValid)
            {

                try
                {

                    RolesView rolesView = Roles_Repo.Obtener_Un_Rol("mentor");

                    if (rolesView == null)
                    {
                        ModelState.AddModelError("error", "Se requiere agregar un rol");
                        return View("Registrar", mentorView);
                    }

                    UsuariosView usuariosView = new UsuariosView();
                    usuariosView.Usuarios_ID = mentorView.Usuarios_ID;
                    usuariosView.Correo_Electronico = mentorView.Correo_Electronico;
                    usuariosView.Password = mentorView.Contraseña;
                    usuariosView.Telefono = mentorView.Telefono;
                    usuariosView.Nombre = mentorView.Nombre;
                    usuariosView.Apellido_Paterno = mentorView.Apellido_Paterno;
                    usuariosView.Apellido_Materno = mentorView.Apellido_Materno;
                    usuariosView.Telefono = mentorView.Telefono;
                    usuariosView.Usuario_Creo = Cls_Sesiones.EMAIL;
                    usuariosView.Estatus = mentorView.Estatus;
                    usuariosView.Especialidad_ID = mentorView.Especialidad_ID;
                    usuariosView.Rol_ID = rolesView.Rol_ID;
                    usuariosView.Es_Empresa = mentorView.Es_Empresa;
                    usuariosView.Tipo_Imagen = mentorView.Tipo_Imagen;
                    usuariosView.Nombre_Imagen = mentorView.Nombre_Imagen;

                    if(image != null)
                    {
                        if (!ValidateFile(image))
                        {
                            ModelState.AddModelError("error", "Farmato de la imagen es incorrecto");
                            return View("Registrar", mentorView);
                        }

                        mentorView.Tipo_Imagen = image.ContentType;
                        mentorView.Nombre_Imagen =  image.FileName;
                        SaveFileToDisk(image);
                        usuariosView.Nombre_Imagen = mentorView.Nombre_Imagen;
                        usuariosView.Tipo_Imagen = mentorView.Tipo_Imagen;
                        
                    }

                    if (Usuarios_Repo.Esta_Repetido_Usuario(mentorView.Correo_Electronico, mentorView.Usuarios_ID) == "SI")
                    {

                        ModelState.AddModelError("error", "El correo escrito ya esta registrado");
                        return View("Registrar", mentorView);
                    }

                    Usuarios_Repo.Registrar_Usuario(usuariosView);


                    if (Cls_Sesiones.NOMBRE_ROL.ToLower() != "mentor")
                    {
                        return RedirectToAction("Listado", "Mentores");
                    }
                    else {
                        ViewBag.Mensaje = "Proceso Correcto";
                    }

                   

                }
                catch (Exception ex) {
                    ModelState.AddModelError("error", ex.Message);
                }

               

            }


            return View("Registrar", mentorView);
        }

        [SessionExpireAttribute]
        [RoleAuthorize()]
        public ActionResult Eliminar(int? Usuarios_ID)
        {
            MentorView mentorView = new MentorView();

            try
            {
                if (Usuarios_ID.HasValue)
                    mentorView = Usuarios_Repo.Obtener_Un_Mentor(Usuarios_ID.Value);
                else
                    mentorView = new MentorView();

            }
            catch (Exception ex) {

            }

            return View(mentorView);

        }

        [SessionExpireAttribute]
        [RoleAuthorize()]
        [HttpPost]
        public ActionResult Eliminar(int Usuarios_ID)
        {
            MentorView mentorView = new MentorView();

            try
            {
                string mensaje = Usuarios_Repo.Eliminar_Usuario(Usuarios_ID);

                if (mensaje == "bien")
                {
                    return RedirectToAction("Listado", "Mentores");
                }
                else {
                    ModelState.AddModelError("error", mensaje);
                    mentorView = Usuarios_Repo.Obtener_Un_Mentor(Usuarios_ID);
                }
            }
            catch (Exception ex) {
                ModelState.AddModelError("error", ex.Message);
            }

            return View(mentorView);
        }

        public FileContentResult GetImage(int Usuarios_ID)
        {
           //MentorView  mentorView = Usuarios_Repo.Obtener_Un_Mentor(Usuarios_ID);
           // try
           // {

           //     if(mentorView != null) {

           //         Image image = Image.FromFile(Server.MapPath("~/Imagenes") + "/" + mentorView.Nombre_Imagen);
           //         return File (Convertir_Imagen_Bytes(image), mentorView.Tipo_Imagen);
           //     }
                
           // }catch(Exception ex)
           // {

           // }

            return null;
        }

        public static byte[] Convertir_Imagen_Bytes(Image img)
        {
            string sTemp = Path.GetTempFileName();
            FileStream fs = new FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            img.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
            fs.Position = 0;

            int imgLength = Convert.ToInt32(fs.Length);
            byte[] bytes = new byte[imgLength];
            fs.Read(bytes, 0, imgLength);
            fs.Close();
            return bytes;
        }

        private bool ValidateFile(HttpPostedFileBase file)
        {
            string fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
            string[] allowedFileTypes = { ".gif", ".png", ".jpeg", ".jpg" };
            if ((file.ContentLength > 0 && file.ContentLength < 2097152) &&
            allowedFileTypes.Contains(fileExtension))
            {
                return true;
            }
            return false;
        }


        private void SaveFileToDisk(HttpPostedFileBase file)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > 500)
            {
                img.Resize(500, 500,false,true);
            }
            img.Save(Server.MapPath("~/Imagenes") + "/" + file.FileName);
          
        }
    }
}