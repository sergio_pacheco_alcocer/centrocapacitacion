﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Controllers
{
    public class OpcionesController : Controller
    {
        IMenuPrincipal_Repo MenuPrincipal_Repo;
        public OpcionesController(IMenuPrincipal_Repo menuPrincipal_Repo)
        {
            MenuPrincipal_Repo = menuPrincipal_Repo;
        }

        // GET: Opciones
        public ActionResult Index(int? Menu_Principal_ID)
        {
            Menu_Principal_View menu_Principal_View = MenuPrincipal_Repo.ObtenerFaseUno(Menu_Principal_ID.Value);

            return View(menu_Principal_View);
        }
    }
}