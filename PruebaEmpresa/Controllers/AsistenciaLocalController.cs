﻿using Newtonsoft.Json;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Servicios;
using PruebaEmpresa.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace PruebaEmpresa.Controllers
{
    public class AsistenciaLocalController : Controller
    {
        IUsuarios_Repo Usuarios_Repo;


        public AsistenciaLocalController(IUsuarios_Repo usuarios_Repo)
        {
            Usuarios_Repo = usuarios_Repo;
        }

        // GET: AsistenciaLocal
        public ActionResult Asistencia_Local(int? page)
        {
            int pageSize = 10;
            int pageNumber = 0;

            IQueryable<MentorView> Lista_Mentores;
            try
            {
                pageNumber = (page ?? 1);
                Lista_Mentores = Usuarios_Repo.Obtener_Mentores();
                

            }
            catch (Exception ex) {
                Lista_Mentores = Enumerable.Empty<MentorView>().AsQueryable();
                pageNumber = 1;
                pageSize = 1;
            }

            return View(Lista_Mentores.ToPagedList(pageNumber,pageSize));
        }

        public ActionResult Exportacion_Ayuda_Negocio()
        {
            return View();
        }

        public ActionResult Recursos()
        {
            return View();
        }

        public ActionResult Acceso_Capital()
        {
            return View();
        }

        [SessionExpireVisitante]
        public ActionResult Encontrar_Ayuda_Cercana()
        {
            return View();
        }






        #region(acciones Encontrar_Ayuda_Cercana)

        public ActionResult Consultar_Especialidades()
        {

            List<Cat_Especialidades> lista = new List<Cat_Especialidades>();
            string json_resultado = "";

            try
            {

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();

                    const string sqlQuery = "SELECT * FROM Cat_Especialidades";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            Cat_Especialidades element = new Cat_Especialidades
                            {
                                Especialidad_ID = Convert.ToInt32(dataReader["Especialidad_ID"]),
                                Nombre_Especialidad = Convert.ToString(dataReader["Nombre_Especialidad"]),
                                Estatus = Convert.ToString(dataReader["Estatus"]),
                                Usuario_Creo = Convert.ToString(dataReader["Usuario_Creo"]),
                                Fecha_Creo = Convert.ToDateTime(dataReader["Fecha_Creo"]),
                                Usuario_Modifico = dataReader["Usuario_Modifico"] == DBNull.Value? "" : Convert.ToString(dataReader["Usuario_Modifico"]),
                                Fecha_Modifico = dataReader["Fecha_Modifico"] == DBNull.Value? new DateTime() : Convert.ToDateTime(dataReader["Fecha_Modifico"])
                            };

                            lista.Add(element);
                        }
                    }


                    json_resultado = JsonConvert.SerializeObject(lista);
                }
            }
            catch (Exception ex)
            {
                json_resultado = "Ha ocurrido un error: " + ex.Message;
            }

            return Content(json_resultado);

        }


        public ActionResult Consultar_Mentores()
        {
            List<Cls_Cat_Usuarios> lista = new List<Cls_Cat_Usuarios>();
            string json_resultado = "";

            try
            {

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();

                    const string sqlQuery = "SELECT Cat_Usuarios.*,Cat_Especialidades.Nombre_Especialidad FROM Cat_Usuarios INNER JOIN Cat_Especialidades ON Cat_Usuarios.Especialidad_ID = Cat_Especialidades.Especialidad_ID WHERE Cat_Usuarios.Especialidad_ID IS NOT NULL ";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            Cls_Cat_Usuarios element = new Cls_Cat_Usuarios
                            {
                                Nombre_Especialidad = Convert.ToString(dataReader["Nombre_Especialidad"]),

                                Usuarios_ID = Convert.ToInt32(dataReader["Usuarios_ID"]),
                                Rol_ID = Convert.ToInt32(dataReader["Rol_ID"]),
                                Nombre = Convert.ToString(dataReader["Nombre"]),
                                Apellido_Paterno = Convert.ToString(dataReader["Apellido_Paterno"]),
                                Apellido_Materno = Convert.ToString(dataReader["Apellido_Materno"]),
                                Correo_Electronico = Convert.ToString(dataReader["Correo_Electronico"]),
                                Telefono = Convert.ToString(dataReader["Telefono"]),
                                Password = Convert.ToString(dataReader["Password"]),
                                CustomerID = dataReader["CustomerID"] == DBNull.Value ? "" : Convert.ToString(dataReader["CustomerID"]),
                                SubscriptionID = dataReader["SubscriptionID"] == DBNull.Value ? "" : Convert.ToString(dataReader["SubscriptionID"]),
                                Especialidad_ID = Convert.ToInt32(dataReader["Especialidad_ID"]),
                                Estatus = Convert.ToString(dataReader["Estatus"]),
                                Usuario_Creo = Convert.ToString(dataReader["Usuario_Creo"]),
                                Fecha_Creo = Convert.ToDateTime(dataReader["Fecha_Creo"]),
                                Usuario_Modifico = dataReader["Usuario_Modifico"] == DBNull.Value ? "" : Convert.ToString(dataReader["Usuario_Modifico"]),
                                Fecha_Modifico = dataReader["Fecha_Modifico"] == DBNull.Value ? new DateTime() : Convert.ToDateTime(dataReader["Fecha_Modifico"]),
                                Nombre_Imagen = dataReader["Nombre_Imagen"] == null ? "": Convert.ToString(dataReader["Nombre_Imagen"])
                            };

                            lista.Add(element);
                        }
                    }


                    json_resultado = JsonConvert.SerializeObject(lista);
                }
            }
            catch (Exception ex)
            {
                json_resultado = "Ha ocurrido un error: " + ex.Message;
            }

            return Content(json_resultado);
        }



        public ActionResult Registrar_Pregunta(string jsonObject)
        {
            Ope_Preguntas obj_filtro = new Ope_Preguntas();
            List<Ope_Preguntas> lista_resultado = new List<Ope_Preguntas>();
            string json_resultado = "";

            try
            {
                obj_filtro = JsonConvert.DeserializeObject<Ope_Preguntas>(jsonObject);                

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();
                    const string sqlQuery = "INSERT INTO Ope_Preguntas (Usuario_ID,Usuario_Mentor_ID,Descripcion_Pregunta,Visto,Usuario_Creo,Fecha_Creo,Usuario_Modifico,Fecha_Modifico) VALUES (@usuario_id,@usuario_mentor_id,@descripcion_pregunta,0,@usuario_creo,GETDATE(),NULL,NULL)";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {                        
                        cmd.Parameters.AddWithValue("@usuario_id", Cls_Sesiones.USUARIO_VISITANTE_ID);
                        cmd.Parameters.AddWithValue("@usuario_mentor_id", obj_filtro.Usuario_Mentor_ID);
                        cmd.Parameters.AddWithValue("@descripcion_pregunta", obj_filtro.Descripcion_Pregunta);                        
                        cmd.Parameters.AddWithValue("@usuario_creo", Cls_Sesiones.USUARIO_VISITANTE_NOMBRE);
                        cmd.ExecuteNonQuery();
                    }



                    string Nombre_Mentor = "";
                    string Correo_Mentor = "";

                    //obtener datos del mentor
                    const string query_mentor = "SELECT * FROM Cat_Usuarios WHERE Usuarios_ID = @usuario_id";
                    using(SqlCommand cmd = new SqlCommand(query_mentor, cnx))
                    {
                        cmd.Parameters.AddWithValue("@usuario_id", obj_filtro.Usuario_Mentor_ID);
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        if (dataReader.Read())
                        {
                            Nombre_Mentor = Convert.ToString(dataReader["Nombre"]) + " " + Convert.ToString(dataReader["Apellido_Paterno"]) + " " + Convert.ToString(dataReader["Apellido_Materno"]);
                            Correo_Mentor = Convert.ToString(dataReader["Correo_Electronico"]);
                        }
                    }




                    //ENIVAR CORREO AL MENTOR
                    const string sqlQuery_m = "select * from Cat_Parametro_Correo";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery_m, cnx))
                    {
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        if(dataReader.Read())
                        {

                            string Email = Convert.ToString(dataReader["Email"]);
                            string Password = Convert.ToString(dataReader["Password"]);



                            string mensaje = "Estimad@ "+ Nombre_Mentor + ", Tienes una nueva pregnta de la \"Plataforma de Capacitación Empresarial\"<br><br>" +
                                "El usuario " + Cls_Sesiones.USUARIO_VISITANTE_NOMBRE + " ha hecho la siguiente pregunta: <br><br>"+
                                "<p style='font-weight:bold; font-size:20px;'>" + obj_filtro.Descripcion_Pregunta + "</p><br><br><br>"+
                                "Favor de Acceder a la plataforma para contestar esta pregunta en cuanto le sea posible. <br>Gracias por su participación";
                            

                            Enviar_Correo_Servicio Mail = new Enviar_Correo_Servicio(Email,Correo_Mentor,mensaje,"Nueva pregunta \"Plataforma de Guía Emprende\"",Email,Password);


                            if (Mail.enviaMail())                            
                                json_resultado = "completado";                            
                            else                            
                                json_resultado = "No se ha enviado el correo: " + Mail.error;                            

                        }
                    }
                }
                                
            }
            catch (Exception ex)
            {
                json_resultado = "Ha ocurrido un error: " + ex.Message;
            }


            return Content(json_resultado);
        }

        #endregion
    }
}