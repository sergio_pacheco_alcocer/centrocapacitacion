﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Interfaces;
using System.Web.Security;
using PruebaEmpresa.Util;

namespace PruebaEmpresa.Controllers
{
    public class LoginController : Controller
    {

        private IUsuarios_Repo Usuarios_Repo;
        private IRoles_Repo Roles_Repo;


        public LoginController(IUsuarios_Repo usuarios_Repo, IRoles_Repo roles_Repo)
        {
            this.Usuarios_Repo = usuarios_Repo;
            this.Roles_Repo = roles_Repo;
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }


        public ViewResult Verificar()
        {
            LoginView loginView = new LoginView();

            return View(loginView);
        }


        [HttpPost]
        public ActionResult Verificar_Usuario(LoginView loginView)
        {
            loginView.Mensajes = "";

            if (ModelState.IsValid)
            {
                UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(loginView.Inicio_Sesion.Correo_Electronico, loginView.Inicio_Sesion.Contraseña);

                if(usuariosView == null)
                {
                    loginView.Mensajes = "Usuario o contraseña incorrecto";
                    return View("Verificar", loginView);
                }

                if (usuariosView.Nombre_Rol.Trim().Length == 0) {
                    loginView.Mensajes = "El usuario no tiene un rol asignado";
                    return View("Verificar", loginView);
                }


                Cls_Sesiones.USUARIO_ID = usuariosView.Usuarios_ID;
                Cls_Sesiones.EMAIL = usuariosView.Correo_Electronico;
                Cls_Sesiones.NOMBRE_ROL = usuariosView.Nombre_Rol;

                FormsAuthentication.SetAuthCookie(usuariosView.Correo_Electronico, false);
               return  RedirectToAction("Index", "Inicio");

            }


            return View("Verificar", loginView);
        }

        [HttpPost]
        public ActionResult Registrar_Usuario(LoginView loginView)
        {
            loginView.Mensajes = "";


            if (ModelState.IsValid)
            {
                try
                {

                    RolesView rolesView = Roles_Repo.Obtener_Un_Rol("usuario");


                    if (rolesView == null)
                    {
                        loginView.Mensajes = "Se requiere agregar un rol";
                        return View("Verificar", loginView);
                    }

                    UsuariosView usuariosView = new UsuariosView();

                    usuariosView.Correo_Electronico = loginView.Registro_Usuario.Correo_Electronico;
                    usuariosView.Password = loginView.Registro_Usuario.Contraseña;
                    usuariosView.Telefono = loginView.Registro_Usuario.Telefono;
                    usuariosView.Nombre = loginView.Registro_Usuario.Nombre;
                    usuariosView.Apellido_Paterno = loginView.Registro_Usuario.Apellido_Paterno;
                    usuariosView.Apellido_Materno = loginView.Registro_Usuario.Apellido_Materno;
                    usuariosView.Telefono = loginView.Registro_Usuario.Telefono;
                    usuariosView.Usuario_Creo = "usuario";
                    usuariosView.Estatus = "ACTIVO";
                    usuariosView.Rol_ID = rolesView.Rol_ID;


                    if(Usuarios_Repo.Esta_Repetido_Usuario(usuariosView.Correo_Electronico,0) == "SI")
                    {
                        loginView.Mensajes = "El correo escrito ya esta registrado";
                        return View("Verificar", loginView);
                    }

                    Usuarios_Repo.Registrar_Usuario(usuariosView);
                    loginView.Mensajes = "Usuario Registrado";
                    ModelState.Clear();
                    loginView.Registro_Usuario.Correo_Electronico = string.Empty;
                    loginView.Registro_Usuario.Nombre = string.Empty;
                    loginView.Registro_Usuario.Telefono = string.Empty;
                    loginView.Registro_Usuario.Apellido_Paterno = string.Empty;
                    loginView.Registro_Usuario.Apellido_Materno = string.Empty;
                    
                }
                catch (Exception ex) {

                }

            }

            return View("Verificar", loginView);
        }

        public ActionResult Salir()
        {

            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Verificar", "Login");
        }
    }
}