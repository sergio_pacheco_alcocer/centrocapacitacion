﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using Stripe;
using Stripe.Checkout;
using PruebaEmpresa.Util;
using PruebaEmpresa.Contexto;
using System.Data.Entity;

namespace PruebaEmpresa.Controllers
{
    [SessionExpireAttribute]
    [Authorize]
    public class PagoController : Controller
    {
        ICargos_Repo Cargos_Repo;
        IUsuarios_Repo Usuarios_Repo;
        IRegistroPagos_Repo RegistroPagos_Repo;
        IProveedores_Repo Proveedores_Repo;
        ICancelarSuscripcion_Repo CancelarSuscripcion_Repo;

        public PagoController(ICargos_Repo cargos_Repo, IUsuarios_Repo usuarios_Repo, IRegistroPagos_Repo registroPagos_Repo,
            IProveedores_Repo proveedores_Repo, ICancelarSuscripcion_Repo cancelarSuscripcion_Repo)
        {
            Cargos_Repo = cargos_Repo;
            Usuarios_Repo = usuarios_Repo;
            RegistroPagos_Repo = registroPagos_Repo;
            Proveedores_Repo = proveedores_Repo;
            CancelarSuscripcion_Repo = cancelarSuscripcion_Repo;
        }



        public ActionResult Eliminar_Suscripcion(int? Cargo_ID)
        {
            int Usuario_ID = Cls_Sesiones.USUARIO_ID;
            CargosView cargosView = new CargosView();

            try
            {
                if (Cargo_ID.HasValue)
                {
                    cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID.Value);
                }
                

            }
            catch (Exception ex) {

            }

            return View(cargosView);
        }


        [HttpPost]
        public ActionResult Eliminar_Suscripcion(int Cargo_ID)
        {
            CargosView cargosView = new CargosView();
            bool blnPagoExito = false; 

            try
            {
                UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(Cls_Sesiones.USUARIO_ID);
                cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID);

                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["stripeSecretkey"];
                var service = new SubscriptionService();
                service.Cancel(usuariosView.SubscriptionID);

                using (EFDbContext context = new EFDbContext())
                {
                    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                    {

                        try
                        {

                            CancelarSuscripcionView cancelarSuscripcion = new CancelarSuscripcionView();
                            cancelarSuscripcion.Cargo_ID = cargosView.Cargo_ID;
                            cancelarSuscripcion.Identificador = usuariosView.SubscriptionID;
                            cancelarSuscripcion.Usuarios_ID = usuariosView.Usuarios_ID;

                            CancelarSuscripcion_Repo.Registrar_Cancelacion(context, cancelarSuscripcion);
                            Usuarios_Repo.Eliminar_SucriptionID(context, usuariosView.Usuarios_ID);

                            transaction.Commit();
                            blnPagoExito = true;
                        }
                        catch (Exception ex)
                        {
                         
                            transaction.Rollback();
                            ModelState.AddModelError("error", ex.Message);
                        }

                    }
                }



            }
            catch (Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
            }

            if (blnPagoExito) {
                return RedirectToAction("Listado_Cargos");
            }


            return View(cargosView);
        }


        public ActionResult Listado_Cargos()
        {
            CargosSeleccionView cargosSeleccionView = new CargosSeleccionView();

            try {

                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["stripeSecretkey"];

                UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(Cls_Sesiones.USUARIO_ID);

                if (usuariosView.SubscriptionID.Trim().Length > 0)
                {
                    var service = new SubscriptionService();
                    Subscription subscription = service.Get(usuariosView.SubscriptionID);

                    if(subscription.Status == "active")
                    {
                        cargosSeleccionView.Tiene_Pago = true;

                        RegistroPagosView registroPagosView = RegistroPagos_Repo.Obtener_Registro_Pago_Actual(Cls_Sesiones.USUARIO_ID);
                         if(registroPagosView != null)
                         {
                              
                               cargosSeleccionView.Cargo_ID = registroPagosView.Cargo_ID;
                          }


                    }
                }

                List<CargosView> Lista_Cargos_View = Cargos_Repo.Obtener_Cargos_Activos();
                cargosSeleccionView.Lista_Cargo = Lista_Cargos_View;

                if(Proveedores_Repo.Obtener_Proveedores(Cls_Sesiones.USUARIO_ID).Count() > 0)
                {
                    cargosSeleccionView.Tiene_Proveedor = true;
                }


            }
            catch(Exception ex)
            {

            }
           

            return View(cargosSeleccionView);
        }


        public ActionResult Hacer_Pago(int? Cargo_ID)
        {
            int Usuario_ID = Cls_Sesiones.USUARIO_ID;
            CargosView cargosView = new CargosView();
            Cls_Cargo_Pago cls_Cargo_Pago = new Cls_Cargo_Pago();
            CheckOutView checkOutView = new CheckOutView();
            try
            {
                ViewBag.StripePublishKey = ConfigurationManager.AppSettings["stripePublishableKey"];

                UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(Usuario_ID);

                if(usuariosView != null)
                {
                    cls_Cargo_Pago.Email = usuariosView.Correo_Electronico;
                    cls_Cargo_Pago.Phone = usuariosView.Telefono;
                }

                if (Cargo_ID.HasValue)
                {
                    cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID.Value);
                    cls_Cargo_Pago.Cargo_ID = Cargo_ID.Value;
                }
                else
                {
                    cls_Cargo_Pago.Cargo_ID = 0;
                }

                checkOutView.cargosView = cargosView;
                checkOutView.cls_Cargo_Pago = cls_Cargo_Pago;


            }catch(Exception ex)
            {

            }

            return View(checkOutView);
        }


        public ActionResult Success( int Cargo_ID)
        {
            CargosView cargosView;

            cargosView = Cargos_Repo.Obtener_Un_Cargo(Cargo_ID);

            return View(cargosView);
        }

        public ActionResult Exitoso()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(CheckOutView checkOutView)
        {
            CargosView cargosView = new CargosView();
            checkOutView.Mensaje = "";
            UsuariosView usuariosView;
            bool blnPagoExito = false;
            string customerID = "";
            try
            {
                ViewBag.StripePublishKey = ConfigurationManager.AppSettings["stripePublishableKey"];
                cargosView = Cargos_Repo.Obtener_Un_Cargo(checkOutView.cls_Cargo_Pago.Cargo_ID);

                if (ModelState.IsValid)
                {
                    
                    StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["stripeSecretkey"];

                    // verificamos si el correo de usuario esta agregado como cliente en la plataforma

                    usuariosView = Usuarios_Repo.Obtener_Usuario(Cls_Sesiones.USUARIO_ID);

                    if(usuariosView.CustomerID.Trim().Length == 0)
                    {

                        // creamos un cliente
                        var customerOptions = new CustomerCreateOptions
                        {
                            Description = checkOutView.cls_Cargo_Pago.CardName,
                            Source = checkOutView.cls_Cargo_Pago.stripeToken,
                            Email = checkOutView.cls_Cargo_Pago.Email,
                            Metadata = new Dictionary<string, string>()
                        {
                            {"Teléfono", checkOutView.cls_Cargo_Pago.Phone }
                        }
                        };

                        // se hace un registro del cliente en la plataforma de stripe
                        var customerService = new CustomerService();
                        Customer customer = customerService.Create(customerOptions);

                        // ese id se le agregamos a la basde de datos
                        customerID = customer.Id;
                        Usuarios_Repo.Agregar_Customer_ID(Cls_Sesiones.USUARIO_ID, customerID);
                    }
                    else
                    {
                        customerID = usuariosView.CustomerID;
                    }


                    // haremos la suscprición 


                    var optionsSubcription = new SubscriptionCreateOptions
                    {
                        Customer = customerID,
                        Items = new List<SubscriptionItemOptions>
                          {
                            new SubscriptionItemOptions
                            {
                              Price = cargosView.ProductoFinancieroID,
                            },
                          },
                    };


                    var service = new SubscriptionService();
                    Subscription subscription =  service.Create(optionsSubcription);


                    // creamos su primer cargo
                    //var options = new ChargeCreateOptions
                    //{
                    //    Amount = Convert.ToInt64(cargosView.Precio) * 100,
                    //    Currency = "MXN",
                    //    Description = cargosView.Nombre_Cargo,
                    //    // Source= cls_Cargo_Pago.StripeToken
                    //    Customer = customerID
                    //};

                    //var service = new ChargeService();
                    //Charge charge = service.Create(options);

                    if (subscription.Id.Trim().Length > 0)
                    {
                        // se creo exitosamente el pago
                        blnPagoExito = true;
                        Usuarios_Repo.Agregar_SubscriptionID(Cls_Sesiones.USUARIO_ID, subscription.Id);

                        //DateTime datFechaInicio = DateTime.Now;
                        //DateTime datFechaFin = DateTime.Now.AddDays(cargosView.Dias);

                        //RegistroPagosView registroPagosView = new RegistroPagosView();
                        //registroPagosView.Cargo_ID = cargosView.Cargo_ID;
                        //registroPagosView.Identificador = subscription.Id;
                        //registroPagosView.Usuarios_ID = Cls_Sesiones.USUARIO_ID;
                        //registroPagosView.Usuario_Creo = Cls_Sesiones.EMAIL;
                        //registroPagosView.Fecha_Inicio_Vigencia = datFechaInicio;
                        //registroPagosView.Fecha_Fin_Vigencia = datFechaFin;


                        //using (EFDbContext context = new EFDbContext())
                        //{
                        //    using (DbContextTransaction transaction = context.Database.BeginTransaction())
                        //    {

                        //        try
                        //        {
                        //            Usuarios_Repo.Agergar_SubscriptionID(context, Cls_Sesiones.USUARIO_ID, subscription.Id);
                        //            RegistroPagos_Repo.Guardar(context, registroPagosView);
                        //            Proveedores_Repo.Actualizar_Vigencia_Proveedor(context, Cls_Sesiones.USUARIO_ID, datFechaInicio, datFechaFin);

                        //            transaction.Commit();
                        //            blnPagoExito = true;
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            checkOutView.Mensaje = ex.Message;
                        //            transaction.Rollback();
                        //        }

                        //    }
                        //}



                    }

                }// fin de la validacion

                

            }
            catch (Exception ex) {
                checkOutView.Mensaje = ex.Message;
            }

            checkOutView.cargosView = cargosView;

            if (blnPagoExito)
            {
                return RedirectToAction("Success", new { Cargo_ID = cargosView.Cargo_ID });
            }


            return View("Hacer_Pago", checkOutView);
        }

        [HttpPost]
        public ActionResult Create(Cls_Cargo_Pago cls_Cargo_Pago)
        {
            StripeConfiguration.ApiKey = "sk_test_7YX79Xi10JwMNaAaVDLIund8";



            var customerOptions = new CustomerCreateOptions
            {
                Description = cls_Cargo_Pago.CardName,
                Source = cls_Cargo_Pago.stripeToken,
                Email = cls_Cargo_Pago.Email,
                Metadata = new Dictionary<string, string>()
                {
                    {"Teléfono", cls_Cargo_Pago.Phone }
                }
            };

            var customerService = new CustomerService();
            Customer customer = customerService.Create(customerOptions);

            var options = new ChargeCreateOptions
            {
                Amount = 2000,
                Currency = "usd",
                Description = "Charge for fwew",
               // Source= cls_Cargo_Pago.StripeToken
               Customer = customer.Id
            };

            var service = new ChargeService();
            Charge charge = service.Create(options);

            return View();

        }


        // GET: Pago
        public ActionResult Iniciar()
        {

            StripeConfiguration.ApiKey = "sk_test_7YX79Xi10JwMNaAaVDLIund8";

            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string>
                {
                    "card",
                },
                LineItems = new List<SessionLineItemOptions>
                {
                    new SessionLineItemOptions
                    {
                        Name = "T-Shirt",
                        Description = "Comfortable cotton t-shirt",
                        Amount = 500,
                        Currency = "usd",
                        Quantity = 1
                    },
                },
                SuccessUrl = "http://localhost:11434/Pago/Iniciar",
                CancelUrl = "http://localhost:11434/Pago/Iniciar",

                PaymentIntentData = new SessionPaymentIntentDataOptions
                {
                    Metadata = new Dictionary<string, string>
                    {

                         { "orderd_id", "ddd"}

                    }

                }
            };

            var service = new SessionService();
            Session session = service.Create(options);

            return View(session);
        }


        public ActionResult Iniciar_Suscripcion()
        {
            ViewBag.StripePublishKey = ConfigurationManager.AppSettings["stripePublishableKey"];

            return View();
        }


        public  JsonResult Crear_Session(CreateCheckoutSessionRequest req)
        {
            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["stripeSecretkey"];

            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string>
                {
                    "card",
                },
                Mode = "subscription",
                LineItems = new List<SessionLineItemOptions>
                {
                    new SessionLineItemOptions
                    {
                        Price = req.PriceId,
                        Quantity = 1
                    },
                },
                SuccessUrl = "http://localhost:11434/Pago/Exitoso",
                CancelUrl = "http://localhost:11434/Pago/Iniciar_Suscripcion",

            };


            try
            {
                var service = new SessionService();
                Session session = service.Create(options);

                return Json(new
                {
                    sessionId = session.Id
                });


            }
            catch(StripeException e)
            {
                Console.WriteLine(e.StripeError.Message);

                return new BadRequest(e.StripeError.Message);


            }

        }


        [HttpPost]
        public ActionResult CreateCheckoutSession()
        {

            StripeConfiguration.ApiKey = "sk_test_7YX79Xi10JwMNaAaVDLIund8";

            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string>
                {
                    "card",
                },
                LineItems = new List<SessionLineItemOptions>
                {
                    new SessionLineItemOptions
                    {
                        Name = "T-Shirt",
                        Description = "Comfortable cotton t-shirt",
                        Amount = 500,
                        Currency = "usd",
                        Quantity = 1
                    },
                },
                SuccessUrl = "",
                CancelUrl = "",

                PaymentIntentData = new SessionPaymentIntentDataOptions
                {
                    Metadata = new Dictionary<string, string>
                    {

                         { "orderd_id", "ddd"}

                    }
                   
                }
            };

            var service = new SessionService();
            Session session = service.Create(options);

            return View(session);

        }
    }
}