﻿using PruebaEmpresa.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaEmpresa.Controllers
{
    public class CapacitacionController : Controller
    {
        // GET: Capacitacion
        public ActionResult Centro_Capacitacion()
        {
            ViewData["actualizacion"] = Cls_Sesiones.FECHA_ACTUALIZACION;
            return View();
        }
    }
}