﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Servicios.Interfaces;

namespace PruebaEmpresa.Controllers
{
    public class RecuperarController : Controller
    {

        ICorreo_Repo Correo_Repo;
        IUsuarios_Repo Usuarios_Repo;
  

        public RecuperarController(ICorreo_Repo correo_Repo, IUsuarios_Repo usuarios_Repo)
        {
            Correo_Repo = correo_Repo;
            Usuarios_Repo = usuarios_Repo;
        }

        // GET: RecuperarCorreo
        public ActionResult Index()
        {


            return View();
        }

        public ActionResult Password(string returnUrl)
        {
            RecuperarPassword recuperarPassword = new RecuperarPassword();
            recuperarPassword.returnUrl = returnUrl;

            return View(recuperarPassword);
        }


        [HttpPost]
        public ActionResult Password(RecuperarPassword recuperarPassword)
        {
            string mensaje;
            recuperarPassword.Mensajes = "";

            if (ModelState.IsValid)
            {

                try
                {
                    UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(recuperarPassword.Corre_Electronico);

                    if(usuariosView == null)
                    {
                        ModelState.AddModelError("no_encontrado", "No se encontro información con el correo escrito");
                        return View(recuperarPassword);
                    }

                    mensaje = Correo_Repo.Enviar_Correo(usuariosView);

                    if (mensaje == "bien")
                    {
                        recuperarPassword.Mensajes = "Se ha enviado tu contraseña al correo escrito";
                    }
                    else {
                        ModelState.AddModelError("no_encontrado", mensaje);
                    }

                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("no_encontrado", ex.Message);
                }
            }

            return View(recuperarPassword);
        }
    }
}