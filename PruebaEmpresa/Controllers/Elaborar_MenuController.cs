﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;
using PruebaEmpresa.Servicios.Interfaces;

namespace PruebaEmpresa.Controllers
{
    [SessionExpireAttribute]
    public class Elaborar_MenuController : Controller
    {

        IUsuarios_Repo Usuarios_Repo;
        IPreguntas_Repo Preguntas_Repo;
        public Elaborar_MenuController(IUsuarios_Repo usuarios_Repo, IPreguntas_Repo preguntas_Repo) {
            Usuarios_Repo = usuarios_Repo;
            Preguntas_Repo = preguntas_Repo;
        }

        public PartialViewResult Construir_Menu()
        {
            List<Cls_Modelo_Menu> objNavegacionPapa = new List<Cls_Modelo_Menu>();
            List<Cls_Modelo_Menu> objNavegacionSimple = new List<Cls_Modelo_Menu>();
            MenuView menuView = new MenuView();

            int usuarioID = Cls_Sesiones.USUARIO_ID;


            UsuariosView usuariosView = Usuarios_Repo.Obtener_Usuario(usuarioID);

            if (usuariosView.Nombre_Rol.Trim().ToLower().Equals("administrador"))
            {
                //objNavegacionPapa.Add(Ayudante_Menu.Crear_Menu_Proveedor());
                objNavegacionPapa.Add(Ayudante_Menu.Crear_Menu_Catalogos());
                objNavegacionPapa.Add(Ayudante_Menu.Crear_Menu_Mentor());
            }


            if (usuariosView.Nombre_Rol.Trim().ToLower().Equals("usuario"))
            {
                objNavegacionPapa.Add(Ayudante_Menu.Crear_Menu_Proveedor());
            }

            if (usuariosView.Nombre_Rol.Trim().ToLower().Equals("mentor")) {
                objNavegacionPapa.Add(Ayudante_Menu.Crear_Menu_Mentor());

                int preguntarSinLeer = Preguntas_Repo.Preguntas_Sin_Leer(Cls_Sesiones.USUARIO_ID);

                objNavegacionSimple.Add(Ayudante_Menu.CrearMenuPreguntasSinLeer(preguntarSinLeer));

            }

            menuView.objNavegacionCombo = objNavegacionPapa;
            menuView.objNavegacionSimple = objNavegacionSimple;

            return PartialView(menuView);
        }

    }
}