﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Infraestructura.Permisos;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Controllers
{
    [SessionExpireAttribute]
    [RoleAuthorize()]
    public class GiroProveedorController : Controller
    {
        IGiroProveedor_Repo GiroProveedor_Repo;


        public GiroProveedorController(IGiroProveedor_Repo giroProveedor_Repo)
        {
            GiroProveedor_Repo = giroProveedor_Repo;
        }

        // GET: GiroProveedor
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Catalogo_Giro_Proveedores()
        {
            return View();
        }

        public string Obtener_Giros_Proveedores(int offset, int limit, string search = null)
        {
            Cls_Paginado<Giro_ProveedorView> Paginado;
            string str_datos = "{}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try {
                Paginado = GiroProveedor_Repo.Obtener_Giro_Proveedores(offset, limit, search);
                str_datos = serializer.Serialize(Paginado);
            }
            catch(Exception ex)
            {
            }

            return str_datos;
        }

        [HttpPost]
        public string Guardar(Giro_ProveedorView giro_ProveedorView)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {

                if(GiroProveedor_Repo.Esta_Repetido_Giro(giro_ProveedorView.Nombre_Giro_Proveedor,giro_ProveedorView.Giro_Proveedor_ID) == "SI")
                {
                    objMensajeServidor.Mensaje = "El nombre del giro esta repetido";
                    str_datos = serializer.Serialize(objMensajeServidor);
                    return str_datos;
                }

                GiroProveedor_Repo.Guardar(giro_ProveedorView);
                objMensajeServidor.Mensaje = "bien";
            }
            catch (Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }

        [HttpPost]
        public string Eliminar(Giro_ProveedorView giro_ProveedorView)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Cls_Mensaje_Servidor objMensajeServidor = new Cls_Mensaje_Servidor();
            string str_datos = "";

            try
            {
                objMensajeServidor.Mensaje = GiroProveedor_Repo.Eliminar(giro_ProveedorView);

            }
            catch(Exception ex)
            {
                objMensajeServidor.Mensaje = ex.Message;
            }

            str_datos = serializer.Serialize(objMensajeServidor);
            return str_datos;

        }
    }
}