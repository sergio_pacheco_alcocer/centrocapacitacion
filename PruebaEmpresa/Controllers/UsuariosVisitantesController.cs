﻿using Newtonsoft.Json;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Infraestructura;
using PruebaEmpresa.Models;
using PruebaEmpresa.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PruebaEmpresa.Controllers
{
    public class UsuariosVisitantesController : Controller
    {


        public ActionResult Salir()
        {

            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        #region(Vistas)

        public ActionResult Login()
        {

            return View();
        }

        public ActionResult LoginPopup()
        {
            return View();
        }

        [SessionExpireVisitante]
        public ActionResult Panel_Control()
        {
            ViewData["nombre_usuario"]=Cls_Sesiones.USUARIO_VISITANTE_NOMBRE;
            ViewData["usuario_id"]=Cls_Sesiones.USUARIO_VISITANTE_ID;

            return View();
        }

        #endregion




        #region(Acciones Panel_Control)


        public ActionResult Actualizar_Respuesta_Visto(string jsonObject)
        {
            Ope_Preguntas_Respuestas obj_repuesta = new Ope_Preguntas_Respuestas();
            string json_resultado = "";

            try
            {
                obj_repuesta = JsonConvert.DeserializeObject<Ope_Preguntas_Respuestas>(jsonObject);

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();

                    const string sqlQuery = "UPDATE Ope_Preguntas_Respuestas SET Visto_Respuesta=1 WHERE Pregunta_Respuesta_ID = @respuesta_id";


                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        cmd.Parameters.AddWithValue("@respuesta_id", obj_repuesta.Pregunta_Respuesta_ID);
                        cmd.ExecuteNonQuery();                        
                    }

                    json_resultado = "{\"mensaje\":\"correcto\"}";
                }
            }
            catch (Exception ex)
            {
                json_resultado = "{\"mensaje\":\"Ha ocurrido un error: "+ ex.Message + "\"}";                
            }

            return Content(json_resultado);
        }




        public ActionResult Consultar_Respuestas_Nuevas()
        {
            List<Cls_Ope_Preguntas_Respuestas> lista = new List<Cls_Ope_Preguntas_Respuestas>();
            string json_resultado = "";

            try
            {

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();

                    const string sqlQuery = "SELECT "+
                                            "op.Usuario_ID, " +
                                            "op.Descripcion_Pregunta," +
                                            "opr.Pregunta_Respuesta_ID, " +
                                            "opr.Respuesta, " +
                                            "opr.Fecha_Creo, " +
                                            "cu.Nombre + ' ' + cu.Apellido_Paterno + ' ' + cu.Apellido_Materno as Nombre_Mentor_Completo, " +
                                            "ce.Nombre_Especialidad " +
                                            "FROM Ope_Preguntas_Respuestas as opr " +
                                            "INNER JOIN Ope_Preguntas as op on opr.Pregunta_ID = op.Pregunta_ID " +
                                            "INNER JOIN Cat_Usuarios as cu on op.Usuario_Mentor_ID = cu.Usuarios_ID " +
                                            "INNER JOIN Cat_Especialidades as ce on cu.Especialidad_ID = ce.Especialidad_ID " +
                                            "WHERE opr.Visto_Respuesta = 0 AND op.Usuario_ID = @usuario_id";


                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        cmd.Parameters.AddWithValue("@usuario_id",Cls_Sesiones.USUARIO_VISITANTE_ID);
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            Cls_Ope_Preguntas_Respuestas element = new Cls_Ope_Preguntas_Respuestas
                            {
                                Pregunta_Respuesta_ID = Convert.ToInt32(dataReader["Pregunta_Respuesta_ID"]),
                                Usuario_Visitante_ID = Convert.ToInt32(dataReader["Usuario_ID"]),
                                Respuesta = Convert.ToString(dataReader["Respuesta"]),
                                Nombre_Mentor_Completo = Convert.ToString(dataReader["Nombre_Mentor_Completo"]),
                                Nombre_Especialidad = Convert.ToString(dataReader["Nombre_Especialidad"]),                                                                
                                Fecha_Creo = Convert.ToDateTime(dataReader["Fecha_Creo"]),
                                Descripcion_Pregunta = Convert.ToString(dataReader["Descripcion_Pregunta"])
                            };

                            lista.Add(element);
                        }
                    }


                    json_resultado = JsonConvert.SerializeObject(lista);
                }
            }
            catch (Exception ex)
            {
                json_resultado = "Ha ocurrido un error: " + ex.Message;
            }

            return Content(json_resultado);
        }





        public ActionResult Consultar_Respuestas_Leidas()
        {
            List<Cls_Ope_Preguntas_Respuestas> lista = new List<Cls_Ope_Preguntas_Respuestas>();
            string json_resultado = "";

            try
            {

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();

                    const string sqlQuery = "SELECT " +
                                            "op.Usuario_ID, " +
                                            "op.Descripcion_Pregunta," +
                                            "opr.Pregunta_Respuesta_ID, " +
                                            "opr.Respuesta, " +
                                            "opr.Fecha_Creo, " +
                                            "cu.Nombre + ' ' + cu.Apellido_Paterno + ' ' + cu.Apellido_Materno as Nombre_Mentor_Completo, " +
                                            "ce.Nombre_Especialidad " +
                                            "FROM Ope_Preguntas_Respuestas as opr " +
                                            "INNER JOIN Ope_Preguntas as op on opr.Pregunta_ID = op.Pregunta_ID " +
                                            "INNER JOIN Cat_Usuarios as cu on op.Usuario_Mentor_ID = cu.Usuarios_ID " +
                                            "INNER JOIN Cat_Especialidades as ce on cu.Especialidad_ID = ce.Especialidad_ID " +
                                            "WHERE opr.Visto_Respuesta = 1 AND op.Usuario_ID = @usuario_id";


                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        cmd.Parameters.AddWithValue("@usuario_id", Cls_Sesiones.USUARIO_VISITANTE_ID);
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            Cls_Ope_Preguntas_Respuestas element = new Cls_Ope_Preguntas_Respuestas
                            {
                                Pregunta_Respuesta_ID = Convert.ToInt32(dataReader["Pregunta_Respuesta_ID"]),
                                Usuario_Visitante_ID = Convert.ToInt32(dataReader["Usuario_ID"]),
                                Respuesta = Convert.ToString(dataReader["Respuesta"]),
                                Nombre_Mentor_Completo = Convert.ToString(dataReader["Nombre_Mentor_Completo"]),
                                Nombre_Especialidad = Convert.ToString(dataReader["Nombre_Especialidad"]),
                                Fecha_Creo = Convert.ToDateTime(dataReader["Fecha_Creo"]),
                                Descripcion_Pregunta = Convert.ToString(dataReader["Descripcion_Pregunta"])
                            };

                            lista.Add(element);
                        }
                    }


                    json_resultado = JsonConvert.SerializeObject(lista);
                }
            }
            catch (Exception ex)
            {
                json_resultado = "Ha ocurrido un error: " + ex.Message;
            }

            return Content(json_resultado);
        }



        public ActionResult Consultar_Preguntas_Pendientes()
        {
            List<Cls_Preguntas_Pendientes> lista = new List<Cls_Preguntas_Pendientes>();
            string json_resultado = "";

            try
            {

                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();

                    const string sqlQuery = "select "+
                                            "op.Pregunta_ID, " +
                                            "op.Descripcion_Pregunta, " +
                                            "cu.Nombre + ' ' + cu.Apellido_Paterno + ' ' + cu.Apellido_Materno as Nombre_Mentor_Completo, " +
                                            "ce.Nombre_Especialidad, " +
                                            "op.Fecha_Creo " +
                                            "from Ope_Preguntas as op " +
                                            "LEFT JOIN Ope_Preguntas_Respuestas as opr on op.Pregunta_ID = opr.Pregunta_ID " +
                                            "INNER JOIN Cat_Usuarios as cu on op.Usuario_Mentor_ID = cu.Usuarios_ID " +
                                            "INNER JOIN Cat_Especialidades as ce on cu.Especialidad_ID = ce.Especialidad_ID " +
                                            "where opr.Pregunta_Respuesta_ID IS NULL and op.Usuario_ID = @usuario_id";


                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        cmd.Parameters.AddWithValue("@usuario_id", Cls_Sesiones.USUARIO_VISITANTE_ID);
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        while (dataReader.Read())
                        {
                            Cls_Preguntas_Pendientes element = new Cls_Preguntas_Pendientes
                            {
                                Pregunta_ID = Convert.ToInt32(dataReader["Pregunta_ID"]),                                
                                Descripcion_Pregunta = Convert.ToString(dataReader["Descripcion_Pregunta"]),
                                Nombre_Mentor_Completo = Convert.ToString(dataReader["Nombre_Mentor_Completo"]),
                                Nombre_Especialidad = Convert.ToString(dataReader["Nombre_Especialidad"]),
                                Fecha_Creo = Convert.ToDateTime(dataReader["Fecha_Creo"]),                                
                            };

                            lista.Add(element);
                        }
                    }


                    json_resultado = JsonConvert.SerializeObject(lista);
                }
            }
            catch (Exception ex)
            {
                json_resultado = "Ha ocurrido un error: " + ex.Message;
            }

            return Content(json_resultado);
        }


        #endregion






        #region(Acciones Login)


        public ActionResult Registrar_Usuario(string jsonObject)
        {
            Cat_Usuarios obj_usuario = new Cat_Usuarios();
            string json_resultado = "";
            int ID = 0;
            string mensaje;
            try
            {
                obj_usuario = JsonConvert.DeserializeObject<Cat_Usuarios>(jsonObject);


                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();
                    const string sqlQuery = "Select top 1 Usuarios_ID from Cat_Usuarios where Correo_Electronico=@email";

                    using (SqlCommand comando = cnx.CreateCommand())
                    {
                        comando.CommandText = sqlQuery;
                        comando.CommandType = CommandType.Text;
                        comando.Parameters.Add(new SqlParameter("@email", obj_usuario.Correo_Electronico));
                        ID = Convert.ToInt32( comando.ExecuteScalar());
                    }

                }

                if (ID > 0) {
                    mensaje = "El correo escrito: " + obj_usuario.Correo_Electronico + " ya esta registrado ";
                    return Content(json_resultado = "{\"mensaje\":\"Ha ocurrido un error: " + mensaje + "\"}");
                }


                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();
                    const string sqlQuery = "INSERT INTO Cat_Usuarios (Rol_ID,Nombre,Apellido_Paterno,Apellido_Materno,Correo_Electronico,Telefono,Password,Estatus,Usuario_Creo,Fecha_Creo,Usuario_Modifico,Fecha_Modifico,CustomerID,SubscriptionID,Especialidad_ID,Nombre_Empresa) VALUES (@rol_id,@nombre,@apellido_paterno,@apellido_materno,@correo_electronico,@telefono,@password,'ACTIVO',NULL,GETDATE(),NULL,NULL,NULL,NULL,NULL,@nombre_empresa); SELECT SCOPE_IDENTITY() as id_nuevo; ";
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {

                        cmd.Parameters.AddWithValue("@rol_id", obj_usuario.Rol_ID);
                        cmd.Parameters.AddWithValue("@nombre", obj_usuario.Nombre);
                        cmd.Parameters.AddWithValue("@apellido_paterno", obj_usuario.Apellido_Paterno);
                        cmd.Parameters.AddWithValue("@apellido_materno", obj_usuario.Apellido_Materno);
                        cmd.Parameters.AddWithValue("@correo_electronico", obj_usuario.Correo_Electronico);
                        cmd.Parameters.AddWithValue("@telefono", obj_usuario.Telefono);
                        cmd.Parameters.AddWithValue("@password", obj_usuario.Password);
                        cmd.Parameters.AddWithValue("@nombre_empresa", obj_usuario.Nombre_Empresa);


                        SqlDataReader dataReader = cmd.ExecuteReader();

                        int id_usuario = 0;
                        if (dataReader.Read())
                        {
                            id_usuario = Convert.ToInt32(dataReader["id_nuevo"]);


                            Cls_Sesiones.USUARIO_VISITANTE_ID = id_usuario;
                            Cls_Sesiones.USUARIO_VISITANTE_NOMBRE = obj_usuario.Nombre + ' ' + obj_usuario.Apellido_Paterno + ' ' + obj_usuario.Apellido_Materno;
                            Cls_Sesiones.NOMBRE_ROL = "Usuario";
                            Cls_Sesiones.EMAIL = obj_usuario.Correo_Electronico;
                            FormsAuthentication.SetAuthCookie(obj_usuario.Correo_Electronico, false);

                            json_resultado = "{\"mensaje\":\"correcto\"}";
                        }
                        else
                        {
                            json_resultado = "{\"mensaje\":\"error\"}";
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                json_resultado = "{\"mensaje\":\"Ha ocurrido un error: " + ex.Message + "\"}";
            }


            return Content(json_resultado);
        }




        public ActionResult Consultar_Usuario_Visitante(string jsonObject)
        {
            Cat_Usuarios obj_usuario = new Cat_Usuarios();
            string json_resultado = "";

            try
            {
                obj_usuario = JsonConvert.DeserializeObject<Cat_Usuarios>(jsonObject);


                using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["EFDbContext"].ToString()))
                {
                    cnx.Open();
                    //const string sqlQuery = "SELECT * FROM Cat_Usuarios WHERE Correo_Electronico = @correo AND Password = @clave";
                    string sqlQuery = "SELECT top 1 u.Usuarios_ID " +
                            ", u.Nombre " +
                            ",u.Apellido_Paterno " +
                            ",u.Apellido_Materno " +
                            ",u.Correo_Electronico " +
                            ",u.Password " +
                            ",u.Fecha_Creo " +
                            ",r.Nombre AS rol " +
                        "FROM dbo.Cat_Usuarios u " +
                        "JOIN dbo.Cat_Roles r ON u.Rol_ID = r.Rol_ID " +
                        "WHERE r.Nombre = 'Usuario' " +
                        "    AND u.Correo_Electronico = @correo " +
                        "    AND u.Password = @clave ";

                    using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                    {
                        cmd.Parameters.AddWithValue("@correo", obj_usuario.Correo_Electronico);
                        cmd.Parameters.AddWithValue("@clave", obj_usuario.Password);
                        SqlDataReader dataReader = cmd.ExecuteReader();

                        
                        
                        if (dataReader.Read()) {                            

                            Cat_Usuarios resultado = new Cat_Usuarios
                            {
                                Usuarios_ID = Convert.ToInt32(dataReader["Usuarios_ID"]),
                                Nombre = Convert.ToString(dataReader["Nombre"]),
                                Apellido_Paterno = Convert.ToString(dataReader["Apellido_Paterno"]),
                                Apellido_Materno = Convert.ToString(dataReader["Apellido_Materno"]),
                                Correo_Electronico = Convert.ToString(dataReader["Correo_Electronico"]),
                                Password = Convert.ToString(dataReader["Password"]),
                                Fecha_Creo = Convert.ToDateTime(dataReader["Fecha_Creo"])
                            };

                            Cls_Sesiones.USUARIO_VISITANTE_ID = resultado.Usuarios_ID;
                            Cls_Sesiones.USUARIO_VISITANTE_NOMBRE = resultado.Nombre + " " + resultado.Apellido_Paterno + " " + resultado.Apellido_Materno;
                            Cls_Sesiones.NOMBRE_ROL = Convert.ToString(dataReader["ROL"]);
                            Cls_Sesiones.EMAIL = resultado.Correo_Electronico;
                            FormsAuthentication.SetAuthCookie(resultado.Correo_Electronico, false);

                            json_resultado = JsonConvert.SerializeObject(resultado);
                        }
                        else
                        {
                            json_resultado = "{\"mensaje\":\"No se ha encontrado al usuario\"}";
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {                
                json_resultado = "{\"mensaje\":\"Ha ocurrido un error: "+ex.Message+"\"}";
            }


            return Content(json_resultado);
        }




        public ActionResult Verificar_Login()
        {
            string json_resultado = "";           
            try
            {                
                if(Cls_Sesiones.USUARIO_VISITANTE_NOMBRE != "")                
                    json_resultado = "{\"mensaje\":\"correcto\"}";                
                else                
                    json_resultado = "{\"mensaje\":\"error\"}";
                
            }
            catch (Exception ex)
            {
                json_resultado = "{\"mensaje\":\"error\"}";
            }

            return Content(json_resultado);
        }

        #endregion
    }

}