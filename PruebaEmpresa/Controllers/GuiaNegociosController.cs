﻿using PruebaEmpresa.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaEmpresa.Controllers
{
    public class GuiaNegociosController : Controller
    {
        // GET: GuiaNegocios
        public ActionResult Guia_Negocios()
        {
            ViewData["actualizacion"] = Cls_Sesiones.FECHA_ACTUALIZACION;

            return View();
        }
        
    }
}