﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaEmpresa.Infraestructura.Enums
{

    public enum Roles_Enum
    {
        Administrador,
        Usuario,
        Mentor
    }
}