﻿using PruebaEmpresa.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PruebaEmpresa.Infraestructura
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            HttpContext context = HttpContext.Current;

            // check session here

            if (Cls_Sesiones.EMAIL.Length == 0)
            {
                filterContext.Result = new RedirectResult("~/Login/Verificar");
                return;
            }

            base.OnActionExecuted(filterContext);
        }
    }
}