﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Contexto;
using PruebaEmpresa.Servicios;
using PruebaEmpresa.Servicios.Interfaces;

namespace PruebaEmpresa.Infraestructura
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {

        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {

            return controllerType == null
                   ? null
                   : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<ICorreo_Repo>().To<Correo_Repo>(); 
            ninjectKernel.Bind<IMenuPrincipal_Repo>().To<EFMenuPrincipal_Repo>();
            ninjectKernel.Bind<IGiroProveedor_Repo>().To<EFGiroProveedor_Repo>();
            ninjectKernel.Bind<IProveedores_Repo>().To<EFProveedores_Repo>();
            ninjectKernel.Bind<IUsuarios_Repo>().To<EFUsuarios_Repo>();
            ninjectKernel.Bind<IRoles_Repo>().To<EFRoles_Repo>();
            ninjectKernel.Bind<IParametroCorreo_Repo>().To<EFParametrosCorreo_Repo>();
            ninjectKernel.Bind<ICargos_Repo>().To<EFCargos_Repo>();
            ninjectKernel.Bind<ICombo_Repo>().To<Combo_Repo>();
            ninjectKernel.Bind<IRegistroPagos_Repo>().To<EFRegistro_Pago>();
            ninjectKernel.Bind<ICancelarSuscripcion_Repo>().To<EFCancelarSuscripcion_Repo>();
            ninjectKernel.Bind<IEspecialidad_Repo>().To<EFEspecialidad_Repo>();
            ninjectKernel.Bind<IPreguntas_Repo>().To<EFPreguntas_Repo>();
            ninjectKernel.Bind<IPreguntas_Repuesta_Repo>().To<EFPregunta_Repuesta_Repo>();
        }
    }
}