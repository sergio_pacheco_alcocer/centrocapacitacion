﻿using PruebaEmpresa.Infraestructura.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PruebaEmpresa.Infraestructura.Permisos
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class,
    Inherited = true, AllowMultiple = true)]
    public class RoleAuthorizeAttribute :  AuthorizeAttribute
    {
        private static readonly string[] RolesWhoAlwaysHaveAccess =
     new[] { Roles_Enum.Administrador.ToString()};


        public RoleAuthorizeAttribute(params object[] roles)
        {
            if (roles.Any(r => r.GetType().BaseType != typeof(Enum)))
            {
                throw new ArgumentException(
                    "The roles parameter may only contain enums",
                    "roles");
            }

            var temp = roles.Select(r =>
                    AddSpacesToSentence(Enum.GetName(r.GetType(), r)))
                .ToList();

            temp.AddRange(RolesWhoAlwaysHaveAccess);

            Roles = string.Join(",", temp);
        }


        private string AddSpacesToSentence(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            var newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]) && text[i - 1] != ' ' && text[i - 1] != '_')
                {
                    newText.Append(' ');
                }

                newText.Append(text[i]);
            }
            return newText.ToString();
        }

    }
}