﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto;

namespace PruebaEmpresa.Infraestructura.Permisos
{
    public class RolePermissionManager
    {
        protected EFDbContext db;

        public RolePermissionManager(EFDbContext context)
        {
            db = context;
        }

        public string[] ResolveRoleName(string loginName)
        {

            string[] resultado = new string[1];

            try
            {
                var usuario = db.Cat_Usuarios.Where(x => x.Correo_Electronico == loginName).FirstOrDefault();

                if (usuario == null)
                {
                    return new string[0];
                }

                if (usuario.Cat_Roles == null)
                {
                    return new string[0];
                }

                resultado[0] = usuario.Cat_Roles.Nombre;
            }
            catch (Exception ex)
            {
                return new string[0];
            }
            return resultado;

        }
    }
}