﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;

namespace PruebaEmpresa.Contexto
{
    public class EFCancelarSuscripcion_Repo : ICancelarSuscripcion_Repo
    {
        public void Registrar_Cancelacion(EFDbContext eFDbContext, CancelarSuscripcionView cancelarSuscripcionView)
        {
            Ope_Cor_Cancelar_Suscripcion ope_Cor_Cancelar_Suscripcion = new Ope_Cor_Cancelar_Suscripcion();
            ope_Cor_Cancelar_Suscripcion.Cargo_ID = cancelarSuscripcionView.Cargo_ID;
            ope_Cor_Cancelar_Suscripcion.Fecha_Creo = DateTime.Now;
            ope_Cor_Cancelar_Suscripcion.Usuarios_ID = cancelarSuscripcionView.Usuarios_ID;
            ope_Cor_Cancelar_Suscripcion.Identificador = cancelarSuscripcionView.Identificador;

            eFDbContext.Ope_Cor_Cancelar_Suscripcion.Add(ope_Cor_Cancelar_Suscripcion);
            eFDbContext.SaveChanges();

        }
    }
}