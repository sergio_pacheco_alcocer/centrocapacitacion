﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface IRoles_Repo
    {
        IQueryable<Cat_Roles> Cat_Roles { get; }
        RolesView Obtener_Un_Rol(string Nombre);
        string Estar_Repetido_Rol(string Nombre, int Rol_ID);
        void Registar_Rol(RolesView rolesView);
        List<ComboView> Obtener_Roles_Combo();
    }
}
