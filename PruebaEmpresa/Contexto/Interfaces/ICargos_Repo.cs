﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface ICargos_Repo
    {

        void Guardar(CargosView cargosView);
        string Eliminar(int Cargo_ID);
        CargosView Obtener_Un_Cargo(int Cargo_ID);
        List<CargosView> Obtener_Cargos();
        List<CargosView> Obtener_Cargos_Activos();
        CargosView Obtener_Un_Cargo(string ProductoFinancieroID);
    }
}
