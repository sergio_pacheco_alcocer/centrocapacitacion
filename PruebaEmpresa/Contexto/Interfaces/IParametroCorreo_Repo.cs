﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Models;


namespace PruebaEmpresa.Contexto.Interfaces
{
    public interface IParametroCorreo_Repo
    {

        void Guardar(ParametrosCorreoView parametrosCorreoView);
        string Eliminar(int Parametro_Correo_ID);
        List<ParametrosCorreoView> Obtener_Parametros_Correo();
        ParametrosCorreoView Obtener_Un_Parametro();
        ParametrosCorreoView Obtener_Un_Parametro(int Parametro_Correo_ID);
    }
}