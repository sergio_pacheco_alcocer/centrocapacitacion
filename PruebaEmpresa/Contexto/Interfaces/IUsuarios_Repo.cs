﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto;

namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface IUsuarios_Repo
    {
        IQueryable<Cat_Usuarios> Cat_Usuarios { get; }
        string Esta_Repetido_Usuario(string Correo_Electronico, int Usuarios_ID);
        void Registrar_Usuario(UsuariosView usuariosView);
        string Eliminar_Usuario(int Usuarios_ID);

        UsuariosView Obtener_Usuario(string Correo_Electronico, string Password);
        UsuariosView Obtener_Usuario(string Correo_Electronico);
        UsuariosView Obtener_Usuario(int Usuario_ID);
        UsuariosView Obtener_Usuario_Customer(string CustomerID);

        MentorView Obtener_Un_Mentor(int Usuarios_ID);

        void Agregar_Customer_ID(int Usuario_ID, string Customer_ID);
        void Agergar_SubscriptionID(EFDbContext eFDbContext, int Usuario_ID, string SubscriptionID);
        void Agregar_SubscriptionID(int Usuario_ID, string SubscriptionID);
        void Eliminar_SucriptionID(EFDbContext eFDbContext, int Usuario_ID);

        IQueryable<UsuariosView> Obtener_Usuarios(string sortOrder, string searchString, int? Rol_ID);

        IQueryable<MentorView> Obtener_Mentores();
    }
}
