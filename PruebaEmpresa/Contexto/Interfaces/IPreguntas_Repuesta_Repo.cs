﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto;


namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface IPreguntas_Repuesta_Repo
    {
        void Guardar(PreguntasRespuestaView preguntasRespuestaView);
        List<PreguntasRespuestaView> Obtener_Respuestas(int Pregunta_ID);
    }
}
