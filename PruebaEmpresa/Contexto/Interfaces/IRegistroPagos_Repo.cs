﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Contexto;
using PruebaEmpresa.Models;


namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface IRegistroPagos_Repo
    {
        void Guardar(EFDbContext eFDbContext, RegistroPagosView registroPagosView);
        RegistroPagosView Obtener_Registro_Pago_Actual(int Usuario_ID);
        RegistroPagosView Obtener_Registro_Pago_Actual_Suscripcion( string SubscriptionID);
    }
}
