﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto;

namespace PruebaEmpresa.Contexto.Interfaces
{
    public interface IProveedores_Repo
    {


        Cls_Paginado<ProveedorView> Obtener_Proveedores(int offset, int limit, int? giro_proveedor_id, string busqueda);
        void Guardar(ProveedorView proveedorView);
        ProveedorView Obtener_Un_Proveedor(int Proveedor_ID);
        List<ProveedorView> Obtener_Proveedores(int Usuarios_ID);
        void Actualizar_Vigencia_Proveedor(EFDbContext eFDbContext, int Usuarios_ID, DateTime FechaInicio, DateTime FechaFin);
    }
}