﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface IGiroProveedor_Repo
    {
        List<ComboView> Obtener_Giros_Industriales();

        Cls_Paginado<Giro_ProveedorView> Obtener_Giro_Proveedores(int offset, int limit, string search);
        void Guardar(Giro_ProveedorView giro_Proveedor);
        string Eliminar(Giro_ProveedorView giro_Proveedor);
        string Esta_Repetido_Giro(string Nombre_Giro_Proveedor, int Giro_Proveedor_ID);
    }
}
