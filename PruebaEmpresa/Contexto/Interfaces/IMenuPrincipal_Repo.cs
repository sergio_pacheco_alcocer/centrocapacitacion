﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;


namespace PruebaEmpresa.Contexto.Interfaces
{
   public interface IMenuPrincipal_Repo
    {
       List< Menu_Principal_View> Obtener_Menu_Principal();
        Menu_Principal_View ObtenerFaseUno(int Menu_Principal_ID);
    }
}
