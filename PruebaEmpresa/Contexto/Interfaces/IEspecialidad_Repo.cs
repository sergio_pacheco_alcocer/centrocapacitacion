﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;

namespace PruebaEmpresa.Contexto.Interfaces
{
  public  interface IEspecialidad_Repo
    {

        IQueryable<Cat_Especialidades> Cat_Especialidades { get; }
        string Eliminar(int Especialidad_ID);
        void Guardar(EspecialidadViewModel especialidadViewModel);
        EspecialidadViewModel Obtener_Una_Especialidad(int Especialidad_ID);
        string Esta_Repetido_Especialidad(string Nombre_Especialidad, int Especialidad_ID);
        List<ComboView> Obtener_Especialidades_Combo();
    }
}
