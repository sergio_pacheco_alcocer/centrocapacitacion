﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Contexto.Interfaces
{
  public  interface IPreguntas_Repo
    {
        IQueryable<Ope_Preguntas> Ope_Preguntas { get; }

        void Guardar(PreguntasView preguntasView);
        string Eliminar(int Pregunta_ID);
        PreguntasView Obtener_Una_Pregunta(int Pregunta_ID);
        void Actualizar_Visto_Pregunta(int Pregunta_ID);
        int Preguntas_Sin_Leer(int Usuario_ID);
    }
}
