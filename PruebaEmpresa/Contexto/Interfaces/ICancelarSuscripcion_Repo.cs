﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto;

namespace PruebaEmpresa.Contexto.Interfaces
{
  public  interface ICancelarSuscripcion_Repo
    {
        void Registrar_Cancelacion(EFDbContext eFDbContext,  CancelarSuscripcionView cancelarSuscripcionView);

    }
}
