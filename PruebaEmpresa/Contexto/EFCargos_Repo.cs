﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace PruebaEmpresa.Contexto
{
    public class EFCargos_Repo : ICargos_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public string Eliminar(int Cargo_ID)
        {
            string respuesta = "";


            try
            {

                Ope_Cor_Cargos ope_Cor_Cargos = Contexto.Ope_Cor_Cargos.Find(Cargo_ID);
                Contexto.Ope_Cor_Cargos.Remove(ope_Cor_Cargos);
                respuesta = "bien";
                Contexto.SaveChanges();

            }catch(DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }

            }

            return respuesta;
        }

        public void Guardar(CargosView cargosView)
        {
            if(cargosView.Cargo_ID == 0)
            {
                Ope_Cor_Cargos ope_Cor_Cargos = new Ope_Cor_Cargos();

                ope_Cor_Cargos.Dias = cargosView.Dias;
                ope_Cor_Cargos.Estatus = cargosView.Estatus;
                ope_Cor_Cargos.Precio = cargosView.Precio;
                ope_Cor_Cargos.Nombre_Cargo = cargosView.Nombre_Cargo;
                ope_Cor_Cargos.Usuario_Creo = cargosView.Usuario_Creo;
                ope_Cor_Cargos.ProductoFinancieroID = cargosView.ProductoFinancieroID;
                ope_Cor_Cargos.Fecha_Creo = DateTime.Now;

                Contexto.Ope_Cor_Cargos.Add(ope_Cor_Cargos);
            }
            else
            {
                Ope_Cor_Cargos ope_Cor_Cargos = Contexto.Ope_Cor_Cargos.Find(cargosView.Cargo_ID);

                ope_Cor_Cargos.Dias = cargosView.Dias;
                ope_Cor_Cargos.Estatus = cargosView.Estatus;
                ope_Cor_Cargos.Precio = cargosView.Precio;
                ope_Cor_Cargos.Nombre_Cargo = cargosView.Nombre_Cargo;
                ope_Cor_Cargos.ProductoFinancieroID = cargosView.ProductoFinancieroID;
                ope_Cor_Cargos.Usuario_Modifico = cargosView.Usuario_Creo;
                ope_Cor_Cargos.Fecha_Modifico = DateTime.Now;
            }

            Contexto.SaveChanges();
        }

        public List<CargosView> Obtener_Cargos()
        {
            return Contexto.Ope_Cor_Cargos
                 .OrderBy(x => x.Dias)
                 .Select(x => new CargosView
                 {
                     Cargo_ID = x.Cargo_ID,
                     Dias = x.Dias,
                     Estatus = x.Estatus,
                     Nombre_Cargo = x.Nombre_Cargo,
                     Precio = x.Precio,
                     ProductoFinancieroID = x.ProductoFinancieroID == null ? "" : x.ProductoFinancieroID
                 }).ToList();
        }

        public List<CargosView> Obtener_Cargos_Activos()
        {
            return Contexto.Ope_Cor_Cargos
                .Where(x=> x.Estatus == "ACTIVO")
             .OrderBy(x => x.Dias)
             .Select(x => new CargosView
             {
                 Cargo_ID = x.Cargo_ID,
                 Dias = x.Dias,
                 Estatus = x.Estatus,
                 Nombre_Cargo = x.Nombre_Cargo,
                 Precio = x.Precio,
                 ProductoFinancieroID = x.ProductoFinancieroID == null ? "" : x.ProductoFinancieroID
             }).ToList();
        }

        public CargosView Obtener_Un_Cargo(int Cargo_ID)
        {
            return Contexto.Ope_Cor_Cargos.Where(x => x.Cargo_ID == Cargo_ID)
                   .Select(x => new CargosView
                   {
                       Cargo_ID = x.Cargo_ID,
                       Dias = x.Dias,
                       Estatus = x.Estatus,
                       Nombre_Cargo = x.Nombre_Cargo,
                       Precio = x.Precio,
                       ProductoFinancieroID = x.ProductoFinancieroID == null ? "": x.ProductoFinancieroID
                   }).FirstOrDefault();
        }

        public CargosView Obtener_Un_Cargo(string ProductoFinancieroID)
        {
            return Contexto.Ope_Cor_Cargos.Where(x => x.ProductoFinancieroID == ProductoFinancieroID)
                .Select(x => new CargosView
                {
                    Cargo_ID = x.Cargo_ID,
                    Dias = x.Dias,
                    Estatus = x.Estatus,
                    Nombre_Cargo = x.Nombre_Cargo,
                    Precio = x.Precio,
                    ProductoFinancieroID = x.ProductoFinancieroID == null ? "" : x.ProductoFinancieroID
                }).FirstOrDefault();
        }
    }
}