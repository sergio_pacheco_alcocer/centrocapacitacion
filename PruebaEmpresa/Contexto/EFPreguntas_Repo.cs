﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace PruebaEmpresa.Contexto
{
    public class EFPreguntas_Repo : IPreguntas_Repo
    {
        private EFDbContext Contexto = new EFDbContext();
        public IQueryable<Ope_Preguntas> Ope_Preguntas => Contexto.Ope_Preguntas;

        public void Actualizar_Visto_Pregunta(int Pregunta_ID)
        {
            Ope_Preguntas ope_Preguntas = Contexto.Ope_Preguntas.Find(Pregunta_ID);
            ope_Preguntas.Visto = true;
            Contexto.SaveChanges();
        }

        public string Eliminar(int Pregunta_ID)
        {
            string respuesta = "";

            try
            {
                Ope_Preguntas ope_Preguntas = Contexto.Ope_Preguntas.Find(Pregunta_ID);
                Contexto.Ope_Preguntas.Remove(ope_Preguntas);
                respuesta = "bien";
                Contexto.SaveChanges();

            }
            catch(DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public void Guardar(PreguntasView preguntasView)
        {

            if (preguntasView.Pregunta_ID == 0)
            {

                Ope_Preguntas ope_Preguntas = new Ope_Preguntas();
                ope_Preguntas.Usuario_Creo = preguntasView.Usuario_Creo;
                ope_Preguntas.Usuario_ID = preguntasView.Usuario_ID;
                ope_Preguntas.Usuario_Mentor_ID = preguntasView.Usuario_Mentor_ID;
                ope_Preguntas.Visto = preguntasView.Visto;
                ope_Preguntas.Fecha_Creo = DateTime.Now;
                ope_Preguntas.Descripcion_Pregunta = preguntasView.Descripcion_Pregunta;
                Contexto.Ope_Preguntas.Add(ope_Preguntas);
            }
            else {

                Ope_Preguntas ope_Preguntas = Contexto.Ope_Preguntas.Find(preguntasView.Pregunta_ID);
                ope_Preguntas.Usuario_Modifico = preguntasView.Usuario_Creo;
                ope_Preguntas.Usuario_ID = preguntasView.Usuario_ID;
                ope_Preguntas.Usuario_Mentor_ID = preguntasView.Usuario_Mentor_ID;
                ope_Preguntas.Visto = preguntasView.Visto;
                ope_Preguntas.Fecha_Modifico = DateTime.Now;
                ope_Preguntas.Descripcion_Pregunta = preguntasView.Descripcion_Pregunta;
            }

            Contexto.SaveChanges();
        }

        public PreguntasView Obtener_Una_Pregunta(int Pregunta_ID)
        {
            return Contexto.Ope_Preguntas.Where(x => x.Pregunta_ID == Pregunta_ID)
                .Select(x => new PreguntasView
                {
                    Descripcion_Pregunta = x.Descripcion_Pregunta,
                    Fecha_Creo = x.Fecha_Creo,
                    Nombre_Mentor = (x.Cat_Usuarios_Mentor.Nombre + " " + x.Cat_Usuarios_Mentor.Apellido_Paterno) + " " + (x.Cat_Usuarios_Mentor.Apellido_Materno ?? ""),
                    Nombre_Usuario = (x.Cat_Usuarios.Nombre + " " + x.Cat_Usuarios.Apellido_Paterno) + " " + (x.Cat_Usuarios.Apellido_Materno ?? ""),
                    Visto = x.Visto,
                    Usuario_ID = x.Usuario_ID,
                    Usuario_Mentor_ID = x.Usuario_Mentor_ID,
                    Pregunta_ID = x.Pregunta_ID
                }).FirstOrDefault();
        }

        public int Preguntas_Sin_Leer(int Usuario_ID)
        {
            return Contexto.Ope_Preguntas.Where(x => x.Usuario_Mentor_ID == Usuario_ID && x.Visto == false).Count();
        }
    }
}