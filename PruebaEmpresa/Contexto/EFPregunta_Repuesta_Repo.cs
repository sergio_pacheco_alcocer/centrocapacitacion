﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;

namespace PruebaEmpresa.Contexto
{
    public class EFPregunta_Repuesta_Repo : IPreguntas_Repuesta_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public void Guardar(PreguntasRespuestaView preguntasRespuestaView)
        {
            
            if(preguntasRespuestaView.Pregunta_Respuesta_ID == 0)
            {
                Ope_Preguntas_Respuestas ope_Preguntas_Respuestas = new Ope_Preguntas_Respuestas();
                ope_Preguntas_Respuestas.Pregunta_ID = preguntasRespuestaView.Pregunta_ID;
                ope_Preguntas_Respuestas.Respuesta = preguntasRespuestaView.Respuesta.Trim();
                ope_Preguntas_Respuestas.Usuario_ID = preguntasRespuestaView.Usuario_ID;
                ope_Preguntas_Respuestas.Fecha_Creo = DateTime.Now;
                ope_Preguntas_Respuestas.Usuario_Creo = preguntasRespuestaView.Usuario_Creo;
                ope_Preguntas_Respuestas.Tipo = preguntasRespuestaView.Tipo;
                ope_Preguntas_Respuestas.Visto_Respuesta = preguntasRespuestaView.Visto_Respuesta;
                Contexto.Ope_Preguntas_Respuestas.Add(ope_Preguntas_Respuestas);
                Contexto.SaveChanges();
            }
        }

        public List<PreguntasRespuestaView> Obtener_Respuestas(int Pregunta_ID)
        {
            return Contexto.Ope_Preguntas_Respuestas.Where(x => x.Pregunta_ID == Pregunta_ID)
                 .OrderByDescending(x => x.Fecha_Creo)
                 .Select(x => new PreguntasRespuestaView
                 {
                     Fecha_Creo = x.Fecha_Creo,
                     Pregunta_ID = x.Pregunta_ID,
                     Pregunta_Respuesta_ID = x.Pregunta_Respuesta_ID,
                     Respuesta = x.Respuesta.Trim().Replace(".", " <br/>"),
                     Usuario_Creo = x.Usuario_Creo,
                     Usuario_ID = x.Usuario_ID,
                     Tipo = x.Tipo ?? ""

                 }).ToList();
        }
    }
}