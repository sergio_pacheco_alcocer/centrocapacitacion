﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Models;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace PruebaEmpresa.Contexto
{
    public class EFGiroProveedor_Repo : IGiroProveedor_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public string Eliminar(Giro_ProveedorView giro_Proveedor)
        {
            string respuesta = "";

            try {


                Cat_Giro_Proveedor cat_Giro_Proveedor = Contexto.Cat_Giro_Proveedor.Find(giro_Proveedor.Giro_Proveedor_ID);
                Contexto.Cat_Giro_Proveedor.Remove(cat_Giro_Proveedor);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch(DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public string Esta_Repetido_Giro(string Nombre_Giro_Proveedor, int Giro_Proveedor_ID)
        {
            string respuesta = "NO";

            var buscarGiro = Contexto.Cat_Giro_Proveedor.Where(x => x.Nombre_Giro_Proveedor == Nombre_Giro_Proveedor && x.Giro_Proveedor_ID != Giro_Proveedor_ID);

            if (buscarGiro.Any())
            {
                respuesta = "SI";
            }

            return respuesta;
        }

        public void Guardar(Giro_ProveedorView giro_Proveedor)
        {
            if (giro_Proveedor.Giro_Proveedor_ID == 0)
            {
                Cat_Giro_Proveedor cat_Giro_Proveedor = new Cat_Giro_Proveedor();
                cat_Giro_Proveedor.Nombre_Giro_Proveedor = giro_Proveedor.Nombre_Giro_Proveedor;
                cat_Giro_Proveedor.Estatus = giro_Proveedor.Estatus;
                cat_Giro_Proveedor.Fecha_Creo = DateTime.Now;
                Contexto.Cat_Giro_Proveedor.Add(cat_Giro_Proveedor);
            }
            else {
                Cat_Giro_Proveedor cat_Giro_Proveedor = Contexto.Cat_Giro_Proveedor.Find(giro_Proveedor.Giro_Proveedor_ID);
                cat_Giro_Proveedor.Nombre_Giro_Proveedor = giro_Proveedor.Nombre_Giro_Proveedor;
                cat_Giro_Proveedor.Estatus = giro_Proveedor.Estatus;
                cat_Giro_Proveedor.Fecha_Modifico = DateTime.Now;
            }

            Contexto.SaveChanges();
        }

        public List<ComboView> Obtener_Giros_Industriales()
        {
            return Contexto.Cat_Giro_Proveedor.Where(x => x.Estatus == "ACTIVO")
                  .Select(x => new ComboView
                  {
                      id = x.Giro_Proveedor_ID,
                      name = x.Nombre_Giro_Proveedor,
                      clave = ""
                  }).ToList();
        }

        public Cls_Paginado<Giro_ProveedorView> Obtener_Giro_Proveedores(int offset, int limit, string search)
        {
            Cls_Paginado<Giro_ProveedorView> Paginado;

            var busquedaFiltro = Contexto.Cat_Giro_Proveedor.Where(x => string.IsNullOrEmpty(search) || x.Nombre_Giro_Proveedor.Contains(search))
                .OrderBy(x => x.Nombre_Giro_Proveedor)
                .Select(x => new Giro_ProveedorView
                {
                    Estatus = x.Estatus,
                    Giro_Proveedor_ID = x.Giro_Proveedor_ID,
                    Nombre_Giro_Proveedor = x.Nombre_Giro_Proveedor
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();
            Paginado = new Cls_Paginado<Giro_ProveedorView>(datos_visualizar, busquedaFiltro.Count());

            return Paginado;
        }
    }
}