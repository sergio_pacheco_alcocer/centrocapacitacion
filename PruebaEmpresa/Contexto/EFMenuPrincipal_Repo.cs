﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;

namespace PruebaEmpresa.Contexto
{
    public class EFMenuPrincipal_Repo : IMenuPrincipal_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public Menu_Principal_View ObtenerFaseUno(int Menu_Principal_ID)
        {
            return Contexto.Cat_Menu_Principal.Where(x => x.Menu_Principal_ID == Menu_Principal_ID)
                .Select(x => new Menu_Principal_View
                {
                    Nombre = x.Nombre,
                    Menu_Principal_ID = x.Menu_Principal_ID,
                    Lista_Fase_Uno = x.Cat_Fase_Uno.Select(y => new FaseUnoView
                    {

                        Nombre_Fase_Uno = y.Nombre_Fase_Uno,
                        Lista_Fase_Dos = y.Cat_Fase_Dos.Select(z => new FaseDosView
                        {

                            Nombre_Fase_Dos = z.Nombre_Fase_Dos,
                            Fase_Dos_ID = z.Fase_Dos_ID,
                            Fase_Uno_ID = z.Fase_Uno_ID

                        }).ToList()

                    }).ToList()



                }).FirstOrDefault();
        }

        public List<Menu_Principal_View> Obtener_Menu_Principal()
        {
            return Contexto.Cat_Menu_Principal.Select(x => new Menu_Principal_View
            {
                Menu_Principal_ID = x.Menu_Principal_ID,
                Nombre = x.Nombre,
                Accion = x.Accion,
                Controlador = x.Controlador

            }).ToList();
        }
    }
}