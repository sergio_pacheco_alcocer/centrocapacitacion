﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;
using PruebaEmpresa.Contexto;

namespace PruebaEmpresa.Contexto
{
    public class EFRegistro_Pago : IRegistroPagos_Repo
    {
        private EFDbContext Contexto = new EFDbContext();


        public void Guardar(EFDbContext eFDbContext, RegistroPagosView registroPagosView)
        {

            Ope_Cor_Registro_Pagos ope_Cor_Registro_Pagos = new Ope_Cor_Registro_Pagos();
            ope_Cor_Registro_Pagos.Cargo_ID = registroPagosView.Cargo_ID;
            ope_Cor_Registro_Pagos.Identificador = registroPagosView.Identificador;
            ope_Cor_Registro_Pagos.Usuarios_ID = registroPagosView.Usuarios_ID;
            ope_Cor_Registro_Pagos.Fecha_Creo = DateTime.Now;
            ope_Cor_Registro_Pagos.Usuario_Creo = registroPagosView.Usuario_Creo;
            ope_Cor_Registro_Pagos.Fecha_Inicio_Vigencia = registroPagosView.Fecha_Inicio_Vigencia;
            ope_Cor_Registro_Pagos.Fecha_Fin_Vigencia = registroPagosView.Fecha_Fin_Vigencia;

            eFDbContext.Ope_Cor_Registro_Pagos.Add(ope_Cor_Registro_Pagos);
            eFDbContext.SaveChanges();

        }

        public RegistroPagosView Obtener_Registro_Pago_Actual(int Usuario_ID)
        {
            return Contexto.Ope_Cor_Registro_Pagos.Where(x => x.Usuarios_ID == Usuario_ID && DateTime.Now <= x.Fecha_Fin_Vigencia)
                    .OrderByDescending(x => x.Cargo_ID)
                    .Select(x => new RegistroPagosView
                    {
                        Cargo_ID = x.Cargo_ID,
                        Registro_Pago_ID = x.Registro_Pago_ID,
                    })
                    .FirstOrDefault();
                    
        }

        public RegistroPagosView Obtener_Registro_Pago_Actual_Suscripcion(string SubscriptionID)
        {
            return Contexto.Ope_Cor_Registro_Pagos.Where(x => x.Identificador == SubscriptionID).
                OrderByDescending(x => x.Registro_Pago_ID)
                .Select(x => new RegistroPagosView
                {
                    Cargo_ID = x.Cargo_ID,
                    Registro_Pago_ID = x.Registro_Pago_ID,
                    Identificador = x.Identificador
                }).FirstOrDefault();
        }
    }
}