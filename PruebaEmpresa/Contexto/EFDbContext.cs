﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Entidades;

namespace PruebaEmpresa.Contexto
{
    public class EFDbContext : DbContext
    {


        public EFDbContext()
        {


        }
        public DbSet<Cat_Proveedor> Cat_Proveedor { get; set; }
        public DbSet<Cat_Giro_Proveedor> Cat_Giro_Proveedor { get; set; }
        public DbSet<Cat_Menu_Principal> Cat_Menu_Principal { get; set; }
        public DbSet<Cat_Fase_Uno> Cat_Fase_Uno { get; set; }
        public DbSet<Cat_Roles> Cat_Roles { get; set; }
        public DbSet<Cat_Usuarios> Cat_Usuarios { get; set; }
        public DbSet<Cat_Parametro_Correo> Cat_Parametro_Correo { get; set; }
        public DbSet<Ope_Cor_Cargos> Ope_Cor_Cargos { get; set; }
        public DbSet<Ope_Cor_Registro_Pagos> Ope_Cor_Registro_Pagos { get; set; }
        public DbSet<Ope_Cor_Cancelar_Suscripcion> Ope_Cor_Cancelar_Suscripcion { get; set; }
        public DbSet<Cat_Especialidades> Cat_Especialidades { get; set; }
        public DbSet<Ope_Preguntas> Ope_Preguntas { get; set; }
        public DbSet<Ope_Preguntas_Respuestas> Ope_Preguntas_Respuestas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cat_Proveedor>().Property(x => x.Latitud).HasPrecision(20, 15);
            modelBuilder.Entity<Cat_Proveedor>().Property(x => x.Longitud).HasPrecision(20, 15);
        }
    }
}