﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Ope_Cor_Cargos")]
    public class Ope_Cor_Cargos
    {
        [Key]
        public int Cargo_ID { get; set; }
        public string Nombre_Cargo { get; set; }
        public decimal Precio { get; set; }
        public int Dias { get; set; }
        public  string Estatus { get; set; }
        public string ProductoFinancieroID { get; set; }
        public  string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
    }
}