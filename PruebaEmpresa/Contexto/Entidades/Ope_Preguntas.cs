﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Ope_Preguntas")]
    public class Ope_Preguntas
    {
        [Key]
        public int Pregunta_ID { get; set; }
        public int Usuario_ID { get; set; }
        public int Usuario_Mentor_ID { get; set; }
        public string Descripcion_Pregunta { get; set; }
        public bool Visto { get; set; }
        public  string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }

        [ForeignKey("Usuario_ID")]
        public Cat_Usuarios Cat_Usuarios { get; set; }
        [ForeignKey("Usuario_Mentor_ID")]
        public Cat_Usuarios Cat_Usuarios_Mentor { get; set; }

        public virtual ICollection<Ope_Preguntas_Respuestas> Ope_Preguntas_Respuestas { get; set; }
    }
}