﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Fase_Dos")]
    public class Cat_Fase_Dos
    {
        [Key]
        public int Fase_Dos_ID { get; set; }
        public int Fase_Uno_ID { get; set; }
        public string Nombre_Fase_Dos { get; set; }
        public string Controlador { get; set; }
        public string Accion { get; set; }

    }
}