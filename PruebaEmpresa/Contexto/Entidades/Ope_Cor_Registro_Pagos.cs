﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Ope_Cor_Registro_Pagos")]
    public class Ope_Cor_Registro_Pagos
    {
         [Key]
         public  int Registro_Pago_ID { get; set; }
         public  int Usuarios_ID { get; set; }
         public  int Cargo_ID { get; set; }
         public  string Identificador { get; set; }
         public  string Usuario_Creo { get; set; }
         public DateTime Fecha_Creo { get; set; }
         public  string Usuario_Modifico { get; set; }
         public DateTime? Fecha_Modifico { get; set; }
         public DateTime Fecha_Inicio_Vigencia { get; set; }
         public DateTime Fecha_Fin_Vigencia { get; set; }
    }
}