﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Parametro_Correo")]
    public class Cat_Parametro_Correo
    {
        [Key]
        public int Parametro_Correo_ID { get; set; }
        public string Email { get; set; }
        public string Host_Email { get; set; }
        public string Puerto { get; set; }
        public string Password { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
    }
}