﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Usuarios")]
    public class Cat_Usuarios
    {
        [Key]
        public int Usuarios_ID { get; set; }
        public int? Rol_ID { get; set; }
        public int? Especialidad_ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido_Paterno { get; set; }
        public string Apellido_Materno { get; set; }
        public string Correo_Electronico { get; set; }
        public string Telefono { get; set; }
        public string Password { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
        public string CustomerID { get; set; }
        public string SubscriptionID { get; set; }
        public bool Es_Empresa { get; set; }
        public string Nombre_Imagen { get; set; }
        public string Tipo_Imagen { get; set; }
        public string Nombre_Empresa { get; set; }
        public virtual Cat_Roles Cat_Roles { get; set; }
        public virtual Cat_Especialidades Cat_Especialidades { get; set; }
    }
}