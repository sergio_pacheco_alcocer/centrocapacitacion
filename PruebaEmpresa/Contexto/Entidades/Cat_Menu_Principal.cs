﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Menu_Principal")]
    public class Cat_Menu_Principal
    {
        [Key]
        public int Menu_Principal_ID { get; set; }
        public string Nombre { get; set; }
        public string Controlador { get; set; }
        public string Accion { get; set; }
        public virtual ICollection<Cat_Fase_Uno> Cat_Fase_Uno { get; set; }
    }
}