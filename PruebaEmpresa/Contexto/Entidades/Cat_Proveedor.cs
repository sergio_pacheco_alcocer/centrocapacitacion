﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Proveedor")]
    public class Cat_Proveedor
    {

        [Key]
        public int Proveedor_ID { get; set; }
        public int Giro_Proveedor_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public string Nombre_Proveedor { get; set; }
        public string Que_Hace_la_Empresa { get; set; }
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string Numero_Exterior { get; set; }
        public string Numero_Interior { get; set; }
        public string Estatus { get; set; }
        public string Correo_Electronico { get; set; }
        public string Sitio_WEB { get; set; }
        public string Telefono { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
        public DateTime? Fecha_Inicio_Vigencia { get; set; }
        public DateTime? Fecha_Fin_Vigencia { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }

        public virtual Cat_Giro_Proveedor Cat_Giro_Proveedor { get; set; }
    }
}