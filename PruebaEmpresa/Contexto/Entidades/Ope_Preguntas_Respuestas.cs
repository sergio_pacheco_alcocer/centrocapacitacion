﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Ope_Preguntas_Respuestas")]
    public class Ope_Preguntas_Respuestas
    {
        [Key]
        public int Pregunta_Respuesta_ID { get; set; }
        public int Pregunta_ID { get; set; }
        public int Usuario_ID { get; set; }
        public string Respuesta { get; set; }
        public string Tipo { get; set; }
        public bool Visto_Respuesta { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }

        [ForeignKey("Pregunta_ID")]
        public Ope_Preguntas Ope_Preguntas { get; set; }
        [ForeignKey("Usuario_ID")]
        public Cat_Usuarios Cat_Usuarios { get; set; }
    }
}