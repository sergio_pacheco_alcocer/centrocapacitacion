﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Especialidades")]
    public class Cat_Especialidades
    {

        [Key]
        public int Especialidad_ID { get; set; }
        public string Nombre_Especialidad { get; set; }
        public string Estatus { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
    }
}