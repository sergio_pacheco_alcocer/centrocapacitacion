﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Giro_Proveedor")]
    public class Cat_Giro_Proveedor
    {
        [Key]
        public int Giro_Proveedor_ID { get; set; }
        public string Nombre_Giro_Proveedor { get; set; }
        public string Estatus { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public DateTime? Fecha_Modifico { get; set; }
    }
}