﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Ope_Cor_Cancelar_Suscripcion")]
    public class Ope_Cor_Cancelar_Suscripcion
    {
        [Key]
        public int Cancelar_Suscripcion_ID { get; set; }
        public int Usuarios_ID { get; set; }
        public int Cargo_ID { get; set; }
        public string Identificador { get; set; }
        public DateTime Fecha_Creo { get; set; }
    }
}