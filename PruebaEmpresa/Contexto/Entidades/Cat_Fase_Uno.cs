﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaEmpresa.Contexto.Entidades
{
    [Table("Cat_Fase_Uno")]
    public class Cat_Fase_Uno
    {
        [Key]
        public int Fase_Uno_ID { get; set; }
        public int Menu_Principal_ID { get; set; }
        public string Nombre_Fase_Uno { get; set; }

        public virtual Cat_Menu_Principal Cat_Menu_Principal { get; set; }
        public virtual ICollection<Cat_Fase_Dos> Cat_Fase_Dos { get; set; }
    }
}