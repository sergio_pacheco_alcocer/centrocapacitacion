﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;

namespace PruebaEmpresa.Contexto
{
    public class EFRoles_Repo : IRoles_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public IQueryable<Cat_Roles> Cat_Roles => Contexto.Cat_Roles;

        public string Estar_Repetido_Rol(string Nombre, int Rol_ID)
        {
            string respuesta = "NO";


            var buscar = Contexto.Cat_Roles.Where(x => x.Nombre == Nombre && x.Rol_ID != Rol_ID);

            if (buscar.Any())
            {
                respuesta = "SI";
            }

            return respuesta;
        }

        public List<ComboView> Obtener_Roles_Combo()
        {
            return Contexto.Cat_Roles
                  .Where(x=> x.Nombre != "Mentor")
                 .OrderBy(x => x.Nombre)
                 .Select(x => new ComboView
                 {
                     id = x.Rol_ID,
                     name = x.Nombre,
                     clave = ""
                 }).ToList();
        }

        public RolesView Obtener_Un_Rol(string Nombre)
        {
          return   Contexto.Cat_Roles.Where(x=> x.Nombre.Trim().ToLower() == Nombre.Trim().ToLower()).Select(x => new RolesView
            {
              Descripcion = x.Descripcion,
              Estatus = x.Estatus,
              Nombre = x.Nombre,
              Rol_ID = x.Rol_ID
            }).FirstOrDefault();
        }

        public void Registar_Rol(RolesView rolesView)
        {
           
            if(rolesView.Rol_ID == 0)
            {
                Cat_Roles cat_Roles = new Cat_Roles();
                cat_Roles.Nombre = rolesView.Nombre;
                cat_Roles.Descripcion = rolesView.Descripcion;
                cat_Roles.Estatus = rolesView.Estatus;
                cat_Roles.Fecha_Creo = DateTime.Now;
                cat_Roles.Usuario_Creo = rolesView.Usuario_Creo;
                Contexto.Cat_Roles.Add(cat_Roles);
            }
            else
            {
                Cat_Roles cat_Roles = Contexto.Cat_Roles.Find(rolesView.Rol_ID);
                cat_Roles.Nombre = rolesView.Nombre;
                cat_Roles.Descripcion = rolesView.Descripcion;
                cat_Roles.Estatus = rolesView.Estatus;
                cat_Roles.Fecha_Modifico = DateTime.Now;
                cat_Roles.Usuario_Creo = rolesView.Usuario_Creo;

            }

            Contexto.SaveChanges();
        }
    }
}