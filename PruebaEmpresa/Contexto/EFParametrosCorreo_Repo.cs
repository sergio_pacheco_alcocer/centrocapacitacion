﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace PruebaEmpresa.Contexto
{
    public class EFParametrosCorreo_Repo :IParametroCorreo_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public string Eliminar(int Parametro_Correo_ID)
        {
            string respuesta = "";

            try
            {
                Cat_Parametro_Correo cat_Parametro_Correo = Contexto.Cat_Parametro_Correo.Find(Parametro_Correo_ID);
                Contexto.Cat_Parametro_Correo.Remove(cat_Parametro_Correo);
                respuesta = "bien";
                Contexto.SaveChanges();

            }
            catch(DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public void Guardar(ParametrosCorreoView parametrosCorreoView)
        {

            if (parametrosCorreoView.Parametro_Correo_ID == 0)
            {
                Cat_Parametro_Correo cat_Parametro_Correo = new Cat_Parametro_Correo();
                cat_Parametro_Correo.Password = parametrosCorreoView.Password;
                cat_Parametro_Correo.Host_Email = parametrosCorreoView.Host_Email;
                cat_Parametro_Correo.Puerto = parametrosCorreoView.Puerto;
                cat_Parametro_Correo.Email = parametrosCorreoView.Email;
                cat_Parametro_Correo.Usuario_Creo = parametrosCorreoView.Usuario_Creo;
                cat_Parametro_Correo.Fecha_Creo = DateTime.Now;

                Contexto.Cat_Parametro_Correo.Add(cat_Parametro_Correo);
            }
            else {

                Cat_Parametro_Correo cat_Parametro_Correo = Contexto.Cat_Parametro_Correo.Find(parametrosCorreoView.Parametro_Correo_ID);
                cat_Parametro_Correo.Password = parametrosCorreoView.Password;
                cat_Parametro_Correo.Host_Email = parametrosCorreoView.Host_Email;
                cat_Parametro_Correo.Puerto = parametrosCorreoView.Puerto;
                cat_Parametro_Correo.Email = parametrosCorreoView.Email;
                cat_Parametro_Correo.Usuario_Modifico = parametrosCorreoView.Usuario_Creo;
                cat_Parametro_Correo.Fecha_Modifico = DateTime.Now;
            }


            Contexto.SaveChanges();
        }

        public List<ParametrosCorreoView> Obtener_Parametros_Correo()
        {
            return Contexto.Cat_Parametro_Correo.Select(x => new ParametrosCorreoView
            {
                Email = x.Email,
                Host_Email = x.Host_Email,
                Parametro_Correo_ID = x.Parametro_Correo_ID,
                Password = x.Password,
                Puerto = x.Puerto

            }).ToList();
        }

        public ParametrosCorreoView Obtener_Un_Parametro()
        {
            return Contexto.Cat_Parametro_Correo.Select(x => new ParametrosCorreoView
            {
                Email = x.Email,
                Host_Email = x.Host_Email,
                Parametro_Correo_ID = x.Parametro_Correo_ID,
                Password = x.Password,
                Puerto = x.Puerto

            }).FirstOrDefault();
        }

        public ParametrosCorreoView Obtener_Un_Parametro(int Parametro_Correo_ID)
        {
            return Contexto.Cat_Parametro_Correo.Where(x=> x.Parametro_Correo_ID == Parametro_Correo_ID).Select(x => new ParametrosCorreoView
            {
                Email = x.Email,
                Host_Email = x.Host_Email,
                Parametro_Correo_ID = x.Parametro_Correo_ID,
                Password = x.Password,
                Puerto = x.Puerto

            }).FirstOrDefault();
        }
    }
}