﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace PruebaEmpresa.Contexto
{
    public class EFEspecialidad_Repo : IEspecialidad_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public IQueryable<Cat_Especialidades> Cat_Especialidades => Contexto.Cat_Especialidades;

        public string Eliminar(int Especialidad_ID)
        {
            string respuesta = "";

            try
            {
                Cat_Especialidades cat_Especialidades = Contexto.Cat_Especialidades.Find(Especialidad_ID);
                Contexto.Cat_Especialidades.Remove(cat_Especialidades);
                respuesta = "bien";
                Contexto.SaveChanges();

            }
            catch (DbUpdateException e) {

                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public string Esta_Repetido_Especialidad(string Nombre_Especialidad, int Especialidad_ID)
        {
            string respuesta="NO";


            var buscar = Contexto.Cat_Especialidades.Where(x => x.Nombre_Especialidad == Nombre_Especialidad && x.Especialidad_ID != Especialidad_ID);

            if (buscar.Any())
            {
                respuesta = "SI";
            }

            return respuesta;
        }

        public void Guardar(EspecialidadViewModel especialidadViewModel)
        {

            if (especialidadViewModel.Especialidad_ID == 0) {

                Cat_Especialidades cat_Especialidades = new Cat_Especialidades();

                cat_Especialidades.Nombre_Especialidad = especialidadViewModel.Nombre_Especialidad;
                cat_Especialidades.Estatus = especialidadViewModel.Estatus;
                cat_Especialidades.Fecha_Creo = DateTime.Now;
                cat_Especialidades.Usuario_Creo = especialidadViewModel.Usuario_Creo;

                Contexto.Cat_Especialidades.Add(cat_Especialidades);

            }
            else
            {
                Cat_Especialidades cat_Especialidades = Contexto.Cat_Especialidades.Find(especialidadViewModel.Especialidad_ID);
                cat_Especialidades.Nombre_Especialidad = especialidadViewModel.Nombre_Especialidad;
                cat_Especialidades.Estatus = especialidadViewModel.Estatus;
                cat_Especialidades.Fecha_Modifico = DateTime.Now;
                cat_Especialidades.Usuario_Modifico = especialidadViewModel.Usuario_Creo;
            }

            Contexto.SaveChanges();
        }

        public List<ComboView> Obtener_Especialidades_Combo()
        {
            return Contexto.Cat_Especialidades.Where(x => x.Estatus == "ACTIVO")
                .OrderBy(x=> x.Nombre_Especialidad)
                .Select(x => new ComboView
                {
                    id = x.Especialidad_ID,
                    name = x.Nombre_Especialidad,
                    clave = ""
                }).ToList();
        }

        public EspecialidadViewModel Obtener_Una_Especialidad(int Especialidad_ID)
        {
            return Contexto.Cat_Especialidades.Where(x => x.Especialidad_ID == Especialidad_ID)
                .OrderBy(x=> x.Nombre_Especialidad)
                 .Select(x => new EspecialidadViewModel
                 {
                     Especialidad_ID = x.Especialidad_ID,
                     Estatus = x.Estatus,
                     Nombre_Especialidad = x.Nombre_Especialidad

                 }).FirstOrDefault();
        }
    }
}