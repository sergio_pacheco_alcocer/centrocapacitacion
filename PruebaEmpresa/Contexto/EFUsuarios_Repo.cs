﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace PruebaEmpresa.Contexto
{
    public class EFUsuarios_Repo : IUsuarios_Repo
    {
        private EFDbContext Contexto = new EFDbContext();
        public IQueryable<Cat_Usuarios> Cat_Usuarios => Contexto.Cat_Usuarios;

        public void Agergar_SubscriptionID(EFDbContext eFDbContext, int Usuario_ID, string SubscriptionID)
        {
            Cat_Usuarios cat_Usuarios = eFDbContext.Cat_Usuarios.Find(Usuario_ID);
            cat_Usuarios.SubscriptionID = SubscriptionID;
            eFDbContext.SaveChanges();
        }

        public void Agregar_Customer_ID(int Usuario_ID, string Customer_ID)
        {
            Cat_Usuarios cat_Usuarios = Contexto.Cat_Usuarios.Find(Usuario_ID);
            cat_Usuarios.CustomerID = Customer_ID; 
            Contexto.SaveChanges();
        }

        public void Agregar_SubscriptionID(int Usuario_ID, string SubscriptionID)
        {
            Cat_Usuarios cat_Usuarios = Contexto.Cat_Usuarios.Find(Usuario_ID);
            cat_Usuarios.SubscriptionID = SubscriptionID;
            Contexto.SaveChanges();
        }

        public void Eliminar_SucriptionID(EFDbContext eFDbContext, int Usuario_ID)
        {
            Cat_Usuarios cat_Usuarios = eFDbContext.Cat_Usuarios.Find(Usuario_ID);
            cat_Usuarios.SubscriptionID = "";
            eFDbContext.SaveChanges();
        }

        public string Eliminar_Usuario(int Usuarios_ID)
        {
            string respuesta = "";

            try {

                Cat_Usuarios cat_Usuarios = Contexto.Cat_Usuarios.Find(Usuarios_ID);
                Contexto.Cat_Usuarios.Remove(cat_Usuarios);
                respuesta = "bien";
                Contexto.SaveChanges();
            }
            catch(DbUpdateException e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        switch (sqlException.Errors[0].Number)
                        {
                            case 547:
                                return respuesta = "El elemento  esta referenciado en otra tabla";

                            default:
                                return respuesta = "No se puede eliminar el elemento";

                        }
                    }
                }
            }

            return respuesta;
        }

        public string Esta_Repetido_Usuario(string Correo_Electronico, int Usuarios_ID)
        {
            string respuesta = "NO";


            var buscar = Contexto.Cat_Usuarios.Where(x => x.Correo_Electronico == Correo_Electronico && x.Usuarios_ID != Usuarios_ID);

            if (buscar.Any())
            {
                respuesta = "SI";
            }

            return respuesta;
        }

        public IQueryable<MentorView> Obtener_Mentores()
        {
            return Contexto.Cat_Usuarios.Where(x => x.Cat_Roles.Nombre == "Mentor")
                    .OrderBy(x=> x.Nombre)
                    .ThenBy(x=> x.Apellido_Paterno)
                   .Select(x => new MentorView
                   {
                       Apellido_Materno = x.Apellido_Materno,
                       Apellido_Paterno = x.Apellido_Paterno,
                       Correo_Electronico = x.Correo_Electronico,
                       Estatus = x.Estatus,
                       Nombre = x.Nombre,
                       Contraseña = x.Password,
                       Confirmar_Contraseña = x.Password,
                       Telefono = x.Telefono,
                       Usuarios_ID = x.Usuarios_ID,
                       Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                       Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                       Nombre_Especialidad = x.Cat_Especialidades == null ? "" : x.Cat_Especialidades.Nombre_Especialidad,
                       Especialidad_ID = x.Especialidad_ID,
                       Es_Empresa = x.Es_Empresa,
                       Nombre_Imagen = x.Nombre_Imagen,
                       Tipo_Imagen = x.Tipo_Imagen

                   });
        }

        public MentorView Obtener_Un_Mentor(int Usuarios_ID)
        {
            return Contexto.Cat_Usuarios.Where(x => x.Usuarios_ID == Usuarios_ID )
                  .Select(x => new MentorView
                  {
                      Apellido_Materno = x.Apellido_Materno,
                      Apellido_Paterno = x.Apellido_Paterno,
                      Correo_Electronico = x.Correo_Electronico,
                      Estatus = x.Estatus,
                      Nombre = x.Nombre,
                      Contraseña = x.Password,
                      Confirmar_Contraseña = x.Password,
                      Telefono = x.Telefono,
                      Usuarios_ID = x.Usuarios_ID,
                      Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                      Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                      Nombre_Especialidad = x.Cat_Especialidades == null ? "" : x.Cat_Especialidades.Nombre_Especialidad,
                      Especialidad_ID = x.Especialidad_ID,
                      Es_Empresa = x.Es_Empresa,
                      Nombre_Imagen = x.Nombre_Imagen,
                      Tipo_Imagen = x.Tipo_Imagen
                  }).FirstOrDefault();
        }

        public UsuariosView Obtener_Usuario(string Correo_Electronico, string Password)
        {

            return Contexto.Cat_Usuarios.Where(x => x.Correo_Electronico == Correo_Electronico && x.Password == Password && x.Estatus=="ACTIVO")
                 .Select(x => new UsuariosView
                 {
                     Apellido_Materno = x.Apellido_Materno,
                     Apellido_Paterno = x.Apellido_Paterno,
                     Correo_Electronico = x.Correo_Electronico,
                     Estatus = x.Estatus,
                     Nombre = x.Nombre,
                     Password = x.Password,
                     Telefono = x.Telefono,
                     Usuarios_ID = x.Usuarios_ID,
                     Rol_ID = x.Cat_Roles == null ? 0: x.Cat_Roles.Rol_ID,
                     Nombre_Rol = x.Cat_Roles == null ? "": x.Cat_Roles.Nombre,
                     CustomerID = x.CustomerID == null ? "" : x.CustomerID,
                     SubscriptionID = x.SubscriptionID == null ? "" : x.SubscriptionID,
                     Nombre_Especialidad = x.Cat_Especialidades == null ? "":x.Cat_Especialidades.Nombre_Especialidad,
                     Especialidad_ID = x.Especialidad_ID
                 }).FirstOrDefault();


        }

        public UsuariosView Obtener_Usuario(string Correo_Electronico)
        {
            return Contexto.Cat_Usuarios.Where(x => x.Correo_Electronico == Correo_Electronico  && x.Estatus == "ACTIVO")
                 .Select(x => new UsuariosView
                 {
                     Apellido_Materno = x.Apellido_Materno,
                     Apellido_Paterno = x.Apellido_Paterno,
                     Correo_Electronico = x.Correo_Electronico,
                     Estatus = x.Estatus,
                     Nombre = x.Nombre,
                     Password = x.Password,
                     Telefono = x.Telefono,
                     Usuarios_ID = x.Usuarios_ID,
                     Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                     Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                     CustomerID = x.CustomerID == null ? "" : x.CustomerID,
                     SubscriptionID = x.SubscriptionID == null ? "" : x.SubscriptionID,
                     Nombre_Especialidad = x.Cat_Especialidades == null ? "" : x.Cat_Especialidades.Nombre_Especialidad,
                     Especialidad_ID = x.Especialidad_ID
                 }).FirstOrDefault();
        }

        public UsuariosView Obtener_Usuario(int Usuario_ID)
        {
            return Contexto.Cat_Usuarios.Where(x => x.Usuarios_ID == Usuario_ID )
                 .Select(x => new UsuariosView
                 {
                     Apellido_Materno = x.Apellido_Materno,
                     Apellido_Paterno = x.Apellido_Paterno,
                     Correo_Electronico = x.Correo_Electronico,
                     Estatus = x.Estatus,
                     Nombre = x.Nombre,
                     Password = x.Password,
                     Telefono = x.Telefono,
                     Usuarios_ID = x.Usuarios_ID,
                     Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                     Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                     CustomerID = x.CustomerID == null ? "" : x.CustomerID,
                     SubscriptionID = x.SubscriptionID == null ? "": x.SubscriptionID,
                     Nombre_Especialidad = x.Cat_Especialidades == null ? "" : x.Cat_Especialidades.Nombre_Especialidad,
                     Especialidad_ID = x.Especialidad_ID,
                     Nombre_Empresa = x.Nombre_Empresa
                 }).FirstOrDefault();
        }

        public IQueryable<UsuariosView> Obtener_Usuarios(string sortOrder, string searchString, int? Rol_ID)
        {
            var dbUsuarios = Contexto.Cat_Usuarios.Where(x => x.Cat_Roles.Nombre != "Mentor");

            if (!String.IsNullOrEmpty(searchString))
            {
                dbUsuarios = dbUsuarios.Where(x => x.Nombre.Contains(searchString) || x.Apellido_Paterno.Contains(searchString) || x.Apellido_Materno.Contains(searchString) || x.Correo_Electronico.Contains(searchString));
            }


            if (Rol_ID.HasValue)
            {
                dbUsuarios = dbUsuarios.Where(x => x.Rol_ID == Rol_ID.Value);
            }

            switch (sortOrder)
            {
                case "Correo":
                    dbUsuarios = dbUsuarios.OrderBy(x => x.Correo_Electronico);
                    break;
                case "Apellido_Paterno":
                    dbUsuarios = dbUsuarios.OrderBy(x => x.Apellido_Paterno);
                    break;
                case "Estatus":
                    dbUsuarios = dbUsuarios.OrderBy(x => x.Estatus);
                    break;
                case "estatus_desc":
                    dbUsuarios = dbUsuarios.OrderByDescending(x => x.Estatus);
                    break;
                case "correo_desc":
                    dbUsuarios = dbUsuarios.OrderByDescending(x => x.Correo_Electronico);
                    break;
                case "apellido_paterno_desc":
                    dbUsuarios = dbUsuarios.OrderByDescending(x => x.Apellido_Paterno);
                    break;
                case "nombre_desc":
                    dbUsuarios = dbUsuarios.OrderByDescending(x => x.Nombre);
                    break;
                default:
                    dbUsuarios = dbUsuarios.OrderBy(x => x.Nombre);
                    break;
            }


            return dbUsuarios.Select(x => new UsuariosView
            {
                Apellido_Materno = x.Apellido_Materno,
                Apellido_Paterno = x.Apellido_Paterno,
                Correo_Electronico = x.Correo_Electronico,
                Estatus = x.Estatus,
                Nombre = x.Nombre,
                Password = x.Password,
                Telefono = x.Telefono,
                Usuarios_ID = x.Usuarios_ID,
                Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                CustomerID = x.CustomerID == null ? "" : x.CustomerID,
                SubscriptionID = x.SubscriptionID == null ? "" : x.SubscriptionID
            });

        }

        public UsuariosView Obtener_Usuario_Customer(string CustomerID)
        {
            return Contexto.Cat_Usuarios.Where(x => x.CustomerID == CustomerID)
                .Select(x => new UsuariosView
                {
                    Apellido_Materno = x.Apellido_Materno,
                    Apellido_Paterno = x.Apellido_Paterno,
                    Correo_Electronico = x.Correo_Electronico,
                    Estatus = x.Estatus,
                    Nombre = x.Nombre,
                    Password = x.Password,
                    Telefono = x.Telefono,
                    Usuarios_ID = x.Usuarios_ID,
                    Rol_ID = x.Cat_Roles == null ? 0 : x.Cat_Roles.Rol_ID,
                    Nombre_Rol = x.Cat_Roles == null ? "" : x.Cat_Roles.Nombre,
                    CustomerID = x.CustomerID == null ? "" : x.CustomerID,
                    SubscriptionID = x.SubscriptionID == null ? "" : x.SubscriptionID,
                    Nombre_Especialidad = x.Cat_Especialidades == null ? "" : x.Cat_Especialidades.Nombre_Especialidad,
                    Especialidad_ID = x.Especialidad_ID
                }).FirstOrDefault();
        }

        public void Registrar_Usuario(UsuariosView usuariosView)
        {
            if (usuariosView.Usuarios_ID == 0)
            {
                Cat_Usuarios cat_Usuarios = new Cat_Usuarios();
                cat_Usuarios.Correo_Electronico = usuariosView.Correo_Electronico;
                cat_Usuarios.Password = usuariosView.Password;
                cat_Usuarios.Telefono = usuariosView.Telefono;
                cat_Usuarios.Nombre = usuariosView.Nombre;

                if (usuariosView.Apellido_Materno != null)
                    cat_Usuarios.Apellido_Materno = usuariosView.Apellido_Materno;
                else
                    cat_Usuarios.Apellido_Materno = "";


                if (usuariosView.Apellido_Paterno != null)
                    cat_Usuarios.Apellido_Paterno = usuariosView.Apellido_Paterno;
                else
                    cat_Usuarios.Apellido_Paterno = "";

                cat_Usuarios.Fecha_Creo = DateTime.Now;
                cat_Usuarios.Usuario_Creo = usuariosView.Usuario_Creo;

                if (usuariosView.Rol_ID !=0)
                   cat_Usuarios.Rol_ID = usuariosView.Rol_ID;

                if (usuariosView.Especialidad_ID.HasValue)
                {
                    cat_Usuarios.Especialidad_ID = usuariosView.Especialidad_ID.Value;
                }
                else
                {
                    cat_Usuarios.Especialidad_ID = null;
                }

                cat_Usuarios.Es_Empresa = usuariosView.Es_Empresa;
                cat_Usuarios.Estatus = usuariosView.Estatus;
                cat_Usuarios.Nombre_Imagen = usuariosView.Nombre_Imagen;
                cat_Usuarios.Tipo_Imagen = usuariosView.Tipo_Imagen;

                if (usuariosView.Nombre_Empresa != null)
                    cat_Usuarios.Nombre_Empresa = usuariosView.Nombre_Empresa;
                else
                    cat_Usuarios.Nombre_Empresa = "";

                Contexto.Cat_Usuarios.Add(cat_Usuarios);
            }
            else {

                Cat_Usuarios cat_Usuarios = Contexto.Cat_Usuarios.Find(usuariosView.Usuarios_ID);
                cat_Usuarios.Correo_Electronico = usuariosView.Correo_Electronico;
                cat_Usuarios.Password = usuariosView.Password;
                cat_Usuarios.Telefono = usuariosView.Telefono;
                cat_Usuarios.Nombre = usuariosView.Nombre;

                if (usuariosView.Apellido_Materno != null)
                    cat_Usuarios.Apellido_Materno = usuariosView.Apellido_Materno;
                else
                    cat_Usuarios.Apellido_Materno = "";

                if (usuariosView.Apellido_Paterno != null)
                    cat_Usuarios.Apellido_Paterno = usuariosView.Apellido_Paterno;
                else
                    cat_Usuarios.Apellido_Paterno = "";

                cat_Usuarios.Fecha_Modifico = DateTime.Now;
                cat_Usuarios.Usuario_Modifico = usuariosView.Usuario_Creo;
                cat_Usuarios.Estatus = usuariosView.Estatus;
                cat_Usuarios.Es_Empresa = usuariosView.Es_Empresa;
                cat_Usuarios.Nombre_Imagen = usuariosView.Nombre_Imagen;
                cat_Usuarios.Tipo_Imagen = usuariosView.Tipo_Imagen;

                if (usuariosView.Nombre_Empresa != null)
                    cat_Usuarios.Nombre_Empresa = usuariosView.Nombre_Empresa;
                else
                    cat_Usuarios.Nombre_Empresa = "";

                if (usuariosView.Rol_ID != 0)
                    cat_Usuarios.Rol_ID = usuariosView.Rol_ID;

                if (usuariosView.Especialidad_ID.HasValue)
                {
                    cat_Usuarios.Especialidad_ID = usuariosView.Especialidad_ID.Value;
                }
                else
                {
                    cat_Usuarios.Especialidad_ID = null;
                }
            }

            Contexto.SaveChanges();
        }
    }
}