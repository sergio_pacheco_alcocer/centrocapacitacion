﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaEmpresa.Contexto.Interfaces;
using PruebaEmpresa.Models;
using PruebaEmpresa.Contexto.Entidades;

namespace PruebaEmpresa.Contexto
{
    public class EFProveedores_Repo : IProveedores_Repo
    {
        private EFDbContext Contexto = new EFDbContext();

        public void Actualizar_Vigencia_Proveedor(EFDbContext eFDbContext, int Usuarios_ID, DateTime FechaInicio, DateTime FechaFin)
        {
            Cat_Proveedor cat_Proveedor = eFDbContext.Cat_Proveedor.Where(x => x.Usuarios_ID == Usuarios_ID).FirstOrDefault();

            cat_Proveedor.Fecha_Inicio_Vigencia = FechaInicio;
            cat_Proveedor.Fecha_Fin_Vigencia = FechaFin;

            eFDbContext.SaveChanges();

        }

        public void Guardar(ProveedorView proveedorView)
        {
            
            if(proveedorView.Proveedor_ID == 0)
            {

                Cat_Proveedor cat_Proveedor = new Cat_Proveedor();
                cat_Proveedor.Calle = proveedorView.Calle;
                cat_Proveedor.Colonia = proveedorView.Colonia;
                cat_Proveedor.Correo_Electronico = proveedorView.Correo_Electronico;
                cat_Proveedor.Estatus = proveedorView.Estatus;
                cat_Proveedor.Fecha_Creo = DateTime.Now;
                cat_Proveedor.Que_Hace_la_Empresa = proveedorView.Que_Hace_la_Empresa;
                cat_Proveedor.Nombre_Proveedor = proveedorView.Nombre_Proveedor;
                cat_Proveedor.Numero_Exterior = proveedorView.Numero_Exterior;
                cat_Proveedor.Numero_Interior = proveedorView.Numero_Interior;
                cat_Proveedor.Telefono = proveedorView.Telefono;
                cat_Proveedor.Sitio_WEB = proveedorView.Sitio_WEB;
                cat_Proveedor.Giro_Proveedor_ID = proveedorView.Giro_Proveedor_ID;
                cat_Proveedor.Usuarios_ID = proveedorView.Usuarios_ID;
                cat_Proveedor.Latitud = proveedorView.Latitud;
                cat_Proveedor.Longitud = proveedorView.Longitud;
                Contexto.Cat_Proveedor.Add(cat_Proveedor);

            }
            else
            {
                Cat_Proveedor cat_Proveedor = Contexto.Cat_Proveedor.Find(proveedorView.Proveedor_ID);
                cat_Proveedor.Calle = proveedorView.Calle;
                cat_Proveedor.Colonia = proveedorView.Colonia;
                cat_Proveedor.Correo_Electronico = proveedorView.Correo_Electronico;
                cat_Proveedor.Estatus = cat_Proveedor.Estatus;
                cat_Proveedor.Fecha_Modifico = DateTime.Now;
                cat_Proveedor.Que_Hace_la_Empresa = proveedorView.Que_Hace_la_Empresa;
                cat_Proveedor.Nombre_Proveedor = proveedorView.Nombre_Proveedor;
                cat_Proveedor.Numero_Exterior = proveedorView.Numero_Exterior;
                cat_Proveedor.Numero_Interior = proveedorView.Numero_Interior;
                cat_Proveedor.Telefono = proveedorView.Telefono;
                cat_Proveedor.Sitio_WEB = proveedorView.Sitio_WEB;
                cat_Proveedor.Latitud = proveedorView.Latitud;
                cat_Proveedor.Longitud = proveedorView.Longitud;
                cat_Proveedor.Giro_Proveedor_ID = proveedorView.Giro_Proveedor_ID;

            }

            Contexto.SaveChanges();

        }

        public Cls_Paginado<ProveedorView> Obtener_Proveedores(int offset, int limit, int? giro_proveedor_id, string busqueda)
        {
            Cls_Paginado<ProveedorView> Paginado;

            var busquedaFiltro = Contexto.Cat_Proveedor.Where(
                x => string.IsNullOrEmpty(busqueda) || x.Nombre_Proveedor.ToLower().Contains(busqueda.ToLower())
                || x.Colonia.ToLower().Contains(busqueda.ToLower())
                || x.Calle.ToLower().Contains(busqueda.ToLower()))
                .Where(x=> x.Estatus == "ACTIVO")
                .Where(x=>  DateTime.Now <= x.Fecha_Fin_Vigencia )
                .Where(x => giro_proveedor_id.HasValue == false || x.Giro_Proveedor_ID == giro_proveedor_id.Value)
                .OrderBy(x => x.Nombre_Proveedor)
                .ThenBy(x => x.Colonia)
                .ThenBy(x => x.Calle)
                .Select(x => new ProveedorView
                {
                    Calle = x.Calle,
                    Colonia = x.Colonia,
                    Giro_Proveedor_ID = x.Giro_Proveedor_ID,
                    Nombre_Giro_Proveedor = x.Cat_Giro_Proveedor.Nombre_Giro_Proveedor,
                    Nombre_Proveedor = x.Nombre_Proveedor,
                    Numero_Exterior = x.Numero_Exterior,
                    Numero_Interior = x.Numero_Interior,
                    Proveedor_ID = x.Proveedor_ID,
                    Sitio_WEB = x.Sitio_WEB,
                    Telefono = x.Telefono,
                    Correo_Electronico = x.Correo_Electronico,
                    Estatus = x.Estatus,
                    Que_Hace_la_Empresa = x.Que_Hace_la_Empresa,
                    Longitud = x.Longitud,
                    Latitud = x.Latitud
                });

            var datos_visualizar = busquedaFiltro.Skip(offset).Take(limit).ToList();
            Paginado = new Cls_Paginado<ProveedorView>(datos_visualizar, busquedaFiltro.Count());
            return Paginado;

        }

        public List<ProveedorView> Obtener_Proveedores(int Usuarios_ID)
        {

            return Contexto.Cat_Proveedor.Where(x => x.Usuarios_ID == Usuarios_ID)
                .Select(x => new ProveedorView
                {
                    Calle = x.Calle,
                    Colonia = x.Colonia,
                    Giro_Proveedor_ID = x.Giro_Proveedor_ID,
                    Nombre_Giro_Proveedor = x.Cat_Giro_Proveedor.Nombre_Giro_Proveedor,
                    Nombre_Proveedor = x.Nombre_Proveedor,
                    Numero_Exterior = x.Numero_Exterior,
                    Numero_Interior = x.Numero_Interior,
                    Proveedor_ID = x.Proveedor_ID,
                    Sitio_WEB = x.Sitio_WEB,
                    Telefono = x.Telefono,
                    Correo_Electronico = x.Correo_Electronico,
                    Estatus = x.Estatus,
                    Que_Hace_la_Empresa = x.Que_Hace_la_Empresa,
                    Longitud = x.Longitud,
                    Latitud = x.Latitud
                }).ToList();
        }

        public ProveedorView Obtener_Un_Proveedor(int Proveedor_ID)
        {
            ProveedorView proveedorView = new ProveedorView();

            proveedorView = Contexto.Cat_Proveedor.Where(x => x.Proveedor_ID == Proveedor_ID)
                .Select(x => new ProveedorView
                {
                    Calle = x.Calle,
                    Colonia = x.Colonia,
                    Giro_Proveedor_ID = x.Giro_Proveedor_ID,
                    Nombre_Giro_Proveedor = x.Cat_Giro_Proveedor.Nombre_Giro_Proveedor,
                    Nombre_Proveedor = x.Nombre_Proveedor,
                    Numero_Exterior = x.Numero_Exterior,
                    Numero_Interior = x.Numero_Interior,
                    Proveedor_ID = x.Proveedor_ID,
                    Sitio_WEB = x.Sitio_WEB,
                    Telefono = x.Telefono,
                    Correo_Electronico = x.Correo_Electronico,
                    Estatus = x.Estatus,
                    Que_Hace_la_Empresa = x.Que_Hace_la_Empresa,
                    Longitud = x.Longitud,
                    Latitud = x.Latitud
                }).FirstOrDefault();

            return proveedorView;
        }
    }
}